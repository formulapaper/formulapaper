package application.model;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GeneralSettings {

	private File lastProjectLoaded;

	private int selectedStyle;
	private List<String> style;

	public File getLastProjectLoaded() {
		return lastProjectLoaded;
	}

	public GeneralSettings(){
		lastProjectLoaded = new File("C:\\Users\\");

		style = new ArrayList<String>();
		try {
			style.add(new File("src/style/FormulaStyle_Default.css").toURI().toURL().toExternalForm());
			style.add(new File("src/style/FormulaStyle_White.css").toURI().toURL().toExternalForm());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		selectedStyle = 0;
	}

	public String getSelectedStylesheet(){
		return style.get(selectedStyle);
	}

	public int getSelectedStyle() {
		return selectedStyle;
	}

	@XmlElement
	public void setSelectedStyle(int selectedStyle) {
		this.selectedStyle = selectedStyle;
	}

	public List<String> getStyle() {
		return style;
	}

	@XmlElement
	public void setStyle(List<String> style) {
		this.style = style;
	}

	@XmlElement
	public void setLastProjectLoaded(File lastProjectLoaded) {
		this.lastProjectLoaded = lastProjectLoaded;
	}

	public void save(){

		try {
	        JAXBContext context = JAXBContext
	                .newInstance(GeneralSettings.class);
	        Marshaller m;

			m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

	        // Enpacotando e salvando XML  no arquivo.
	        m.marshal(this, new File("settings"));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void copy(GeneralSettings other){
		lastProjectLoaded = other.getLastProjectLoaded();
		selectedStyle = other.getSelectedStyle();
	}

	public boolean load() {
		File file = new File("settings");
		if(!file.exists()){
			try {
				file.createNewFile();
				save();
				return true;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}else{
		    try {
		        JAXBContext context = JAXBContext
		                .newInstance(GeneralSettings.class);
		        Unmarshaller um = context.createUnmarshaller();

		        // Reading XML from the file and unmarshalling.
		        GeneralSettings wrapper = (GeneralSettings) um.unmarshal(new File("settings"));

		        this.copy(wrapper);

		        return true;
		    } catch (Exception e) { // catches ANY exception
		    	e.printStackTrace();
		    	return false;
		    }
		}
	}
}
