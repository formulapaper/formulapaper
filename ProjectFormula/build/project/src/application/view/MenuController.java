package application.view;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import application.ApplicationContext;
import application.MainApp;
import application.model.AlgorithmConfiguration;
import box.InformationBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.User;
import undecorator.insidefx.undecorator.Undecorator;
import undecorator.insidefx.undecorator.UndecoratorScene;
import webservice.FormulaWebService;

public class MenuController {

	@FXML
	private Button createProject;

	@FXML
	private Button loadProject;

	@FXML
	private Button settings;

	@FXML
	private Button about;


	/**
     * The constructor is called before initialize()
     */
    public MenuController() {
    	/*User user = FormulaWebService.getUser("manolo@gmail.com", "teste");
    	if(user == null){
    		status = "User: null";
    	}else{
    		status = "User: " + user.getName();
    	}*/


	}

	/**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {

    	//initializeCreateButton();
    	initializeCreateButton();
    	initializeLoadButton();
    	initializeSettingsButton();
    	initializeAboutButton();

    }

    public void initializeSettingsButton(){
    	settings.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	BorderPane page = null;
            	try {
	                FXMLLoader loader = new FXMLLoader();
	                loader.setLocation(MainApp.class.getResource("view/Settings.fxml"));
	                page = (BorderPane) loader.load();


                    // Cria o palco dialogStage.
                    Stage dialogStage = new Stage();
                    dialogStage.setTitle("Settings");
                    dialogStage.initModality(Modality.WINDOW_MODAL);
                    dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
                    UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
                    dialogStage.setScene(scene);

                    dialogStage.sizeToScene();
                    dialogStage.toFront();

                    Undecorator undecorator = scene.getUndecorator();
                    dialogStage.setMinWidth(undecorator.getMinWidth());
                    dialogStage.setMinHeight(undecorator.getMinHeight());

                    // Define a pessoa no controller.
                    SettingsController controller = loader.getController();
                    controller.setDialogStage(dialogStage);
                    /*controller.setPerson(person);*/

                    // Mostra a janela e espera at� o usu�rio fechar.
                    dialogStage.showAndWait();

                    if(controller.isOkClicked()){
                    	System.out.println("OkClicked");
                    }




            	//if(createProject())
            	//	ApplicationContext.getContext().getMainApp().setScene("RunAlgorithm");

            	} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

            }


    	});
    }

    public void initializeAboutButton(){
    	about.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	InformationBox informationBox = new InformationBox("About", "This software was developed by Thiago Nepomuceno. "
            														+ "The main goal here is allow people that research or just use Genetic Algorithms"
            														+ " to do faster formulations and tests.", 14);

            	informationBox.show();
            }

    	});
    }

    private void initializeCreateButton(){
    	createProject.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                	// Carrega o arquivo fxml e cria um novo stage para a janela popup.
                	BorderPane page = null;
                	try {
		                FXMLLoader loader = new FXMLLoader();
		                loader.setLocation(MainApp.class.getResource("view/CreateProject.fxml"));
		                page = (BorderPane) loader.load();


	                    // Cria o palco dialogStage.
	                    Stage dialogStage = new Stage();
	                    dialogStage.setTitle("Create Project");
	                    dialogStage.initModality(Modality.WINDOW_MODAL);
	                    dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
	                    UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
	                    dialogStage.setScene(scene);

	                    dialogStage.sizeToScene();
	                    dialogStage.toFront();

	                    Undecorator undecorator = scene.getUndecorator();
	                    dialogStage.setMinWidth(undecorator.getMinWidth());
	                    dialogStage.setMinHeight(undecorator.getMinHeight());

	                    // Define a pessoa no controller.
	                    CreateProjectController controller = loader.getController();
	                    controller.setDialogStage(dialogStage);
	                    /*controller.setPerson(person);*/

	                    // Mostra a janela e espera at� o usu�rio fechar.
	                    dialogStage.showAndWait();

	                    if(controller.isOkClicked()){
	                    	ApplicationContext.getContext().getMainApp().loadScene("RunAlgorithm");
	                    	ApplicationContext.getContext().getMainApp().setScene();
	                    }




                	//if(createProject())
                	//	ApplicationContext.getContext().getMainApp().setScene("RunAlgorithm");

                	} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

                }

       });
    }

    private void initializeLoadButton(){
    	loadProject.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                	if(loadProject()){
                		ApplicationContext.getContext().getMainApp().loadScene("MainMenu");
                		ApplicationContext.getContext().getMainApp().setScene();
                	}


                }

       });
    }


    private boolean loadProject(){
    	FileChooser chooser = new FileChooser();
    	chooser.setTitle("Create a new project");

    	if(!ApplicationContext.getContext().getMainApp().getGeneralSettings().getLastProjectLoaded().exists())
    		ApplicationContext.getContext().getMainApp().getGeneralSettings().setLastProjectLoaded(new File("C:\\Users\\"));;

    	File defaultDirectory = ApplicationContext.getContext().getMainApp().getGeneralSettings().getLastProjectLoaded();

    	chooser.setInitialDirectory(defaultDirectory);
    	File selectedFile = chooser.showOpenDialog(ApplicationContext.getContext().getMainApp().getStage());

    	if(selectedFile != null && selectedFile.getName().equals("project.gao")){
    		ApplicationContext.getContext().getProject().setPath(selectedFile.getParentFile());
    		ApplicationContext.getContext().getMainApp().getGeneralSettings().setLastProjectLoaded(selectedFile.getParentFile());
    		ApplicationContext.getContext().getMainApp().getGeneralSettings().save();
    		return ApplicationContext.getContext().getProject().load(selectedFile);

    	}

    	return false;
    }

}
