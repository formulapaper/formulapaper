package magius;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import application.ApplicationContext;
import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.mocell.MOCell;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.util.Configuration;
import jmetal.util.JMException;

public class FormulaPaperExperiments {

	static Problem   problem   ; // The problem to solve
	static Algorithm algorithm ; // The algorithm to use
	static Operator  crossover ; // Crossover operator
	static Operator  mutation  ; // Mutation operator
	static Operator  selection ; // Selection operator

	static HashMap  parameters ; // Operator parameters

	static void metricToFile(String value, File file, boolean append){
    	try {
	      /* Open the file */
    	  FileWriter fw = new FileWriter(file, append);
	      BufferedWriter bw = new BufferedWriter(fw);

	      bw.write(value);

	      if(append)
	    	  bw.newLine();

	      /* Close the file */
	      bw.close();
	    }catch (IOException e) {
	      Configuration.logger_.severe("Error acceding to the file");
	      e.printStackTrace();
	    }
    }


	public static void main(String [] args) throws JMException, ClassNotFoundException, IOException{

		List<String> list = new ArrayList<String>();

		list.add("TSP_disk_100.inst");
		list.add("TSP_disk_500.inst");
		//list.add("TSP_disk_1000.inst");
		list.add("TSP_crane_100.inst");
		list.add("TSP_crane_500.inst");
		//list.add("TSP_crane_1000.inst");


		List<Integer> evaluationInput = new ArrayList<Integer>();

		//evaluationInput.add(100000);
		evaluationInput.add(500000);

		List<Integer> populationInput = new ArrayList<Integer>();

		populationInput.add(128);
		//populationInput.add(256);


		String instance = "";

		//Clear all files
		for(int alg = 0; alg < 2; ++alg){
			for(int inst = 0; inst < list.size(); ++inst){
				for(int conf = 0; conf < evaluationInput.size(); ++conf){
					for(int pop = 0; pop < populationInput.size(); ++pop){
						instance = list.get(inst);

						File timeMetric = new File("FormulaPaperResults\\" + (alg == 0 ? "NSGA-II" : "MoCell") + "_[" + instance + "]_[" + evaluationInput.get(conf) + "]_[" + populationInput.get(pop) + "].time");
					    timeMetric.getParentFile().mkdirs();
					    timeMetric.createNewFile();

					    metricToFile("", timeMetric, false);
					}
				}
			}
		}

		for(int alg = 0; alg < 2; ++alg){
			for(int inst = 0; inst < list.size(); ++inst){
				for(int conf = 0; conf < evaluationInput.size(); ++conf){
					for(int pop = 0; pop < populationInput.size(); ++pop){
						instance = list.get(inst);
						problem = new TSPFormula("C:\\Users\\thine\\Dropbox\\Projects\\FormulaOptimizer\\TestProject\\Instances\\TSP\\" + instance);

						algorithm = (alg == 0 ? new NSGAII(problem) : new MOCell(problem) );

						// Algorithm parameters
					    algorithm.setInputParameter("populationSize",populationInput.get(pop));
					    algorithm.setInputParameter("archiveSize",populationInput.get(pop));
					    algorithm.setInputParameter("maxEvaluations",evaluationInput.get(conf));

					    // Mutation and Crossover for Real codification
					    parameters = new HashMap() ;
					    parameters.put("probability", 0.8) ;
					    parameters.put("distributionIndex", 20.0) ;
					    crossover = CrossoverFactory.getCrossoverOperator("PMXCrossover", parameters);

					    parameters = new HashMap() ;
					    parameters.put("probability", 1.0/problem.getNumberOfVariables()) ;
					    parameters.put("distributionIndex", 20.0) ;
					    mutation = MutationFactory.getMutationOperator("SwapMutation", parameters);

					    // Selection Operator
					    parameters = null ;
					    selection = SelectionFactory.getSelectionOperator("BinaryTournament", parameters) ;

					    // Add the operators to the algorithm
					    algorithm.addOperator("crossover",crossover);
					    algorithm.addOperator("mutation",mutation);
					    algorithm.addOperator("selection",selection);

					    File timeMetric = new File("FormulaPaperResults\\" + (alg == 0 ? "NSGA-II" : "MoCell") + "_[" + instance + "]_[" + evaluationInput.get(conf) + "]_[" + populationInput.get(pop) + "].time");
					    timeMetric.getParentFile().mkdirs();
					    timeMetric.createNewFile();

					    System.out.println((alg == 0 ? "NSGA-II" : "MoCell"));
					    System.out.println(instance);
					    System.out.println("Evaluations: " + evaluationInput.get(conf));
					    System.out.println("Population: " + populationInput.get(pop));

					    //Repetitions: 30
					    int repetitions = 30;

					    double sum = 0.0;
					    double mean = 0 ;
					    List<Long> value = new ArrayList<Long>();
					    for(int i=0;i<repetitions;++i){
						    // Execute the Algorithm
						    long initTime = System.currentTimeMillis();
						    SolutionSet population = algorithm.execute();
						    long estimatedTime = System.currentTimeMillis() - initTime;

						    value.add(estimatedTime);
						    sum += estimatedTime;

						    System.out.println(estimatedTime);
						    metricToFile(String.valueOf(estimatedTime), timeMetric, true);
					    }

					    mean = sum / repetitions;

					    System.out.println("Mean: " + new DecimalFormat("#0.00000").format(mean));
					    metricToFile("Mean: " + new DecimalFormat("#0.00000").format(mean), timeMetric, true);

					    double standart_deviation = 0;
					    for(int i=0; i < value.size(); ++i){
					    	standart_deviation += Math.pow(value.get(i) - mean, 2);
					    	//System.out.println(value.get(i) + " " + mean + " " + Math.pow(value.get(i) - mean, 2));
					    }
					    standart_deviation = Math.sqrt(standart_deviation);

					    System.out.println("SD: " + new DecimalFormat("#0.00000").format(standart_deviation));
					    metricToFile("SD: " + new DecimalFormat("#0.00000").format(standart_deviation), timeMetric, true);
					}
				}
			}
		}

	}

}

