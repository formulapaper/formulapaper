package magius;

import java.io.Serializable;

/**
 * Created by ThiNepo on 17/07/2014.
 */
public class GeneralVariable implements Serializable{

    public String type; //Normal, Vector, Matrix
    public String name;
    String texToken;

    public int dimension1, dimension2;
    private String variableDimension1, variableDimension2;

    public int data;
    public int vectorData[] = null;
    public int matrixData[][] = null;

    public GeneralVariable(String _type, String _name){
        type = _type;
        name = _name;
        data = 0;
        dimension1 = 0;
        dimension2 = 0;

        variableDimension1 = variableDimension2 = "";
    }

    public GeneralVariable(String _type, String _name, int _dimension1){
        type = _type;
        name = _name;
        data = 0;
        dimension1 = _dimension1;
        dimension2 = 0;

        vectorData = new int[dimension1];

        variableDimension1 = variableDimension2 = "";
    }

    public GeneralVariable(String _type, String _name, int _dimension1, int _dimension2){
        type = _type;
        name = _name;
        data = 0;
        dimension1 = _dimension1;
        dimension2 = _dimension2;

        matrixData = new int[dimension1][dimension2];

        variableDimension1 = variableDimension2 = "";
    }

    public void resize(int _dimension1){
    	dimension1 = _dimension1;

    	vectorData = new int[dimension1];
    }

    public void resize(int _dimension1, int _dimension2){
    	dimension1 = _dimension1;
        dimension2 = _dimension2;

        matrixData = new int[dimension1][dimension2];
    }

    public String getTex(){
        return name;
    }

    @Override
    public String toString(){
    	String showString = name + " (" + type + ")";
    	return showString;
    }

	public String getVariableDimension2() {
		return variableDimension2;
	}

	public void setVariableDimension2(String variableDimension2) {
		this.variableDimension2 = variableDimension2;
	}

	public String getVariableDimension1() {
		return variableDimension1;
	}

	public void setVariableDimension1(String variableDimension1) {
		this.variableDimension1 = variableDimension1;
	}



}
