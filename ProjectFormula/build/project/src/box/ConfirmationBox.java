package box;

import java.io.IOException;

import application.ApplicationContext;
import application.MainApp;
import application.view.ConfirmationBoxController;
import application.view.CreateProjectController;
import application.view.InformationBoxController;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import undecorator.insidefx.undecorator.Undecorator;
import undecorator.insidefx.undecorator.UndecoratorScene;

public class ConfirmationBox {

	private String title;
	private String text;
	private double fontSize;

	public ConfirmationBox(String _title, String _text, double _fontSize){
		title = _title;
		text = _text;
		fontSize = _fontSize;
	}

	//return if clicked in Ok button or not
	public boolean show(){
		BorderPane page = null;
    	try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/ConfirmationBox.fxml"));
            page = (BorderPane) loader.load();


            // Cria o palco dialogStage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle(title);
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
            UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
            dialogStage.setScene(scene);

            dialogStage.sizeToScene();
            dialogStage.toFront();

            Undecorator undecorator = scene.getUndecorator();
            dialogStage.setMinWidth(undecorator.getMinWidth());
            dialogStage.setMinHeight(undecorator.getMinHeight());

            // Define a pessoa no controller.
            ConfirmationBoxController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            /*controller.setPerson(person);*/

            controller.setFontSize(fontSize);
            controller.setText(text);


            // Mostra a janela e espera at� o usu�rio fechar.
            dialogStage.showAndWait();

            return controller.isOk();

    	//if(createProject())
    	//	ApplicationContext.getContext().getMainApp().setScene("RunAlgorithm");

    	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			return false;
		}


	}

}
