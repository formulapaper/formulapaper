package application.view;

import java.io.File;

import com.sun.prism.paint.Color;

import application.ApplicationContext;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class CreateProjectController {

	@FXML
	private TextField projectName;

	@FXML
	private TextField projectPath;

	@FXML
	private Button cancelButton;

	@FXML
	private Button okButton;

	@FXML
	private Button findDirectory;

	boolean okButtonClicked;

	private Stage dialogStage;

	File selectedDirectory;

	public CreateProjectController() {
		okButtonClicked = false;
		selectedDirectory = new File("C:\\Users\\");
	}

	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

	/**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {
    	okButtonController();
    	cancelButtonController();
    	findButtonController();

    	projectName.setText("");
    	projectPath.setText(selectedDirectory.getPath().toString());
    }

    public boolean isOkClicked(){
    	return okButtonClicked;
    }

    private void okButtonController(){
    	okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            	//Validate
            	if(!projectName.getText().equals("") && selectedDirectory != null && selectedDirectory.exists()){
        	    	//Create Problems folder
            		File createFolder = new File(selectedDirectory + "\\" + projectName.getText().toString());
            		if(!createFolder.exists()) createFolder.mkdir();

            		selectedDirectory = new File(selectedDirectory + "\\" + projectName.getText().toString());

            		createFolder = new File(selectedDirectory + "\\Problems");
            		if(!createFolder.exists()) createFolder.mkdir();

        	    	//Create Solutions folder
            		createFolder = new File(selectedDirectory + "\\Solutions");
            		if(!createFolder.exists()) createFolder.mkdir();

            		//Create Intances folder
            		createFolder = new File(selectedDirectory + "\\Instances");
            		if(!createFolder.exists()) createFolder.mkdir();

            		ApplicationContext.getContext().getProject().setName( selectedDirectory.getName() );
            		ApplicationContext.getContext().getProject().setPath(selectedDirectory);
            		ApplicationContext.getContext().getProject().save();

            		okButtonClicked = true;

            		dialogStage.close();

            	}else{
            		if(projectName.getText().equals("")){
            			projectName.setStyle("-fx-background-color: #FF5B5E;");
            		}

            		if(selectedDirectory != null && selectedDirectory.exists()){
            			projectPath.setStyle("-fx-background-color: #FF5B5E;");
            		}
            	}


            }
    	});
    }

    private void cancelButtonController(){
    	cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	dialogStage.close();
            }
    	});
    }

    private void findButtonController(){
    	findDirectory.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	selectDirectory();
            }
    	});
    }

    private void selectDirectory(){
    	DirectoryChooser chooser = new DirectoryChooser();
    	chooser.setTitle("Create a new project");
    	File defaultDirectory = new File("C:\\Users\\");
    	chooser.setInitialDirectory(defaultDirectory);
    	selectedDirectory = chooser.showDialog(ApplicationContext.getContext().getMainApp().getStage());

    	projectPath.setText(selectedDirectory.getPath().toString());
    }

    /*private boolean createProject(){


    	if(selectedDirectory != null){
	    	//Create Problems folder
    		File createFolder = new File(selectedDirectory + "\\Problems");
    		if(!createFolder.exists()) createFolder.mkdir();

	    	//Create Solutions folder
    		createFolder = new File(selectedDirectory + "\\Solutions");
    		if(!createFolder.exists()) createFolder.mkdir();

    		//Create Intances folder
    		createFolder = new File(selectedDirectory + "\\Instances");
    		if(!createFolder.exists()) createFolder.mkdir();

    		ApplicationContext.getContext().getProject().setName( selectedDirectory.getName() );
    		ApplicationContext.getContext().getProject().setPath(selectedDirectory);
    		ApplicationContext.getContext().getProject().save();

    	}

    	return selectedDirectory != null;
    }*/

}
