package magius;

import java.io.Serializable;

/**
 * Created by ThiNepo on 17/07/2014.
 */
public class Loop implements Serializable{
    public String identifier;
    public String type;

    //Loop 0...N-1
    public int count;
    public int initial;
    public int N;

    private String initialVariable;
    private String finalVariable;

    public Loop(String _type, String _identifier, int _initial, int _N){
        type = _type;
        identifier = _identifier + "";
        initial = _initial;
        N = _N;
        count = 0;

        //Variáveis associadas
        setInitialVariable("");
        setFinalVariable("");
    }



    public String getTex(){

        return ("\\"+type+"\\limits_{" + identifier + "=" +
        		(initialVariable.equals("") ? initial : initialVariable)
        		+ "}^{" +
        		(finalVariable.equals("") ? N : finalVariable)
        		+ "}[");
    }



	public String getInitialVariable() {
		return initialVariable;
	}



	public void setInitialVariable(String initialVariable) {
		this.initialVariable = initialVariable;
	}



	public String getFinalVariable() {
		return finalVariable;
	}



	public void setFinalVariable(String finalVariable) {
		this.finalVariable = finalVariable;
	}
}

