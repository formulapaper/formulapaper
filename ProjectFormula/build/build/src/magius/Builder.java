package magius;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import application.ApplicationContext;
import box.InformationBox;
import jmetal.core.Problem;
import jmetal.encodings.variable.Permutation;

//TODO: For now, only build to Integer type variables. Need modify to Real (double) variables

public class Builder {

	GeneralProblem problem;

	PrintWriter w;

	List<GeneralVariable> normal;
	List<GeneralVariable> vector;
	List<GeneralVariable> matrix;

	public Builder(GeneralProblem generalProblem){
		this.problem = generalProblem;

		normal = new ArrayList<GeneralVariable>();
		vector = new ArrayList<GeneralVariable>();
		matrix = new ArrayList<GeneralVariable>();
	}

	public void build() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException{
		File output	= new File(ApplicationContext.getContext().getProject().getPath() + "\\Problems\\" + problem.getName() + ".java");

		if(!output.exists())
			output.createNewFile();

		w = new PrintWriter(output, "UTF-8");

		//Print libraries
		libraries();

		//Line------------------------------
		w.println("");

		w.println("public class " + problem.getName() + " extends Problem {");

		//Line------------------------------
		w.println("");

		variables();

		//Line------------------------------
		w.println("");

		w.println("public " + problem.getName() + "(String filename) throws FileNotFoundException{");

		w.println("Scanner scn  = new Scanner (new File (filename));");

		readInstance();

		parameters();

		constructorCustomCode();

		w.println("}");

		//Line------------------------------
		w.println("");

		evaluate();

		evaluateConstraints();

		w.println("}");

		w.close();

		compile(problem.getName(), ApplicationContext.getContext().getProject().getPath() + "\\Instances\\" + problem.getName() + "\\teste.txt");

	}

	private void parameters(){
		if(problem.getVariableType() == "Permutation"){
			w.println("numberOfVariables_ = 1;");
		}else{
			w.println("numberOfVariables_ = N;");
		}
		w.println("numberOfObjectives_ = " + problem.getNumberOfObjectives() + ";");
		w.println("numberOfConstraints_ = " + problem.getNumberOfConstraints() + ";");
		w.println("problemName_ = \"" + problem.getName() + "\";");

		//Adicionado
		w.println("upperLimit_ = new double[numberOfVariables_] ;");
	    w.println("lowerLimit_ = new double[numberOfVariables_] ;");

	    w.println("for (int i = 0; i < numberOfVariables_; i++) {");
	    	w.println("lowerLimit_[i] = " + problem.getGlobalLowerLimit() + ";");
	    	w.println("upperLimit_[i] = " + problem.getGlobalUpperLimit() + ";");
	    w.println("}");

		w.println("try{");
			//solutionType_ = new BinarySolutionType(this);
			if(problem.getVariableType() == "Real"){
				w.println("solutionType_ = new RealSolutionType(this);");
			}else if(problem.getVariableType() == "Permutation"){
				w.println("solutionType_ = new PermutationSolutionType(this);");
				w.println("length_       = new int[numberOfVariables_];");
				w.println("length_[0]      = N;");
			}else{
				w.println("solutionType_ = new IntSolutionType(this);");
			}

		w.println("}");
		w.println("catch (Exception e){");
			w.println("e.printStackTrace();");
		w.println("}");
	}

	private void evaluate(){
		w.println("@Override");
		w.println("public void evaluate(Solution solution) throws JMException {");
		w.println("double obj[] = new double[" + problem.getNumberOfObjectives() + "];");

		w.println("Variable _variable[] = solution.getDecisionVariables();");

		//TODO: Need change to accept each type
		if(problem.getVariableType() == "Real"){
			w.println("Double X[] = new Double[_variable.length];");
			w.println("for(int ii = 0;ii < X.length;ii++)");
			w.println("X[ii] = (double) _variable[ii].getValue();");
		}else if(problem.getVariableType() == "Permutation"){
			w.println("Integer X[] = new Integer[N];");
			w.println("for(int ii = 0;ii < X.length;ii++)");
			w.println("X[ii] = ((Permutation)solution.getDecisionVariables()[0]).vector_[ii];");
		}else{
			w.println("Integer X[] = new Integer[_variable.length];");
			w.println("for(int ii = 0;ii < X.length;ii++)");
			w.println("X[ii] = (int) _variable[ii].getValue();");
		}



		for(int i=0;i<problem.getNumberOfObjectives();++i){
			process("obj[" + i + "]",problem.objective[i], i, 0);

			String customCode = problem.customCodeObjective[i];
			customCode = customCode.replace("OBJ", "obj[" + i + "]");

			w.println(customCode);

			w.println("solution.setObjective(" + i + ", " + "obj[" + i + "]);");
		}
		w.println("}");
	}

	private void evaluateConstraints(){
		w.println("@Override");
		w.println("public void evaluateConstraints(Solution solution) throws JMException {");

		w.println("double cons1[] = new double[" + problem.getNumberOfConstraints() + "];");
		w.println("double cons2[] = new double[" + problem.getNumberOfConstraints() + "];");
		w.println("int penality[] = new int[" + problem.getNumberOfConstraints() + "];");

		w.println("Variable _variable[] = solution.getDecisionVariables();");

		//TODO: Need change to accept each type
		if(problem.getVariableType() == "Real"){
			w.println("Double X[] = new Double[_variable.length];");
			w.println("for(int ii = 0;ii < X.length;ii++)");
			w.println("X[ii] = (double) _variable[ii].getValue();");
		}else if(problem.getVariableType() == "Permutation"){
			w.println("Integer X[] = new Integer[N];");
			w.println("for(int ii = 0;ii < X.length;ii++)");
			w.println("X[ii] = ((Permutation)solution.getDecisionVariables()[0]).vector_[ii];");
		}else{
			w.println("Integer X[] = new Integer[_variable.length];");
			w.println("for(int ii = 0;ii < X.length;ii++)");
			w.println("X[ii] = (int) _variable[ii].getValue();");
		}

		w.println("solution.setNumberOfViolatedConstraint(0);");
		w.println("solution.setOverallConstraintViolation(0);");

		for(int i=0;i<problem.getNumberOfConstraints();++i){
			w.println("penality[" + i + "] = 0;");

			process("cons1[" + i + "]",problem.constraintPart1[i], i, 0);

			process("cons2[" + i + "]",problem.constraintPart2[i], i, 0);

			w.println("if(!(" + "cons1[" + i + "]"  + problem.constraintType.get(i) +  "cons2[" + i + "]))");
			w.println("penality[" + i + "]++;");

			String customCode = problem.customCodeConstraint[i];
			customCode = customCode.replace("PENALITY", "penality[" + i + "]");

			w.println(customCode);

			w.println("solution.setNumberOfViolatedConstraint(penality[" + i + "]);");
			w.println("solution.setOverallConstraintViolation(penality[" + i + "] * -1);");
		}

		w.println("}");
	}



	int process(String var, ArrayList<String> obj, int objIdx, int idx){
		List<String> list = new ArrayList<String>();

		//boolean finish = false;

		while(idx < obj.size()){
			//If start a loop here
			if(obj.get(idx).equals("]")){
				//finish = true;
				break;
			}else if(obj.get(idx).startsWith("L")){
				list.add(obj.get(idx) + 1);
				idx = processLoop(obj, objIdx, idx);
			}else if(obj.get(idx).startsWith("V")){ //If it is a variable
				int varIndex = Integer.valueOf(obj.get(idx).substring(1));
				if(problem.variable.get(varIndex).type.equals("Normal")){
					list.add(problem.variable.get(varIndex).name);
				}else if(problem.variable.get(varIndex).type.equals("Vector")){
					list.add(problem.variable.get(varIndex).name);
					list.add("[" + obj.get(++idx) + "]");
				}else if(problem.variable.get(varIndex).type.equals("Matrix")){
					list.add(problem.variable.get(varIndex).name);
					list.add("[" + obj.get(++idx) + "]");
					list.add("[" + obj.get(++idx) + "]");
				}
			}else{
				list.add(obj.get(idx));
			}

			System.out.println(idx + " " + obj.size());

			idx++;
		}

		//w.print("obj[" + objIdx + "] = ");
		w.print(var + " = ");
		System.out.println(list.size());
		for(int i=0;i<list.size();++i){
			w.print(list.get(i));
		}
		w.println(";");

		/*if(finish){
			w.println("}");
		}*/

		return idx;
	}

	int processLoop(ArrayList<String> obj, int objIdx, int idx){

		int index = Integer.valueOf(obj.get(idx).substring(1));
		Loop loop = problem.loop.get(index);

		String loopName = obj.get(idx);

		if(loop.type.equals("sum")){
			w.println("double " + loopName + 1 + " = 0.0;");
		}else if(loop.type.equals("prod")){
			w.println("double " + loopName + 1 + " = 1.0;");
		}

		w.println("for(int " + loop.identifier + "=" + (loop.getInitialVariable().equals("") ? loop.initial : loop.getInitialVariable()) +
				   ";" + loop.identifier + "<=" + (loop.getFinalVariable().equals("") ? loop.initial : loop.getFinalVariable()) + ";" +
				   loop.identifier + "++){");


		w.println("double " + obj.get(idx) + 2 + ";");
		idx = process(obj.get(idx) + 2, obj, objIdx , idx + 1);

		if(loop.type.equals("sum")){
			w.println(loopName + 1 + " += " + loopName + 2 + ";");
		}else if(loop.type.equals("prod")){
			w.println(loopName + 1 + " *= " + loopName + 2 + ";");
		}

		w.println("}");

		return idx;
	}

	//Only for Normal and Vector for now. NOT Matrix
	private void readInstance(){
		for(String s : problem.readList){
			for(int i=0;i<problem.variable.size();++i){
				GeneralVariable gv = problem.variable.get(i);
				if(s.equals(gv.name)){
					//If will read a normal variable
					if(gv.type.equals("Normal")){
						w.println(gv.name + " = scn.nextInt();");
					}else if(gv.type.equals("Vector")){
						//create vector
						String D1 = (gv.getVariableDimension1().equals("") ? String.valueOf(gv.dimension1): gv.getVariableDimension1());
						w.println(gv.name + " = new int[" + D1 + "];");
						w.println("for(int i=0;i<" +  D1 + "; ++i){");
							w.println(gv.name + "[i] = scn.nextInt();");
						w.println("}");
					}else if(gv.type.equals("Matrix")){
						//create vector
						//if(!gv.getVariableDimension1().equals("") && !gv.getVariableDimension2().equals("")){
						String D1 = (gv.getVariableDimension1().equals("") ? String.valueOf(gv.dimension1): gv.getVariableDimension1());
						String D2 = (gv.getVariableDimension2().equals("") ? String.valueOf(gv.dimension2): gv.getVariableDimension2());
							w.println(gv.name + " = new int[" + D1 + "][" + D2 + "];");
							w.println("for(int i=0;i<" +  D1 + "; ++i){");
							w.println("for(int j=0;j<" +  D2 + "; ++j){");
								w.println(gv.name + "[i][j] = scn.nextInt();");
							w.println("}");
							w.println("}");
						//}
					}
				}
			}
		}
	}

	private void libraries(){
		w.println("import java.io.File;");
		w.println("import java.io.FileNotFoundException;");
		w.println("import java.io.FileWriter;");
		w.println("import java.io.IOException;");
		w.println("import java.util.Scanner;");

		w.println("import jmetal.core.*;");
		w.println("import jmetal.encodings.solutionType.ArrayRealSolutionType;");
		w.println("import jmetal.encodings.solutionType.BinaryRealSolutionType;");
		w.println("import jmetal.encodings.solutionType.IntSolutionType;");
		w.println("import jmetal.encodings.solutionType.PermutationSolutionType;");
		w.println("import jmetal.encodings.solutionType.RealSolutionType;");
		w.println("import jmetal.encodings.variable.Permutation;");
		w.println("import jmetal.util.JMException;");
		w.println("import jmetal.util.Configuration.*;");
		w.println("import jmetal.util.wrapper.XReal;");
	}

	private void variables(){
		normal.clear();
		vector.clear();
		matrix.clear();

		for(GeneralVariable gv : problem.variable){
			if(gv.type.equals("Normal")){
				normal.add(gv);
			}else if(gv.type.equals("Vector")){
				vector.add(gv);
			}else if(gv.type.equals("Matrix")){
				matrix.add(gv);
			}
		}

		//Normal
		for(GeneralVariable gv : normal){
			w.println("int " + gv.name + ";");
		}

		//Vector
		for(GeneralVariable gv : vector){
			if(!gv.name.equals("X"))
				w.println("int " + gv.name + "[];");
		}

		//Matrix
		for(GeneralVariable gv : matrix){
			w.println("int " + gv.name + "[][];");
		}

		String customCode = problem.customCodeVariables;
		w.println(customCode);
	}

	public void constructorCustomCode(){
		String customCode = problem.customCodeConstructor;
		w.println(customCode);
	}


	//Just for internal test
	public Problem compile(String problemName, String instancePath){
    	// Prepare source somehow.
    	Problem problem = null;

    	// Save source in .java file.
    	File root = new File(ApplicationContext.getContext().getProject().getPath() + "\\Problems\\");
    	File sourceFile = new File(root, problemName + ".java");
    	//sourceFile.getParentFile().mkdirs();
    	//Files.write(sourceFile.toPath(), source, StandardCharsets.UTF_8);

    	// Compile source file.
    	JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
    	//System.out.println(sourceFile.getPath());

    	ByteArrayOutputStream errorStream = new ByteArrayOutputStream ();
    	int status = compiler.run(null, null, errorStream, sourceFile.getPath());

    	if(status != 0){
	    	InformationBox errorBox = new InformationBox("Build Error", errorStream.toString(), 10);

	    	errorBox.show();
    	}else{
    		InformationBox okBox = new InformationBox("Build Success", "Your formulation was builded successfully.", 18);

	    	okBox.show();
    	}

    	/*URL url = root.toURL();          // file:/c:/myclasses/
        URL[] urls = new URL[]{url};

        ClassLoader cl = new URLClassLoader(urls);
        Class cls = cl.loadClass(problemName);
        System.out.println(cls.getName());
        Constructor constructor = cls.getConstructor(String.class);
        problem = (Problem) constructor.newInstance(instancePath);*/

        return problem;

    	// Load and instantiate compiled class.
    	/*URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] { root.toURI().toURL() });
    	Class<?> cls = Class.forName("Test", true, classLoader); // Should print "hello".
    	Object instance = cls.newInstance(); // Should print "world".
    	System.out.println(instance); // Should print "test.Test@hashcode".*/
    }
}
