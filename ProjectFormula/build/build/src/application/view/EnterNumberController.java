package application.view;

import java.io.File;

import com.sun.prism.paint.Color;

import application.ApplicationContext;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class EnterNumberController {

	@FXML
	private TextField number;

	@FXML
	private Button cancelButton;

	@FXML
	private Button okButton;

	boolean okButtonClicked;

	private Stage dialogStage;


	public EnterNumberController() {
		okButtonClicked = false;
	}

	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

	/**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {
    	okButtonController();
    	cancelButtonController();

    	number.setText("");
    }

    public boolean isOkClicked(){
    	return okButtonClicked;
    }

    public String getNumber(){
    	return number.getText();
    }

    private boolean isInteger(){
    	try
    	  {
    	    Integer value = Integer.parseInt(number.getText());
    	  }
    	  catch(NumberFormatException nfe)
    	  {
    	    return false;
    	  }
    	  return true;
    }

    private void okButtonController(){
    	okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            	//Validate
            	if(!number.getText().equals("") && isInteger()){

            		okButtonClicked = true;

            		dialogStage.close();

            	}else{
            			number.setStyle("-fx-background-color: #FF5B5E;");
            	}


            }
    	});
    }

    private void cancelButtonController(){
    	cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	dialogStage.close();
            }
    	});
    }

}
