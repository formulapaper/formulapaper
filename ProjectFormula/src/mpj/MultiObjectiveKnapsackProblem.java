package mpj;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import jmetal.core.*;
import jmetal.encodings.solutionType.ArrayRealSolutionType;
import jmetal.encodings.solutionType.BinaryRealSolutionType;
import jmetal.encodings.solutionType.IntSolutionType;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.util.JMException;
import jmetal.util.Configuration.*;
import jmetal.util.wrapper.XReal;

public class MultiObjectiveKnapsackProblem extends Problem {
	public int n_itens, n_charac;

	public int C[][]; //Valor de cada item
	public int W[][]; //Peso, volume, etc. de cada item.

	public int wMax[];

	int quantRun = 0; //Quantas vezes o algoritimo rodou

	public int quantExactsCreated;

	public double A[][];
	public double b[];
	public double c[];

	public MultiObjectiveKnapsackProblem(String filename) throws FileNotFoundException{
		Scanner scn  = new Scanner (new File (filename));

		quantExactsCreated = 0;

		numberOfObjectives_ = scn.nextInt();

		n_itens = scn.nextInt();
		n_charac = scn.nextInt();

		C = new int[numberOfObjectives_][n_itens];
		W = new int[n_charac][n_itens];

		for(int i = 0;i < numberOfObjectives_;i++){
			for(int j=0; j < n_itens; j++){
				C[i][j] = scn.nextInt();
			}
		}

		for(int i = 0;i < n_charac;i++){
			for(int j=0; j < n_itens; j++){
				W[i][j] = scn.nextInt();
			}
		}

		wMax = new int[n_charac];
		for(int i = 0;i < n_charac;i++){
			wMax[i] = scn.nextInt();
		}


		numberOfVariables_ = n_itens;
		numberOfConstraints_ = n_charac;
		problemName_ = "MultiObjectiveKnapsackProblem";

		//Adicionado
		upperLimit_ = new double[numberOfVariables_] ;
	    lowerLimit_ = new double[numberOfVariables_] ;

	    for (int i = 0; i < numberOfVariables_; i++) {
	      lowerLimit_[i] = 0 ;
	      upperLimit_[i] = 1 ;
	    }

		try{
			//solutionType_ = new BinarySolutionType(this);
			solutionType_ = new IntSolutionType(this);
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void evaluate(Solution solution) throws JMException {

		Variable _variable[] = solution.getDecisionVariables();

		Integer variable[] = new Integer[_variable.length];
		for(int i = 0;i < variable.length;i++)
			variable[i] = (int) _variable[i].getValue();

		double obj;
		for(int i = 0; i < numberOfObjectives_; i++){
			obj = 0.0;
			for(int j = 0;j < n_itens;j++){
				if(variable[j] == 1){
					obj += C[i][j];
				}
			}
			solution.setObjective(i, - obj);
		}

	}


	@Override
	public void evaluateConstraints(Solution solution) throws JMException {
		Variable _variable[] = solution.getDecisionVariables();
		int number = 0;

		Integer variable[] = new Integer[_variable.length];
		for(int i = 0;i < variable.length;i++)
			variable[i] = (int) _variable[i].getValue();

		solution.setNumberOfViolatedConstraint(0);
		solution.setOverallConstraintViolation(0);

		int cost;
		for(int i = 0; i < n_charac; i++ ){
			cost = 0;
			for(int j = 0;j < n_itens;j++){
				if(variable[j] == 1){
					cost += W[i][j];
				}
			}
			//System.out.println("W" + i + " : " + cost + "/" + wMax[i]);
			if(cost > wMax[i]) number++;
		}

		//if(number==0) System.out.println(number);
		solution.setNumberOfViolatedConstraint(number);
		solution.setOverallConstraintViolation((number) * -1);
	}

	public Variable[] fixSolution(Solution solution) throws JMException{
		Variable _variable[] = solution.getDecisionVariables();

		_variable[(int) Math.random() * solution.numberOfVariables()].setValue(0);

		return _variable;

	}
}