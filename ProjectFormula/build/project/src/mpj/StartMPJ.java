package mpj;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Random;

import application.ApplicationContext;
import application.model.AlgorithmFactory;
import application.model.AlgorithmParameters;
import jmetal.core.Algorithm;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.util.JMException;
import jmetal.util.Ranking;
import jmetal.util.comparators.CrowdingComparator;
import mpi.*;
import mpj.StartMPJ.TAG;
import mpjdev.javampjdev.MPJDev;

public class StartMPJ {

	 public enum AlgorithmName {
		    NONE, NSGA, MoCell
	}

	 public enum TAG {
		    SOLUTION (0), FINISH (1);

		 	public int code;

		 	TAG(int code) {
		        this.code = code;
		    }
	}

	public static void main(String args[]) {
	 MPI.Init(args);
	 int me = MPI.COMM_WORLD.Rank();
	 int size = MPI.COMM_WORLD.Size();

	 int root = 0;

	 System.out.println("Hi from <"+me+">");

		if(me == root && size != 1){

			Solution solution[] = new Solution[512];

			int value[] = new int[1];

			SolutionSet result = new SolutionSet(512);

			Status status;
			Request sendingRequest[] = new Request[size];

			CrowdingComparator comparationOperator = new CrowdingComparator();

			int finished = 0;
			while(finished < (size-1)){
				for(int i=1;i<size;++i){

					//Receive finisth
					Status finishStatus = MPI.COMM_WORLD.Iprobe(i, TAG.FINISH.code);

					if(finishStatus != null){
						if(finishStatus.count > 0){
							value[0] = 1;
							finishStatus = MPI.COMM_WORLD.Recv(value, 0, finishStatus.count, MPI.INT, i, TAG.FINISH.code);
							finished++;
							System.out.println("Received " + finished + "/" + (size-1));

						}
						finishStatus = null;
					}

					Status newSolution = MPI.COMM_WORLD.Iprobe(i, TAG.SOLUTION.code);

					if(newSolution != null){
						MPI.COMM_WORLD.Recv(solution, 0, newSolution.count, MPI.OBJECT, i, TAG.SOLUTION.code);
						for(int j=0;j<newSolution.count;++j){
							if(result.size() + 1 > result.getCapacity()){
								result.remove(result.indexWorst(comparationOperator));
							}
							result.add(new Solution(solution[j]));

						}

						System.out.println("Received "+ newSolution.count +" solutions with objectives: " + solution[0].getObjective(0) + " " + solution[0].getObjective(1));
						newSolution = null;
					}


					if(result.size() > 0){
						SolutionSet paretoFront = new Ranking(result).getSubfront(0);
						Random random = new Random();
						/*if(sendingRequest != null){
							sendingRequest.finalize();
							sendingRequest = null;
						}*/

						solution[0] = paretoFront.get(random.nextInt(paretoFront.size()));
						solution[1] = paretoFront.get(random.nextInt(paretoFront.size()));
						solution[2] = paretoFront.get(random.nextInt(paretoFront.size()));
						solution[3] = paretoFront.get(random.nextInt(paretoFront.size()));
						solution[4] = paretoFront.get(random.nextInt(paretoFront.size()));
						//System.out.println("Trying to send a solution to node " + i);
						sendingRequest[i] = MPI.COMM_WORLD.Isend(solution, 0, 5, MPI.OBJECT, i, TAG.SOLUTION.code);
						//sendingRequest = MPI.COMM_WORLD.Isend(solution, 0, 1, MPI.OBJECT, 0, TAG.SOLUTION.code);
					}


				}

			}

			Ranking ranking = new Ranking(result);

			ranking.getSubfront(0).printObjectivesToFile("testeMPI.fun");
			ranking.getSubfront(0).printVariablesToFile("testeMPI.var");

		    //MPI_Send(buffer2, 10, MPI_INT, right, 123, MPI_COMM_WORLD);
		    //MPI_Wait(&request, &status);
		}else{

			try {
				Problem problem = new MultiObjectiveKnapsackProblem("c1600_w2.txt");

				//Run the algorithm
				AlgorithmParameters algorithmParameters = new AlgorithmParameters();
				// Algorithm parameters
				algorithmParameters.addParameter("populationSize",128);
				algorithmParameters.addParameter("archiveSize",128);
				algorithmParameters.addParameter("maxEvaluations",250000);

			    // Mutation and Crossover for Real codification
				try {
					algorithmParameters.setCrossover("SinglePointCrossover", 0.8);
					algorithmParameters.setMutation("BitFlipMutation",0.2);
					algorithmParameters.setSelection("BinaryTournament");
				} catch (JMException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				algorithmParameters.setAlgorithm("NSGA-II");
				Algorithm algorithm = algorithmInitialize(problem, algorithmParameters);

				ApplicationContext.getContext().isRunningAlgorithm.set(true);
				SolutionSet population = algorithm.execute();
				ApplicationContext.getContext().isRunningAlgorithm.set(false);


				Ranking ranking = new Ranking(population);

				ranking.getSubfront(0).printObjectivesToFile("testeMPI[" + me + "].fun");
				ranking.getSubfront(0).printVariablesToFile("testeMPI[" + me + "].var");

				/*for(int i=0;i<100;++i){
					Solution solution[] = new Solution[1];
					solution[0] = new Solution(problem);

					Request request = MPI.COMM_WORLD.Isend(solution, 0, 1, MPI.OBJECT, root, TAG.SOLUTION.code);

					request.Wait();
				}*/

				int value[] = new int[1];
				value[0] = 1;
				MPI.COMM_WORLD.Send(value, 0, 1, MPI.INT, root, TAG.FINISH.code);

			} catch (FileNotFoundException | ClassNotFoundException | JMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}




			/*Solution solution[] = new Solution[1];
			solution[0] = new Solution(new MultiObjectiveKnapsackProblem("c200_w2[1].txt"));
			solution[0].setObjective(0, 10);
			solution[0].setObjective(1, 50);
			MPI.COMM_WORLD.Send(solution, 0, 1, MPI.OBJECT, 0, TAG.SOLUTION.code);*/
		}


		MPI.Finalize();
	}

	public static Algorithm algorithmInitialize(Problem problem, AlgorithmParameters ap){
		Algorithm algorithm = AlgorithmFactory.createAlgorithm(ap.getAlgorithm(), problem);

		for (Map.Entry<String, Object> entry : ap.getMap().entrySet()) {
		    String key = entry.getKey();
		    Object value = entry.getValue();

		    algorithm.setInputParameter(key, value);
		}

		algorithm.addOperator("crossover",ap.getCrossover());
	    algorithm.addOperator("mutation",ap.getMutation());
	    algorithm.addOperator("selection",ap.getSelection());

	    return algorithm;
	}

}
