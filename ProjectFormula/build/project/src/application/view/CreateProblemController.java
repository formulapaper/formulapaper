package application.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.TextField;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;

import org.scilab.forge.jlatexmath.Box;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

import application.ApplicationContext;
import application.MainApp;
import application.model.ProblemType;
import box.ConfirmationBox;
import box.InformationBox;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.DoubleSpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import magius.Builder;
import magius.GeneralProblem;
import magius.GeneralVariable;
import magius.Loop;
import undecorator.insidefx.undecorator.Undecorator;
import undecorator.insidefx.undecorator.UndecoratorScene;

public class CreateProblemController {

	@FXML
	private BorderPane rootLayout;

	@FXML
	private ImageView backButton;

	@FXML
	private AnchorPane mainPane;

	@FXML
	private Button builderButton;

	@FXML
	private Button zoomIn;

	@FXML
	private Button zoomOut;

	@FXML
	private Button backFormulationButton;

	@FXML
	private Button plusButton;

	@FXML
	private Button minusButton;

	@FXML
	private Button multiplyButton;

	@FXML
	private Button divideButton;

	@FXML
	private Button powerButton;

	@FXML
	private Button openBracket;

	@FXML
	private Button closeBracket;

	@FXML
	private Button numberButton;

	@FXML
	private Button sumButton;

	@FXML
	private Button prodButton;

	@FXML
	private Button endLoopButton;

	@FXML
	private Button createVariableButton;

	@FXML
	private Button deleteVariableButton;

	@FXML
	private Button editVariableButton;

	@FXML
	private Button customCodeButton;

	@FXML
	private Button customCodeVariablesButton;

	@FXML
	private Button customCodeConstructorButton;

	@FXML
	private ChoiceBox<String> solutionType;

	@FXML
	private ComboBox<String> readBox;

	@FXML
	private Spinner<Double> variableLowerLimit;

	@FXML
	private Spinner<Double> variableUpperLimit;

	@FXML
	private Button addReadButton;

	@FXML
	private Button deleteReadButton;

	@FXML
	private ListView<String> readListView;

	@FXML
	private Canvas latexFormulation;
	ContextMenu canvasContextMenu;
	int textSize;

	@FXML
	private ListView<GeneralVariable> variableList;

	@FXML
	private ComboBox<String> objectiveSelector;

	@FXML
	private ChoiceBox<String> constraintSelector;

	@FXML
	private ChoiceBox<String> constraintTypeSelector;

	@FXML
	private TextArea debugConsole;
	OutputStream outConsole; //If use the console, remember close the OutputStream

	//@FXML
	//private ListView<String> objectiveList;
	//private ObservableList<String> objectiveObservable;

	//@FXML
	//private ListView<String> constraintList;
	//private ObservableList<String> constraintObservable;


	//Index from current objective or constraint
	int idx;
	int type;

	private GeneralProblem problem;

	ObservableList<GeneralVariable> variableObservableList;

	static int OBJECTIVE = 0;
	static int CONSTRAINT = 1;

	static int MINIMUN_TEXT_SIZE = 8;
	static int MAXIMUM_TEXT_SIZE = 100;

	public CreateProblemController() {
		problem = new GeneralProblem("Teste", "Integer", 10, 2, 1);
		//problem = new GeneralProblem("Teste", "Integer", 10, 0, 0);

		textSize = 30;

		idx = 0;
		type = OBJECTIVE;

	}

	/**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {

    	//Just to test
    	//problem.loadTestProblem();

    	updateScreen();


    	//BackButton Action
    	backButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
    	     @Override
    	     public void handle(MouseEvent event) {

    	    	 try {
					outConsole.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

    	    	 //Save the problem
    	    	 ApplicationContext.getContext().getProject().saveProblem(problem);

    	    	 ApplicationContext.getContext().getMainApp().loadScene("MainMenu");
    	    	 ApplicationContext.getContext().getMainApp().setScene();
    	     }
    	});

    	configButtons();

    	configObjectiveSelector();

    	configListViews();


    	latexFormulation.addEventHandler(ScrollEvent.SCROLL, new EventHandler<ScrollEvent>() {
    		@Override
	   	     public void handle(ScrollEvent event) {

    				double value = event.getDeltaY();
    				value /= 20;

    				if(textSize - value >= MINIMUN_TEXT_SIZE){
    					textSize -= value;
    				}else{
    					textSize = MINIMUN_TEXT_SIZE;
    				}

    				renderFormula();
	   	     }
    	});

    	renderFormula();

    	latexFormulation.widthProperty().addListener(evt -> renderFormula());
    	latexFormulation.heightProperty().addListener(evt -> renderFormula());

    	//latexFormulation.widthProperty().bind(mainPane.widthProperty());

    	updateReadBox();
    	updateReadListView();

    	//Add Text formulation to clipboard
    	canvasContextMenu = new ContextMenu();
    	MenuItem copyTex = new MenuItem("Copy Tex");
    	copyTex.setOnAction(new EventHandler<ActionEvent>() {
    	    public void handle(ActionEvent e) {
    	        Clipboard clipboard = Clipboard.getSystemClipboard();
    	        ClipboardContent content = new ClipboardContent();

    	        if(type == OBJECTIVE){
    	        	content.putString(problem.getTex(problem.objective, idx));
    	        }else{
    	        	content.putString(problem.getTex(problem.constraintPart1, idx) +
                			problem.getComparisionSymbol(problem.constraintType.get(idx)) +
                			problem.getTex(problem.constraintPart2, idx));
    	        }

    	        //content.putImage(pic.getImage());

    	        clipboard.setContent(content);
    	    }
    	});

    	canvasContextMenu.getItems().add(copyTex);

    	latexFormulation.addEventHandler(MouseEvent.MOUSE_CLICKED,
    	    new EventHandler<MouseEvent>() {
    	        @Override public void handle(MouseEvent e) {
    	            if (e.getButton() == MouseButton.SECONDARY)
    	            	canvasContextMenu.show(latexFormulation, e.getScreenX(), e.getScreenY());
    	        }
    	});


       	outConsole = ApplicationContext.getContext().initializeConsole(debugConsole);

    }

    public void appendText(String str) {
	    Platform.runLater(() -> debugConsole.appendText(str));
	}

    void updateScreen(){
    	variableObservableList = FXCollections.observableArrayList();
    	variableObservableList.addAll(problem.variable);

    	variableList.setItems(variableObservableList);

    	constraintSelector.setDisable(type == OBJECTIVE);
	    constraintTypeSelector.setDisable(type == OBJECTIVE);

    	List<String> objectiveList = new ArrayList<String>();

    	//For each objective
    	for(int i=0;i<problem.getNumberOfObjectives();i++){
    		//System.out.println("Adding objective " + (i+1));
    		objectiveList.add("Objective " + (i+1));
    	}

    	//For each constraint
    	for(int i=0;i<problem.getNumberOfConstraints();i++){
    		//System.out.println("Adding constraint " + (i+1));
    		objectiveList.add("Constraint " + (i+1));
    	}

    	objectiveSelector.getItems().clear();
    	objectiveSelector.getItems().addAll(objectiveList);
    	if(objectiveList.size() > 0)
    		objectiveSelector.setValue(objectiveList.get(0));

    	//CONSTRAINT
    	List<String> constraintPartList = new ArrayList<String>();
    	constraintPartList.add("Part 1");
    	constraintPartList.add("Part 2");
    	constraintSelector.getItems().clear();
    	constraintSelector.getItems().addAll(constraintPartList);
    	if(constraintPartList.size() > 0)
    		constraintSelector.setValue(constraintPartList.get(0));


    	/*objectiveObservable =  FXCollections.observableArrayList();
    	constraintObservable =  FXCollections.observableArrayList();

    	//For each objective
    	for(int i=0;i<problem.getNumberOfObjectives();i++){
    		System.out.println("Adding objective " + (i+1));
    		objectiveObservable.add("Objective " + (i+1));
    	}
    	objectiveList.setItems(objectiveObservable);

    	//For each constraint
    	for(int i=0;i<problem.getNumberOfConstraints();i++){
    		constraintObservable.add("Constraint " + (i+1));
    	}
    	constraintList.setItems(constraintObservable);*/

    	//CONSTRAINT TYPE
    	List<String> constraintTypeList = new ArrayList<String>();
    	constraintTypeList.add("<");
    	constraintTypeList.add("<=");
    	constraintTypeList.add("==");
    	constraintTypeList.add("!=");
    	constraintTypeList.add(">");
    	constraintTypeList.add(">=");
    	constraintTypeSelector.getItems().clear();
    	constraintTypeSelector.getItems().addAll(constraintTypeList);
    	if(constraintTypeList.size() > 0){
    		if(type == CONSTRAINT){
    			constraintTypeSelector.setValue(problem.constraintType.get(idx));
    		}else
    			constraintTypeSelector.setValue(constraintTypeList.get(0));
    	}

		constraintTypeSelector.valueProperty().addListener((obs, oldValue, newValue) -> {
			if(type == CONSTRAINT){
				problem.constraintType.set(idx, newValue);
				renderFormula();
			}
		});

    	List<String> variableTypeList = new ArrayList<String>();
    	variableTypeList.add("Integer");
    	variableTypeList.add("Real");
    	variableTypeList.add("Permutation");
    	solutionType.getItems().clear();
    	solutionType.getItems().addAll(variableTypeList);

    	//System.out.println(problem.getVariableType());

    	solutionType.valueProperty().addListener((obs, oldValue, newValue) ->{
    		if(newValue != null)
    			problem.setVariableType(newValue);
    	});

    	if(variableTypeList.size() > 0)
    		solutionType.setValue(problem.getVariableType());


    	DoubleSpinnerValueFactory lowerValueFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(0, 1000);
    	lowerValueFactory.setValue(problem.getGlobalLowerLimit());
    	lowerValueFactory.setAmountToStepBy(0.1);
    	variableLowerLimit.setValueFactory(lowerValueFactory);
    	variableLowerLimit.valueProperty().addListener((obs, oldValue, newValue) ->
        problem.setGlobalLowerLimit(newValue));

    	DoubleSpinnerValueFactory upperValueFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(0, 1000);
    	upperValueFactory.setValue(problem.getGlobalUpperLimit());
    	upperValueFactory.setAmountToStepBy(0.1);
    	variableUpperLimit.setValueFactory(upperValueFactory);
    	variableUpperLimit.valueProperty().addListener((obs, oldValue, newValue) ->
        problem.setGlobalUpperLimit(newValue));

    	renderFormula();
    	updateReadBox();
    	updateReadListView();
    }

    public void setProblem(GeneralProblem problem){
    	this.problem = problem;
    	updateScreen();
    }

    private void updateReadBox(){
    	List<String> readBoxList = new ArrayList<String>();
    	readBoxList.clear();
    	boolean have;
    	for(int i=0;i<problem.variable.size();i++){
    		have = false;

    		//If the variable is the chromossome
    		if(problem.variable.get(i).name.equals("X")) continue;

    		for(int j=0;j<problem.readList.size();j++){
    			if(problem.variable.get(i).name.equals(problem.readList.get(j))){
    				have = true;
    				break;
    			}
    		}
    		if(!have){
    			readBoxList.add(problem.variable.get(i).name);
    		}
    	}
    	readBox.getItems().clear();
    	readBox.getItems().addAll(readBoxList);
    	if(readBoxList.size() > 0)
    		readBox.setValue(readBoxList.get(0));
    }

    private void updateReadListView(){
    	List<String> tmp = new ArrayList<String>();
    	for(int i=0;i<problem.readList.size();i++){
    		tmp.add(problem.readList.get(i));
    	}
    	readListView.getItems().clear();
    	readListView.getItems().addAll(tmp);
    }

    void configObjectiveSelector(){
    	objectiveSelector.setOnAction((event) -> {
    	    //String selected = objectiveSelector.getSelectionModel().getSelectedItem();
    	    if(objectiveSelector.getSelectionModel().getSelectedIndex() < problem.getNumberOfObjectives()){
    	    	idx = objectiveSelector.getSelectionModel().getSelectedIndex();
    	    	type = OBJECTIVE;
    	    }else{
    	    	idx = objectiveSelector.getSelectionModel().getSelectedIndex() - problem.getNumberOfObjectives();
    	    	type = CONSTRAINT;
    	    }

    	    constraintSelector.setDisable(type == OBJECTIVE);
    	    constraintTypeSelector.setDisable(type == OBJECTIVE);

    	    renderFormula();

    	});


    }

    void configListViews(){

    	/*objectiveList.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if(!objectiveList.getSelectionModel().isEmpty()){
                	idx = objectiveList.getSelectionModel().getSelectedIndex();
                	type = OBJECTIVE;
                	renderFormula();

                	System.out.println("Selected objective " + idx);
                }
            }
        });

    	constraintList.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if(!constraintList.getSelectionModel().isEmpty()){
                	idx = constraintList.getSelectionModel().getSelectedIndex();
                	type = CONSTRAINT;
                	renderFormula();

                	System.out.println("Selected constraint " + idx);
                }
            }
        });*/

    	variableList.setOnMouseClicked(new EventHandler<MouseEvent>() {

    	    @Override
    	    public void handle(MouseEvent click) {

    	        if (click.getClickCount() == 2) {
    	           //Use ListView's getSelected Item
    	           GeneralVariable currentItemSelected = variableList.getSelectionModel()
    	                                                    .getSelectedItem();

    	           if(currentItemSelected.type.equals("Normal")){
    	        	   if(type == OBJECTIVE){
    	        		   problem.objective[idx].add("V" + variableList.getSelectionModel().getSelectedIndex());
    	        	   }else if(type == CONSTRAINT){
    	        		   if(constraintSelector.getSelectionModel().getSelectedIndex() == 0){
    	        			   problem.constraintPart1[idx].add("V" + variableList.getSelectionModel().getSelectedIndex());
    	        		   }else{
    	        			   problem.constraintPart2[idx].add("V" + variableList.getSelectionModel().getSelectedIndex());
    	        		   }
    	        	   }
    	        	   renderFormula();
    	           }else if(currentItemSelected.type.equals("Vector") || currentItemSelected.type.equals("Matrix")){
    	        	   BorderPane page = null;
    	            	try{
    	                	FXMLLoader loader = new FXMLLoader();
    		                loader.setLocation(MainApp.class.getResource("view/EnterVariableVector.fxml"));
    		                page = (BorderPane) loader.load();


    	                    // Cria o palco dialogStage.
    	                    Stage dialogStage = new Stage();
    	                    dialogStage.setTitle("Enter Variable " + currentItemSelected.type);
    	                    dialogStage.initModality(Modality.WINDOW_MODAL);
    	                    dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
    	                    UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
    	                    dialogStage.setScene(scene);

    	                    dialogStage.sizeToScene();
    	                    dialogStage.toFront();

    	                    Undecorator undecorator = scene.getUndecorator();
    	                    dialogStage.setMinWidth(undecorator.getMinWidth());
    	                    dialogStage.setMinHeight(undecorator.getMinHeight());

    	                    // Define a pessoa no controller.
    	                    EnterVariableVectorController controller = loader.getController();
    	                    controller.setDialogStage(dialogStage);
    	                    controller.setType(currentItemSelected.type);
    	                    controller.setVariable(currentItemSelected);
    	                    controller.setObjectiveIdx(idx);
    	                    controller.setObjectiveType(type);
    	                    controller.setProblem(problem);


    	                    // Mostra a janela e espera at� o usu�rio fechar.
    	                    dialogStage.showAndWait();

    	                    if(controller.isOkClicked()){

    	                    	if(type == OBJECTIVE){
    	                    		problem.objective[idx].addAll(controller.getResult());
    	                    	}else if(type == CONSTRAINT){
    	     	        		   if(constraintSelector.getSelectionModel().getSelectedIndex() == 0){
    	     	        			   problem.constraintPart1[idx].addAll(controller.getResult());
    	     	        		   }else{
    	     	        			   problem.constraintPart2[idx].addAll(controller.getResult());
    	     	        		   }
    	     	        	   }
    	                    	renderFormula();


    	                    }

    		            } catch (IOException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					}

    		  	     }

    	           }
    	           //use this to do whatever you want to. Open Link etc.
    	        }

    	});

    }

    void createOrEditVariable(boolean editing){

    	if(editing && variableList.getSelectionModel().getSelectedIndex() < 0)
    		return;


    	BorderPane page = null;
       	try{
           	FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainApp.class.getResource("view/CreateVariable.fxml"));
                page = (BorderPane) loader.load();


               // Cria o palco dialogStage.
               Stage dialogStage = new Stage();
               dialogStage.setTitle( editing ? "Edit Variable" : "Create Variable");
               dialogStage.initModality(Modality.WINDOW_MODAL);
               dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
               UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
               dialogStage.setScene(scene);

               dialogStage.sizeToScene();
               dialogStage.toFront();

               Undecorator undecorator = scene.getUndecorator();
               dialogStage.setMinWidth(undecorator.getMinWidth());
               dialogStage.setMinHeight(undecorator.getMinHeight());

               // Define a pessoa no controller.
               CreateVariableController controller = loader.getController();
               controller.setDialogStage(dialogStage);
               controller.setProblem(problem);

               if(editing)
            	   controller.setVariable(variableList.getSelectionModel().getSelectedItem());

               int index = variableList.getSelectionModel().getSelectedIndex();


               // Mostra a janela e espera at� o usu�rio fechar.
               dialogStage.showAndWait();

               if(controller.isOkClicked()){

            	   //That have a created variable
            	   if(controller.getVariable()!=null){
            		   if(editing){
            			   problem.variable.set(index, controller.getVariable());
            		   }else{
            			   problem.variable.add(controller.getVariable());
            		   }

            		   updateReadBox();
            		   updateReadListView();
            		   updateScreen();
            	   }

               	renderFormula();


               }

            } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }

    void configButtons(){

    	builderButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {

	  	    	//Save the problem
    	    	 ApplicationContext.getContext().getProject().saveProblem(problem);

	  	    	 System.out.println("Build the problem.");
	  	    	 try {
	  	    		Builder builder = new Builder(problem);
					builder.build();
				} catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	  	     }
		});

    	addReadButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
	  	    	 //System.out.println(readBox.getSelectionModel().getSelectedItem());
	  	    	 problem.readList.add(readBox.getSelectionModel().getSelectedItem());
	  	    	 updateReadBox();
	  	    	 updateReadListView();
	  	     }
		});

    	deleteReadButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
	  	    	 for(int i=0;i<problem.readList.size();i++){
	  	    		 if(readListView.getSelectionModel().getSelectedItem().equals(problem.readList.get(i))){
	  	    			problem.readList.remove(i);
	  	    		 }
	  	    	 }
	  	    	 updateReadBox();
	  	    	 updateReadListView();
	  	     }
		});

    	zoomIn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	   	     @Override
	   	     public void handle(MouseEvent event) {
	   	    	double value = 2;

				if(textSize + value <= MAXIMUM_TEXT_SIZE){
					textSize += value;
				}else{
					textSize = MAXIMUM_TEXT_SIZE;
				}

				renderFormula();
	   	     }
	   	});

	   	zoomOut.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
		   	     @Override
		   	     public void handle(MouseEvent event) {
		   	    	double value = 2;

					if(textSize - value >= MINIMUN_TEXT_SIZE){
						textSize -= value;
					}else{
						textSize = MINIMUN_TEXT_SIZE;
					}

					renderFormula();
		   	     }
	   	});

	   	backFormulationButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
	  	    	 //System.out.println("Back");


	  	    	if(type == OBJECTIVE){
	  	    		if(problem.objective[idx].size() > 0){

		  	    		//If is a Loop, remove from loop list too
		  	    		if(problem.objective[idx].get(problem.objective[idx].size()-1).contains("L")){
		  	    			int loopIdx = Integer.valueOf(problem.objective[idx].get(problem.objective[idx].size()-1).substring(1));
		  	    			problem.loop.remove(loopIdx);

		  	    			//Need correct each loop in each objective or constraint
		  	    			for(int i=0;i<problem.getNumberOfObjectives();++i){
		  	    				for(int j=0;j<problem.objective[i].size();++j){
		  	    					if(problem.objective[i].get(j).contains("L") &&
		  	    							(Integer.valueOf(problem.objective[i].get(j).substring(1)) > loopIdx)){
		  	    						problem.objective[i].set(j, "L" + (Integer.valueOf(problem.objective[i].get(j).substring(1)) - 1));
		  	    					}
		  	    				}
		  	    			}

		  	    			for(int i=0;i<problem.getNumberOfConstraints();++i){
		  	    				for(int j=0;j<problem.constraintPart1[i].size();++j){
		  	    					if(problem.constraintPart1[i].get(j).contains("L") &&
		  	    							(Integer.valueOf(problem.constraintPart1[i].get(j).substring(1)) > loopIdx)){
		  	    						problem.constraintPart1[i].set(j, "L" + (Integer.valueOf(problem.constraintPart1[i].get(j).substring(1)) - 1));
		  	    					}
		  	    				}

		  	    				for(int j=0;j<problem.constraintPart2[i].size();++j){
		  	    					if(problem.constraintPart2[i].get(j).contains("L") &&
		  	    							(Integer.valueOf(problem.constraintPart2[i].get(j).substring(1)) > loopIdx)){
		  	    						problem.constraintPart2[i].set(j, "L" + (Integer.valueOf(problem.constraintPart2[i].get(j).substring(1)) - 1));
		  	    					}
		  	    				}
		  	    			}
		  	    		}

		  	    		problem.objective[idx].remove(problem.objective[idx].size()-1);
		  	    		renderFormula();
		  	    	}
	  	    	}else if(type == CONSTRAINT){
	  	    		if(constraintSelector.getSelectionModel().getSelectedIndex() == 0){
	  	    			if(problem.constraintPart1[idx].size() > 0){

			  	    		//If is a Loop, remove from loop list too
			  	    		if(problem.constraintPart1[idx].get(problem.constraintPart1[idx].size()-1).contains("L")){
			  	    			int loopIdx = Integer.valueOf(problem.constraintPart1[idx].get(problem.constraintPart1[idx].size()-1).substring(1));
			  	    			problem.loop.remove(loopIdx);
			  	    		}

			  	    		problem.constraintPart1[idx].remove(problem.constraintPart1[idx].size()-1);
			  	    		renderFormula();
			  	    	}
        		   }else{
	  	    			if(problem.constraintPart2[idx].size() > 0){

			  	    		//If is a Loop, remove from loop list too
			  	    		if(problem.constraintPart2[idx].get(problem.constraintPart2[idx].size()-1).contains("L")){
			  	    			int loopIdx = Integer.valueOf(problem.constraintPart2[idx].get(problem.constraintPart2[idx].size()-1).substring(1));
			  	    			problem.loop.remove(loopIdx);
			  	    		}

			  	    		problem.constraintPart2[idx].remove(problem.constraintPart2[idx].size()-1);
			  	    		renderFormula();
			  	    	}
        		   }
	  	    	}

	  	     }
		});


	   	//Operators
	   	plusButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
	  	    	if(type == OBJECTIVE){
	  	    		problem.objective[idx].add("+");
	  	    	}else if(type == CONSTRAINT){
	  	    		if(constraintSelector.getSelectionModel().getSelectedIndex() == 0){
        			   problem.constraintPart1[idx].add("+");
        		   }else{
        			   problem.constraintPart2[idx].add("+");
        		   }
	  	    	}
	  	    	renderFormula();
	  	     }
		});

	   	minusButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
		  	    	if(type == OBJECTIVE){
		  	    		problem.objective[idx].add("-");
		  	    	}else if(type == CONSTRAINT){
		  	    		if(constraintSelector.getSelectionModel().getSelectedIndex() == 0){
	        			   problem.constraintPart1[idx].add("-");
	        		   }else{
	        			   problem.constraintPart2[idx].add("-");
	        		   }
		  	    	}
	  	    	renderFormula();
	  	     }
		});

	   	divideButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
		  	    	if(type == OBJECTIVE){
		  	    		problem.objective[idx].add("/");
		  	    	}else if(type == CONSTRAINT){
		  	    		if(constraintSelector.getSelectionModel().getSelectedIndex() == 0){
	        			   problem.constraintPart1[idx].add("/");
	        		   }else{
	        			   problem.constraintPart2[idx].add("/");
	        		   }
		  	    	}
	  	    	renderFormula();
	  	     }
		});

	   	powerButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {

	  	    	InformationBox informationBox = new InformationBox("Not avaliable", "This content is not yet avaliable.", 18);

            	informationBox.show();
		  	    	/*if(type == OBJECTIVE){
		  	    		problem.objective[idx].add("^");
		  	    	}else if(type == CONSTRAINT){
		  	    		if(constraintSelector.getSelectionModel().getSelectedIndex() == 0){
	        			   problem.constraintPart1[idx].add("^");
	        		   }else{
	        			   problem.constraintPart2[idx].add("^");
	        		   }
		  	    	}*/
	  	    	renderFormula();
	  	     }
		});

	   	multiplyButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
		  	    	if(type == OBJECTIVE){
		  	    		problem.objective[idx].add("*");
		  	    	}else if(type == CONSTRAINT){
		  	    		if(constraintSelector.getSelectionModel().getSelectedIndex() == 0){
	        			   problem.constraintPart1[idx].add("*");
	        		   }else{
	        			   problem.constraintPart2[idx].add("*");
	        		   }
		  	    	}
	  	    	renderFormula();
	  	     }
		});

	   	openBracket.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
		  	    	if(type == OBJECTIVE){
		  	    		problem.objective[idx].add("(");
		  	    	}else if(type == CONSTRAINT){
		  	    		if(constraintSelector.getSelectionModel().getSelectedIndex() == 0){
	        			   problem.constraintPart1[idx].add("(");
	        		   }else{
	        			   problem.constraintPart2[idx].add("(");
	        		   }
		  	    	}
	  	    	renderFormula();
	  	     }
		});

	   	closeBracket.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
		  	    	if(type == OBJECTIVE){
		  	    		problem.objective[idx].add(")");
		  	    	}else if(type == CONSTRAINT){
		  	    		if(constraintSelector.getSelectionModel().getSelectedIndex() == 0){
	        			   problem.constraintPart1[idx].add(")");
	        		   }else{
	        			   problem.constraintPart2[idx].add(")");
	        		   }
		  	    	}
	  	    	renderFormula();
	  	     }
		});

	   	endLoopButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
		  	    	if(type == OBJECTIVE){
		  	    		problem.objective[idx].add("]");
		  	    	}else if(type == CONSTRAINT){
		  	    		if(constraintSelector.getSelectionModel().getSelectedIndex() == 0){
	        			   problem.constraintPart1[idx].add("]");
	        		   }else{
	        			   problem.constraintPart2[idx].add("]");
	        		   }
		  	    	}
	  	    	renderFormula();
	  	     }
		});

	   	sumButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {

	  	    	BorderPane page = null;
            	try{
                	FXMLLoader loader = new FXMLLoader();
	                loader.setLocation(MainApp.class.getResource("view/EnterLoop.fxml"));
	                page = (BorderPane) loader.load();


                    // Cria o palco dialogStage.
                    Stage dialogStage = new Stage();
                    dialogStage.setTitle("Create New Loop");
                    dialogStage.initModality(Modality.WINDOW_MODAL);
                    dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
                    UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
                    dialogStage.setScene(scene);

                    dialogStage.sizeToScene();
                    dialogStage.toFront();

                    Undecorator undecorator = scene.getUndecorator();
                    dialogStage.setMinWidth(undecorator.getMinWidth());
                    dialogStage.setMinHeight(undecorator.getMinHeight());

                    // Define a pessoa no controller.
                    EnterLoopController controller = loader.getController();
                    controller.setDialogStage(dialogStage);
                    controller.setType("Sum");
                    controller.setProblem(problem);
                    controller.updateIdentifier();


                    // Mostra a janela e espera at� o usu�rio fechar.
                    dialogStage.showAndWait();

                    if(controller.isOkClicked()){

                    	//System.out.println("Loops");
                    	/*for(int i=0;i<problem.loop.size();++i){
                    		System.out.println(problem.loop.get(i).identifier);
                    	}*/

                    	Loop tmpLoop = new Loop("sum",controller.getIdentifier(), 0, 1);
                    	problem.loop.add(tmpLoop);

                    	problem.loop.get(problem.loop.size() - 1).setInitialVariable(controller.getInitial());
                    	problem.loop.get(problem.loop.size() - 1).setFinalVariable(controller.getFinal());

                    	//problem.objective[idx].add("L" + (problem.loop.size() - 1));

        	  	    	if(type == OBJECTIVE){
        	  	    		problem.objective[idx].add("L" + (problem.loop.size() - 1));
        	  	    	}else if(type == CONSTRAINT){
        	  	    		if(constraintSelector.getSelectionModel().getSelectedIndex() == 0){
                			   problem.constraintPart1[idx].add("L" + (problem.loop.size() - 1));
                		   }else{
                			   problem.constraintPart2[idx].add("L" + (problem.loop.size() - 1));
                		   }
        	  	    	}



                    	renderFormula();


                    }

	            } catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

	  	     }
		});

	   	prodButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {

	  	    	BorderPane page = null;
           	try{
               	FXMLLoader loader = new FXMLLoader();
	                loader.setLocation(MainApp.class.getResource("view/EnterLoop.fxml"));
	                page = (BorderPane) loader.load();


                   // Cria o palco dialogStage.
                   Stage dialogStage = new Stage();
                   dialogStage.setTitle("Create New Loop");
                   dialogStage.initModality(Modality.WINDOW_MODAL);
                   dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
                   UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
                   dialogStage.setScene(scene);

                   dialogStage.sizeToScene();
                   dialogStage.toFront();

                   Undecorator undecorator = scene.getUndecorator();
                   dialogStage.setMinWidth(undecorator.getMinWidth());
                   dialogStage.setMinHeight(undecorator.getMinHeight());

                   // Define a pessoa no controller.
                   EnterLoopController controller = loader.getController();
                   controller.setDialogStage(dialogStage);
                   controller.setType("Prod");
                   controller.setProblem(problem);
                   controller.updateIdentifier();


                   // Mostra a janela e espera at� o usu�rio fechar.
                   dialogStage.showAndWait();

                   if(controller.isOkClicked()){
                   	Loop tmpLoop = new Loop("prod",controller.getIdentifier(), 0, 1);
                   	problem.loop.add(tmpLoop);

                   	problem.loop.get(problem.loop.size() - 1).setInitialVariable(controller.getInitial());;
                   	problem.loop.get(problem.loop.size() - 1).setFinalVariable(controller.getFinal());;

                   	if(type == OBJECTIVE){
    	  	    		problem.objective[idx].add("L" + (problem.loop.size() - 1));
    	  	    	}else if(type == CONSTRAINT){
    	  	    		if(constraintSelector.getSelectionModel().getSelectedIndex() == 0){
            			   problem.constraintPart1[idx].add("L" + (problem.loop.size() - 1));
            		   }else{
            			   problem.constraintPart2[idx].add("L" + (problem.loop.size() - 1));
            		   }
    	  	    	}


                   	renderFormula();


                   }

	            } catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

	  	     }
		});

	   	//Insert a number
	   	numberButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
	  	    // Carrega o arquivo fxml e cria um novo stage para a janela popup.
             	BorderPane page = null;
             	try {
		                FXMLLoader loader = new FXMLLoader();
		                loader.setLocation(MainApp.class.getResource("view/EnterNumber.fxml"));
		                page = (BorderPane) loader.load();


	                    // Cria o palco dialogStage.
	                    Stage dialogStage = new Stage();
	                    dialogStage.setTitle("Enter Number");
	                    dialogStage.initModality(Modality.WINDOW_MODAL);
	                    dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
	                    UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
	                    dialogStage.setScene(scene);

	                    dialogStage.sizeToScene();
	                    dialogStage.toFront();

	                    Undecorator undecorator = scene.getUndecorator();
	                    dialogStage.setMinWidth(undecorator.getMinWidth());
	                    dialogStage.setMinHeight(undecorator.getMinHeight());

	                    // Define a pessoa no controller.
	                    EnterNumberController controller = loader.getController();
	                    controller.setDialogStage(dialogStage);
	                    /*controller.setPerson(person);*/

	                    // Mostra a janela e espera at� o usu�rio fechar.
	                    dialogStage.showAndWait();

	                    if(controller.isOkClicked()){

	                    	///problem.objective[idx].add(controller.getNumber());

	                    	if(type == OBJECTIVE){
	                    		problem.objective[idx].add(controller.getNumber());
	        	  	    	}else if(type == CONSTRAINT){
	        	  	    		if(constraintSelector.getSelectionModel().getSelectedIndex() == 0){
	                			   problem.constraintPart1[idx].add(controller.getNumber());
	                		   }else{
	                			   problem.constraintPart2[idx].add(controller.getNumber());
	                		   }
	        	  	    	}
	                    	renderFormula();
	                    }


             	//if(createProject())
             	//	ApplicationContext.getContext().getMainApp().setScene("RunAlgorithm");

             	} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	  	     }
		});

	   	createVariableButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
	  	    	createOrEditVariable(false);
	  	     }
		});

	   	editVariableButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
	  	    	 createOrEditVariable(true);
	  	     }
		});

	   	deleteVariableButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
	  	    	 if(variableList.getSelectionModel().getSelectedIndex() >= 0){
	  	    		 ConfirmationBox confirmationBox = new ConfirmationBox("Delete", "Do you really want delete this variable?", 18);

	  	    		 GeneralVariable variableToDelete = variableList.getSelectionModel().getSelectedItem();

	  	    		if(variableToDelete.name.equals("X") || variableToDelete.name.equals("N")){
 	    				 InformationBox informationBox = new InformationBox("Can not do this", "You can't delete critical variables", 18);
 	    				 informationBox.show();
	  	    		}else{
		  	    		 if(confirmationBox.show()){
		  	    			 /*for(int i = 0; i<problem.variable.size(); ++i){
		  	    				 if(problem.variable.get(i).name.compareTo(variableToDelete.name))
		  	    			 }*/

	  	    				 problem.variable.remove(variableToDelete);
	  	    				 updateScreen();

		  	    		 }
	  	    		}
	  	    	 }
	  	     }
		});

	   	//CUSTOM CODE BUTTON
	  //Insert a number
	   	customCodeButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
	  	    // Carrega o arquivo fxml e cria um novo stage para a janela popup.
             	BorderPane page = null;
             	try {
		                FXMLLoader loader = new FXMLLoader();
		                loader.setLocation(MainApp.class.getResource("view/CustomCode.fxml"));
		                page = (BorderPane) loader.load();


	                    // Cria o palco dialogStage.
	                    Stage dialogStage = new Stage();
	                    dialogStage.setTitle("Custom Code to " + (type == OBJECTIVE ? "Objective " : "Constraint ") + (idx + 1) );
	                    dialogStage.initModality(Modality.WINDOW_MODAL);
	                    dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
	                    UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
	                    dialogStage.setScene(scene);

	                    dialogStage.sizeToScene();
	                    dialogStage.toFront();

	                    Undecorator undecorator = scene.getUndecorator();
	                    dialogStage.setMinWidth(undecorator.getMinWidth());
	                    dialogStage.setMinHeight(undecorator.getMinHeight());

	                    CustomCodeController controller = loader.getController();
	                    controller.setDialogStage(dialogStage);

	                    if(type == OBJECTIVE){
	                    	controller.setCode(problem.customCodeObjective[idx]);
	                    }else if(type == CONSTRAINT){
	                    	controller.setCode(problem.customCodeConstraint[idx]);
	                    }

	                    // Mostra a janela e espera at� o usu�rio fechar.
	                    dialogStage.showAndWait();

	                    if(type == OBJECTIVE){
	                    	problem.customCodeObjective[idx] = controller.getCode();

	                    	//System.out.println(problem.customCodeObjective[idx]);
	                    }else if(type == CONSTRAINT){
	                    	problem.customCodeConstraint[idx] = controller.getCode();
	                    }

	                    //Put the custom code into the objective or constraint



             	} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	  	     }
		});

	   	customCodeVariablesButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
	  	    // Carrega o arquivo fxml e cria um novo stage para a janela popup.
            	BorderPane page = null;
            	try {
		                FXMLLoader loader = new FXMLLoader();
		                loader.setLocation(MainApp.class.getResource("view/CustomCode.fxml"));
		                page = (BorderPane) loader.load();


	                    // Cria o palco dialogStage.
	                    Stage dialogStage = new Stage();
	                    dialogStage.setTitle("Custom Code");
	                    dialogStage.initModality(Modality.WINDOW_MODAL);
	                    dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
	                    UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
	                    dialogStage.setScene(scene);

	                    dialogStage.sizeToScene();
	                    dialogStage.toFront();

	                    Undecorator undecorator = scene.getUndecorator();
	                    dialogStage.setMinWidth(undecorator.getMinWidth());
	                    dialogStage.setMinHeight(undecorator.getMinHeight());

	                    CustomCodeController controller = loader.getController();
	                    controller.setDialogStage(dialogStage);

	                    controller.setCode(problem.customCodeVariables);

	                    // Mostra a janela e espera at� o usu�rio fechar.
	                    dialogStage.showAndWait();


	                    problem.customCodeVariables = controller.getCode();

            	} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	  	     }
		});

	   	customCodeConstructorButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
	  	     @Override
	  	     public void handle(MouseEvent event) {
	  	    // Carrega o arquivo fxml e cria um novo stage para a janela popup.
           	BorderPane page = null;
           	try {
		                FXMLLoader loader = new FXMLLoader();
		                loader.setLocation(MainApp.class.getResource("view/CustomCode.fxml"));
		                page = (BorderPane) loader.load();


	                    // Cria o palco dialogStage.
	                    Stage dialogStage = new Stage();
	                    dialogStage.setTitle("Custom Code");
	                    dialogStage.initModality(Modality.WINDOW_MODAL);
	                    dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
	                    UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
	                    dialogStage.setScene(scene);

	                    dialogStage.sizeToScene();
	                    dialogStage.toFront();

	                    Undecorator undecorator = scene.getUndecorator();
	                    dialogStage.setMinWidth(undecorator.getMinWidth());
	                    dialogStage.setMinHeight(undecorator.getMinHeight());

	                    CustomCodeController controller = loader.getController();
	                    controller.setDialogStage(dialogStage);

	                    controller.setCode(problem.customCodeConstructor);

	                    // Mostra a janela e espera at� o usu�rio fechar.
	                    dialogStage.showAndWait();


	                    problem.customCodeConstructor = controller.getCode();

           	} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	  	     }
		});

    }

    //TODO: Put 2 buttons to change the size dynamically.
    public void renderFormula() {

    	if(idx < 0) return; //To avoid errors

        try {
            String latex = "x=\\frac{-b \\pm \\sqrt {b^2-4ac}}{2a}";

            //create a formula
            TeXFormula formula = null;
            if(type == CONSTRAINT)
            	formula = new TeXFormula(problem.getTex(problem.constraintPart1, idx) +
            			problem.getComparisionSymbol(problem.constraintType.get(idx)) +
            			problem.getTex(problem.constraintPart2, idx));
            else
            	formula = new TeXFormula(problem.getTex(problem.objective, idx));

            //TODO: Change 25 value to a new variable to change size dynamically
            TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, textSize);

            //icon.setIconWidth(25, TeXConstants.ALIGN_CENTER);


            // now create an actual image of the rendered equation
            BufferedImage image = new BufferedImage((int) icon.getIconWidth(),
                    icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
            Graphics2D gg = image.createGraphics();
            gg.setColor(new Color(47, 52, 57));
            gg.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
            JLabel jl = new JLabel();
            jl.setForeground(new Color(33, 150, 243));
            icon.paintIcon(jl, gg, 0, 0);
            // at this point the image is created, you could also save it with ImageIO


            GraphicsContext gc = latexFormulation.getGraphicsContext2D();
            gc.clearRect(0, 0, latexFormulation.getWidth(), latexFormulation.getHeight());
            gc.drawImage(SwingFXUtils.toFXImage(image, null), latexFormulation.getWidth()/2 - icon.getIconWidth()/2, latexFormulation.getHeight()/2 - icon.getIconHeight()/2);


        } catch (Exception ex) {
            ex.printStackTrace();

        }

    }

}
