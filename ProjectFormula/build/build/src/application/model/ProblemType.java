package application.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProblemType{
	private String name;
	private String type;

	public static String TYPE_IMPORTED = "Imported";
	public static String TYPE_NATIVE = "Native";

	public ProblemType() {
		// TODO Auto-generated constructor stub
	}

	public ProblemType(String name, String type) {
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	@XmlElement
	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	@XmlElement
	public void setType(String type) {
		this.type = type;
	}

}
