package application.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import com.sun.deploy.uitoolkit.impl.fx.ui.FXConsole;

import application.ApplicationContext;
import application.model.AlgorithmConfiguration;
import application.model.AlgorithmFactory;
import application.model.AlgorithmParameters;
import application.model.ProblemType;
import box.InformationBox;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.DoubleSpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.util.JMException;
import magius.GeneralProblem;
import undecorator.insidefx.undecorator.UndecoratorScene;

public class CreateParametersController {

	AlgorithmConfiguration algorithmConfiguration;

	AlgorithmParameters algorithmParameters;

	@FXML
	private TextField nameField;

	@FXML
	private Spinner<Integer> evaluations;

	@FXML
	private Spinner<Integer> populationSize;

	@FXML
	private Spinner<Double> mutationRatio;

	@FXML
	private Spinner<Double> crossoverRatio;

	@FXML
	private Spinner<Integer> repetitionsQuant;

	@FXML
	private ComboBox algorithmType;

	@FXML
	private ComboBox problemType;

	@FXML
	private ComboBox initialPopulationType;

	@FXML
	private ComboBox selectionType;

	@FXML
	private ComboBox crossoverType;

	@FXML
	private ComboBox mutationType;

	@FXML
	private ListView<File> instanceList;
	ObservableList<File> instanceFileList;

	@FXML
	private Button instanceFileButton;

	@FXML
	private Button okButton;
	boolean okButtonClicked;

	@FXML
	private Button cancelButton;

	private Stage dialogStage;

	@FXML
	private TextField outputFile;

	/**
     * The constructor is called before initialize()
     */
    public CreateParametersController() {
    	algorithmConfiguration = new AlgorithmConfiguration();

    	for(String s : ApplicationContext.getContext().getProject().getProblemNames()){
    		algorithmConfiguration.addProblem(s);
    	}

    	algorithmParameters = new AlgorithmParameters();

    	okButtonClicked = false;

    	instanceFileList = FXCollections.observableArrayList();

    	algorithmParameters.setName("Default");



	}

    public boolean isOkClicked(){
    	return okButtonClicked;
    }

	public AlgorithmParameters getAlgorithmParameters() {
		return algorithmParameters;
	}

	public void setAlgorithmParameters(AlgorithmParameters algorithmParameters) {
		this.algorithmParameters = algorithmParameters;

		instanceFileList.addAll(algorithmParameters.getInstance());

		loadParametersToScreen();
	}

    /**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {
    	initializeAlgorithm();
    	initializeConfiguration(25000,512,0.2,0.5,1);

    	initButtons();

    	instanceList.setItems(instanceFileList);

     	instanceList.setCellFactory(new Callback<ListView<File>, ListCell<File>>() {
      	     @Override public ListCell<File> call(ListView<File> list) {
      	         return new ListCellHighlight();
      	     }
      	 });


     	nameField.setText(algorithmParameters.getName());

     	outputFile.setText("%i - %r");

    }

    void loadParametersToScreen(){
    	nameField.setText(algorithmParameters.getName());
    	outputFile.setText(algorithmParameters.getOutputPattern());

    	initializeConfiguration((int) algorithmParameters.getMap().get("maxEvaluations"),
    			(int) algorithmParameters.getMap().get("populationSize"),
    			(double) algorithmParameters.getMutation().getParameter("probability"),
    			(double) algorithmParameters.getCrossover().getParameter("probability"),
    			algorithmParameters.getQuantityRuns());


    	algorithmType.setValue(algorithmParameters.getAlgorithm());

   		problemType.setValue(algorithmParameters.getProblem());;


    	selectionType.setValue(algorithmParameters.getSelectionName());


    	mutationType.setValue(algorithmParameters.getMutationName());


    	crossoverType.setValue(algorithmParameters.getCrossoverName());
    }

    public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

    private void initializeConfiguration(int _evaluation, int _population, double _mutation, double _crossover, int _repetition){
    	//Spinner Evaluation
    	IntegerSpinnerValueFactory evaluationsFacotry = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE);
    	evaluationsFacotry.setValue(_evaluation);
    	evaluationsFacotry.setAmountToStepBy(1);
    	evaluations.setValueFactory(evaluationsFacotry);

    	//Spinner Population
    	IntegerSpinnerValueFactory populationSizeFacotry = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE);
    	populationSizeFacotry.setValue(_population);
    	populationSizeFacotry.setAmountToStepBy(1);
    	populationSize.setValueFactory(populationSizeFacotry);

    	//Spinner Mutation
    	DoubleSpinnerValueFactory mutationRatioFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(0, 1);
    	mutationRatioFactory.setValue(_mutation);
    	mutationRatioFactory.setAmountToStepBy(0.01);
    	mutationRatioFactory.setMin(0.0001);
    	mutationRatio.setValueFactory(mutationRatioFactory);

    	//Spinner Crossover
    	DoubleSpinnerValueFactory crossoverRatioFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(0, 1);
    	crossoverRatioFactory.setValue(_crossover);
    	crossoverRatioFactory.setAmountToStepBy(0.01);
    	crossoverRatioFactory.setMin(0.0001);
    	crossoverRatio.setValueFactory(crossoverRatioFactory);


    	IntegerSpinnerValueFactory repetitionsFacotry = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE);
    	repetitionsFacotry.setValue(_repetition);
    	repetitionsFacotry.setAmountToStepBy(1);
    	repetitionsQuant.setValueFactory(repetitionsFacotry);
    }

    private void initializeAlgorithm(){
    	algorithmType.getItems().addAll(algorithmConfiguration.getAlgorithms());
    	if(algorithmConfiguration.getAlgorithms().size() > 0)
    		algorithmType.setValue(algorithmConfiguration.getAlgorithms().get(0));;

    	problemType.getItems().addAll(algorithmConfiguration.getProblems());
    	if(algorithmConfiguration.getProblems().size() > 0)
    		problemType.setValue(algorithmConfiguration.getProblems().get(0));;

    	initialPopulationType.getItems().addAll(algorithmConfiguration.getInitialPopulations());
   		if(algorithmConfiguration.getInitialPopulations().size() > 0)
   			initialPopulationType.setValue(algorithmConfiguration.getInitialPopulations().get(0));

   		selectionType.getItems().addAll(algorithmConfiguration.getSelections());
    	if(algorithmConfiguration.getSelections().size() > 0)
    		selectionType.setValue(algorithmConfiguration.getSelections().get(0));

		mutationType.getItems().addAll(algorithmConfiguration.getMutations());
    	if(algorithmConfiguration.getMutations().size() > 0)
    		mutationType.setValue(algorithmConfiguration.getMutations().get(0));

    	crossoverType.getItems().addAll(algorithmConfiguration.getCrossovers());
    	if(algorithmConfiguration.getCrossovers().size() > 0)
    		crossoverType.setValue(algorithmConfiguration.getCrossovers().get(0));
    }

    String decodeOutputFile(String file, String instance){

    	//Remove extension
    	file = file.replace("%i", instance.replaceFirst("[.][^.]+$", ""));

    	return file;
    }

    public void initButtons(){

    	cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	dialogStage.close();
            }
    	});

    	instanceFileButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            	FileChooser chooser = new FileChooser();
            	chooser.setTitle("Load Instances");
            	File defaultDirectory = new File(ApplicationContext.getContext().getProject().getPath().toString());
            	chooser.setInitialDirectory(defaultDirectory);

            	 List<File> list =
            			 chooser.showOpenMultipleDialog(ApplicationContext.getContext().getMainApp().getStage());
                 if (list != null) {
                	 //algorithmParameters.getInstance().clear();
                	 //algorithmParameters.getInstance().addAll(list);

                	 instanceFileList.clear();
                	 instanceFileList.addAll(list);
                 }
            }
        });

    	okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            	if(nameField.getText().isEmpty()){
            		InformationBox ib = new InformationBox("Error", "The configuration need to have a name.", 18);
            		ib.show();
            	}else{

	            	System.out.println(algorithmParameters);

	            	algorithmParameters.setName(nameField.getText());

					// Algorithm parameters
					algorithmParameters.addParameter("populationSize",populationSize.getValue());
					algorithmParameters.addParameter("archiveSize",populationSize.getValue());
					algorithmParameters.addParameter("maxEvaluations",evaluations.getValue());

				    // Mutation and Crossover for Real codification
					try {
						algorithmParameters.setCrossover(crossoverType.getValue().toString(), crossoverRatio.getValue());
						algorithmParameters.setMutation(mutationType.getValue().toString(), mutationRatio.getValue());

					    // Selection Operator
						algorithmParameters.setSelection(selectionType.getValue().toString());

						algorithmParameters.setAlgorithm(algorithmType.getValue().toString());

					} catch (JMException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					algorithmParameters.setInstance(instanceFileList);

					algorithmParameters.setProblem(problemType.getValue().toString());

					algorithmParameters.setOutputPattern(outputFile.getText());

					algorithmParameters.setQuantityRuns(repetitionsQuant.getValue());


	            	okButtonClicked = true;
	            	dialogStage.close();
            	}
            }
    	});

    }

    public Problem compileAndLoad(String problemName, String instancePath) throws MalformedURLException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException{
    	// Prepare source somehow.
    	Problem problem = null;

    	// Save source in .java file.
    	File root = new File(ApplicationContext.getContext().getProject().getPath() + "\\Problems\\");
    	File sourceFile = new File(root, problemName + ".java");
    	//sourceFile.getParentFile().mkdirs();
    	//Files.write(sourceFile.toPath(), source, StandardCharsets.UTF_8);

    	// Compile source file.
    	JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
    	//System.out.println(sourceFile.getPath());
    	compiler.run(null, null, null, sourceFile.getPath());

    	URL url = root.toURL();          // file:/c:/myclasses/
        URL[] urls = new URL[]{url};

        ClassLoader cl = new URLClassLoader(urls);
        Class cls = cl.loadClass(problemName);
        System.out.println(cls.getName());
        Constructor constructor = cls.getConstructor(String.class);
        problem = (Problem) constructor.newInstance(instancePath);

        return problem;

    	// Load and instantiate compiled class.
    	/*URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] { root.toURI().toURL() });
    	Class<?> cls = Class.forName("Test", true, classLoader); // Should print "hello".
    	Object instance = cls.newInstance(); // Should print "world".
    	System.out.println(instance); // Should print "test.Test@hashcode".*/
    }

    public void dialogException(Exception ex){
    	Alert alert = new Alert(AlertType.ERROR);
    	alert.setTitle("Exception Dialog");
    	alert.setHeaderText("Look, an Exception Dialog");
    	alert.setContentText("Could not find file blabla.txt!");

    	// Create expandable Exception.
    	StringWriter sw = new StringWriter();
    	PrintWriter pw = new PrintWriter(sw);
    	ex.printStackTrace(pw);
    	String exceptionText = sw.toString();

    	Label label = new Label("The exception stacktrace was:");

    	TextArea textArea = new TextArea(exceptionText);
    	textArea.setEditable(false);
    	textArea.setWrapText(true);

    	textArea.setMaxWidth(Double.MAX_VALUE);
    	textArea.setMaxHeight(Double.MAX_VALUE);
    	GridPane.setVgrow(textArea, Priority.ALWAYS);
    	GridPane.setHgrow(textArea, Priority.ALWAYS);

    	GridPane expContent = new GridPane();
    	expContent.setMaxWidth(Double.MAX_VALUE);
    	expContent.add(label, 0, 0);
    	expContent.add(textArea, 0, 1);

    	// Set expandable Exception into the dialog pane.
    	alert.getDialogPane().setExpandableContent(expContent);

    	alert.showAndWait();
    }

}
