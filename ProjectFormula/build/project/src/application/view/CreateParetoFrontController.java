package application.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sun.prism.paint.Color;

import application.ApplicationContext;
import box.InformationBox;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.util.Ranking;
import magius.GeneralProblem;
import magius.GeneralVariable;

public class CreateParetoFrontController {

	@FXML
	private TextField output;

	@FXML
	private Button loadPopulationsButton;

	@FXML
	private ListView<File> fileView;
	ObservableList<File> populationFileList;

	@FXML
	private Button cancelButton;

	@FXML
	private Button okButton;

	boolean okButtonClicked;

	private Stage dialogStage;

	public CreateParetoFrontController() {
		okButtonClicked = false;

		populationFileList = FXCollections.observableArrayList();
	}

	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

	/**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {
    	okButtonController();
    	cancelButtonController();

    	fileView.setItems(populationFileList);

    	fileView.setCellFactory(new Callback<ListView<File>, ListCell<File>>() {
	   	     @Override public ListCell<File> call(ListView<File> list) {
	   	         return new ListCellHighlight();
	   	     }
	   	 });

    	loadPopulationsButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            	FileChooser chooser = new FileChooser();
            	chooser.setTitle("Load Instances");
            	File defaultDirectory = new File(ApplicationContext.getContext().getProject().getPath().toString());
            	chooser.setInitialDirectory(defaultDirectory);

            	 List<File> list =
            			 chooser.showOpenMultipleDialog(ApplicationContext.getContext().getMainApp().getStage());
                 if (list != null) {
                	 populationFileList.clear();
                	 populationFileList.addAll(list);
                 }
            }
        });
    }


    public boolean isOkClicked(){
    	return okButtonClicked;
    }


    private boolean isInteger(TextField field){
    	try
    	  {
    	    Integer value = Integer.parseInt(field.getText());
    	  }
    	  catch(NumberFormatException nfe)
    	  {
    	    return false;
    	  }
    	  return true;
    }

    private void okButtonController(){
    	okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            	try {
            		//Get number of objectives
					BufferedReader br = new BufferedReader(new FileReader(populationFileList.get(0)));

					String firstLine[] = br.readLine().split(" ");

					int numberOfObjectives = firstLine.length;

					System.out.println("NumberOfObjectives: " + numberOfObjectives);

					br.close();

					//Put all solutions to a solutionSet

					SolutionSet population;

					population = new SolutionSet(128);

					for(int i=0;i<populationFileList.size();++i){
						br = new BufferedReader(new FileReader(populationFileList.get(i)));

						String line;
						while ((line = br.readLine()) != null) {
							String splitted[] = line.split(" ");
							Solution solution = new Solution(numberOfObjectives);

							for(int j=0;j<numberOfObjectives;++j){
								solution.setObjective(j, Double.valueOf(splitted[j]));
							}

							if(population.size() + 1 >= population.getMaxSize())
								population.setCapacity(population.getMaxSize() * 2);
							population.add(solution);
						}

						br.close();
					}

					Ranking ranking = new Ranking(population);

					File outputFile = new File(ApplicationContext.getContext().getProject().getPath() + "\\Solutions\\" + output.getText() +  ".fun");
				    outputFile.getParentFile().mkdirs();
				    outputFile.createNewFile();

				    ranking.getSubfront(0).printObjectivesToFile(outputFile.getAbsolutePath());

				    InformationBox ib = new InformationBox("Pareto Front", "Pareto front created in " + ApplicationContext.getContext().getProject().getPath() + "\\Solutions\\" + output.getText() +  ".fun", 14);
				    ib.show();


				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


        		okButtonClicked = true;
            	dialogStage.close();
            }
    	});
    }

    private void cancelButtonController(){
    	cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	dialogStage.close();
            }
    	});
    }

}
