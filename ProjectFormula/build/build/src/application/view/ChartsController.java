package application.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import com.sun.deploy.uitoolkit.impl.fx.ui.FXConsole;

import application.ApplicationContext;
import application.MainApp;
import application.model.AlgorithmConfiguration;
import application.model.AlgorithmFactory;
import application.model.ChartPopulation;
import application.model.ProblemType;
import box.ConfirmationBox;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.Axis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.DoubleSpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.util.JMException;
import magius.GeneralProblem;
import magius.GeneralVariable;
import undecorator.insidefx.undecorator.Undecorator;
import undecorator.insidefx.undecorator.UndecoratorScene;

public class ChartsController {

	@FXML
	private ImageView backButton;

	@FXML
	private Button addChartButton;

	@FXML
	private Button editChartButton;

	@FXML
	private Button deleteChartButton;

	@FXML
	private ListView<ChartPopulation> fileView;

	@FXML
    private NumberAxis xAxis;

    @FXML
    private NumberAxis yAxis;

	@FXML
	private ScatterChart<Double, Double> scatterChart;

	ObservableList<ChartPopulation> chartList;

	ContextMenu chartContextMenu;



	/**
     * The constructor is called before initialize()
     */
    public ChartsController() {
    	chartList = FXCollections.observableArrayList();
	}

    /**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {

    	fileView.setItems(chartList);

    	scatterChart.setAnimated(false);

    	/*if(ApplicationContext.getContext().getProject().getChart() == null){
    		List<ChartPopulation>  tmp = new ArrayList<ChartPopulation>();
    		ApplicationContext.getContext().getProject().setChart(tmp);
    	}*/

    	System.out.println(ApplicationContext.getContext().getProject().getChart().size());

    	for(ChartPopulation cp : ApplicationContext.getContext().getProject().getChart()){
    		System.out.println("AQUI");
    		chartList.add(cp);
    	}


    	backButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
    	     @Override
    	     public void handle(MouseEvent event) {
    	    	 ApplicationContext.getContext().getMainApp().loadScene("MainMenu");
    	    	 ApplicationContext.getContext().getMainApp().setScene();

    	     }
    	});



    	deleteChartButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

    	    @Override
    	    public void handle(MouseEvent click) {

    	    	if(!fileView.getSelectionModel().isEmpty()){
    	    		ConfirmationBox confirmationBox = new ConfirmationBox("Delete", "Do you really want delete the chart " + fileView.getSelectionModel().getSelectedItem().getName(), 18);

    	    		if(confirmationBox.show()){
    	    			System.out.println("Deleting char " + fileView.getSelectionModel().getSelectedItem());

    	    			ApplicationContext.getContext().getProject().getChart().remove(fileView.getSelectionModel().getSelectedItem());
    	    			ApplicationContext.getContext().getProject().save();

    	    			chartList.remove(fileView.getSelectionModel().getSelectedItem());


    	    		}
    	    	}


    	    }

    	});

    	addChartButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

    	    @Override
    	    public void handle(MouseEvent click) {
    	        	   BorderPane page = null;
    	            	try{
    	                	FXMLLoader loader = new FXMLLoader();
    		                loader.setLocation(MainApp.class.getResource("view/AddChart.fxml"));
    		                page = (BorderPane) loader.load();


    	                    // Cria o palco dialogStage.
    	                    Stage dialogStage = new Stage();
    	                    dialogStage.setTitle("Create new chart");
    	                    dialogStage.initModality(Modality.WINDOW_MODAL);
    	                    dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
    	                    UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
    	                    dialogStage.setScene(scene);

    	                    dialogStage.sizeToScene();
    	                    dialogStage.toFront();

    	                    Undecorator undecorator = scene.getUndecorator();
    	                    dialogStage.setMinWidth(undecorator.getMinWidth());
    	                    dialogStage.setMinHeight(undecorator.getMinHeight());

    	                    // Define a pessoa no controller.
    	                    AddChartController controller = loader.getController();
    	                    controller.setDialogStage(dialogStage);

    	                    // Mostra a janela e espera at� o usu�rio fechar.
    	                    dialogStage.showAndWait();

    	                    if(controller.isOkClicked()){

    	                    	System.out.println("Aqui");

    	                    	chartList.add(controller.getChart());

    	                    	ApplicationContext.getContext().getProject().getChart().add(controller.getChart());
    	                    	ApplicationContext.getContext().getProject().save();


    	                    }

    		            } catch (IOException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					}

    	    }

    	});

    	editChartButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

    	    @Override
    	    public void handle(MouseEvent click) {
    	        	   BorderPane page = null;
    	            	try{
    	                	FXMLLoader loader = new FXMLLoader();
    		                loader.setLocation(MainApp.class.getResource("view/AddChart.fxml"));
    		                page = (BorderPane) loader.load();


    	                    // Cria o palco dialogStage.
    	                    Stage dialogStage = new Stage();
    	                    dialogStage.setTitle("Edit chart");
    	                    dialogStage.initModality(Modality.WINDOW_MODAL);
    	                    dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
    	                    UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
    	                    dialogStage.setScene(scene);

    	                    dialogStage.sizeToScene();
    	                    dialogStage.toFront();

    	                    Undecorator undecorator = scene.getUndecorator();
    	                    dialogStage.setMinWidth(undecorator.getMinWidth());
    	                    dialogStage.setMinHeight(undecorator.getMinHeight());

    	                    // Define a pessoa no controller.
    	                    AddChartController controller = loader.getController();
    	                    controller.setDialogStage(dialogStage);

    	                    controller.setChart(fileView.getSelectionModel().getSelectedItem());

    	                    // Mostra a janela e espera at� o usu�rio fechar.
    	                    dialogStage.showAndWait();

    	                    if(controller.isOkClicked()){

    	                    	System.out.println("Editing");

    	                    	chartList.set(fileView.getSelectionModel().getSelectedIndex(), controller.getChart());

    	                    	ApplicationContext.getContext().getProject().getChart().set(fileView.getSelectionModel().getSelectedIndex(), controller.getChart());
    	                    	ApplicationContext.getContext().getProject().save();


    	                    }

    		            } catch (IOException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					}

    	    }

    	});

    	fileView.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                System.out.println("clicked on " + fileView.getSelectionModel().getSelectedItem());

                if(fileView.getSelectionModel().getSelectedIndex() < chartList.size())
                	loadChart(fileView.getSelectionModel().getSelectedItem());


            }
        });

    	//Add Text formulation to clipboard
    	chartContextMenu = new ContextMenu();
    	MenuItem copyTex = new MenuItem("Copy Chart");
    	copyTex.setOnAction(new EventHandler<ActionEvent>() {
    	    public void handle(ActionEvent e) {
    	        System.out.println("Copied to clipboard");

                Clipboard clipboard = Clipboard.getSystemClipboard();
                ClipboardContent content = new ClipboardContent();
                content.putImage(scatterChart.snapshot(new SnapshotParameters(), null));
                clipboard.setContent(content);
    	    }
    	});

    	chartContextMenu.getItems().add(copyTex);

    	scatterChart.addEventHandler(MouseEvent.MOUSE_CLICKED,
    	    new EventHandler<MouseEvent>() {
    	        @Override public void handle(MouseEvent e) {
    	            if (e.getButton() == MouseButton.SECONDARY)
    	            	chartContextMenu.show(scatterChart, e.getScreenX(), e.getScreenY());
    	        }
    	});

    	/*final double SCALE_DELTA = 1.1;
    	scatterChart.setOnScroll(new EventHandler<ScrollEvent>() {
    	    public void handle(ScrollEvent event) {
    	        event.consume();

    	        if (event.getDeltaY() == 0) {
    	            return;
    	        }

    	        double scaleFactor = (event.getDeltaY() > 0) ? SCALE_DELTA : 1 / SCALE_DELTA;

    	        scatterChart.setScaleX(scatterChart.getScaleX() * scaleFactor);
    	        scatterChart.setScaleY(scatterChart.getScaleY() * scaleFactor);
    	    }
    	});

    	scatterChart.setOnMousePressed(new EventHandler<MouseEvent>() {
    	    public void handle(MouseEvent event) {
    	        if (event.getClickCount() == 2) {
    	        	scatterChart.setScaleX(1.0);
    	        	scatterChart.setScaleY(1.0);
    	        }
    	    }
    	});*/

    	/*scatterChart.setOnScroll(new EventHandler<ScrollEvent>() {
            @Override
            public void handle(ScrollEvent ev) {
                double zoomFactor = 1.05;
                double deltaY = ev.getDeltaY();

                if (deltaY < 0) {
                    zoomFactor = 2 - zoomFactor;
                }

                System.out.println("DeltaX = " + ev.getDeltaX());
                System.out.println("DeltaY = " + ev.getDeltaY());
                System.out.println("Zoomfactor = " + zoomFactor);

                Axis<Double> xAxisLocal = (scatterChart.getXAxis());

                xAxisLocal.setUpperBound(xAxisLocal.getLayoutBounds().get * zoomFactor);
                xAxisLocal.setLowerBound(xAxisLocal.getLowerBound() * zoomFactor);
                xAxisLocal.setTickUnit(xAxisLocal.getTickUnit() * zoomFactor);

                ev.consume();
            }
        });*/
    }

    String removeExtension(String word){
    	if (word.indexOf(".") > 0)
    		word = word.substring(0, word.lastIndexOf("."));

    	return word;

    }

    private void loadChart(ChartPopulation chart){

    	scatterChart.getData().clear();

         scatterChart.setTitle(chart.getTitle());

         double limitX[] = new double[2], limitY[] = new double[2];

         limitX[0] = Double.MAX_VALUE;
         limitY[0] = Double.MAX_VALUE;

         limitX[1] = -Double.MAX_VALUE;
         limitY[1] = -Double.MAX_VALUE;


    	 int n = chart.getFile().size();
    	 XYChart.Series serie[] = new XYChart.Series[n];

    	 for(int i=0;i<n;i++){
    		 serie[i] = new XYChart.Series();
    		 serie[i].setName(removeExtension(chart.getFile().get(i).getName()));
    		 System.out.println(serie[i].getName());

    		 try {
    			BufferedReader br = new BufferedReader(new FileReader(chart.getFile().get(i)));
			    String line;
			    while ((line = br.readLine()) != null) {
			       String splitted[] = line.split(" ");
			       //System.out.println(line);
			       serie[i].getData().add(new XYChart.Data(Double.valueOf(splitted[chart.getxAxis()]), Double.valueOf(splitted[chart.getyAxis()])));

			       if(Double.valueOf(splitted[chart.getxAxis()]) < limitX[0]) limitX[0] = Double.valueOf(splitted[chart.getxAxis()]);
			       if(Double.valueOf(splitted[chart.getxAxis()]) > limitX[1]) limitX[1] = Double.valueOf(splitted[chart.getxAxis()]);

			       if(Double.valueOf(splitted[chart.getyAxis()]) < limitY[0]) limitY[0] = Double.valueOf(splitted[chart.getyAxis()]);
			       if(Double.valueOf(splitted[chart.getyAxis()]) > limitY[1]) limitY[1] = Double.valueOf(splitted[chart.getyAxis()]);

			       //System.out.println(Double.valueOf(splitted[chart.getxAxis()]) + " " +  Double.valueOf(splitted[chart.getyAxis()]));
			    }
    		 } catch (IOException e) {
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			}

    		scatterChart.getData().add(serie[i]);

    	 }

    	 double distanceX = Math.abs(limitX[0] - limitX[1]);
    	 System.out.println("Distance in X: " + distanceX + "(" + limitX[0] + "," + limitX[1] + ")");
    	 xAxis.setAutoRanging(false);
     	 xAxis.setLowerBound(limitX[0] - (0.1 * distanceX));
	   	 xAxis.setUpperBound(limitX[1] + (0.1 * distanceX));
	   	 xAxis.setTickUnit(distanceX / 5);
	   	 xAxis.setLabel(chart.getxAxisName());


	   	double distanceY = Math.abs(limitY[0] - limitY[1]);
	   	System.out.println("Distance in Y: " + distanceY + "(" + limitY[0] + "," + limitY[1] + ")" + " Tick: " + distanceY / 5);
	   	 yAxis.setAutoRanging(false);
	   	 yAxis.setLowerBound(limitY[0] - (0.1 * distanceY));
	   	 yAxis.setUpperBound(limitY[1] + (0.1 * distanceY));
	   	 yAxis.setTickUnit(distanceY / 5);
	   	 yAxis.setLabel(chart.getyAxisName());
    }


}
