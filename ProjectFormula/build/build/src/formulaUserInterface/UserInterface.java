package formulaUserInterface;

import java.io.IOException;

import application.ApplicationContext;
import application.model.GeneralSettings;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import undecorator.insidefx.undecorator.Undecorator;
import undecorator.insidefx.undecorator.UndecoratorScene;

public class UserInterface {

	private Stage stage;
    private BorderPane rootLayout;
    UndecoratorScene scene;
    FXMLLoader loader;

    String title;
    String fxml;

    double width, height;

    public UserInterface(String title, String fxml, double width, double height, Stage primaryStage){
    	this.title = title;
    	this.fxml = fxml;
    	this.width = width;
    	this.height = height;

    	this.stage = primaryStage;
        this.stage.setTitle(title);

    }


    /*@Override
    public void start(Stage primaryStage) {


    }*/

    public Stage getStage(){
    	return stage;
    }

    /**
     * Inicializa o root layout (layout base).
     */
    public void show(Class c) {
        try {
            // Carrega o root layout do arquivo fxml.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(c.getResource(fxml));
            rootLayout = (BorderPane) loader.load();

            // Mostra a scene (cena) contendo oroot layout.
            UndecoratorScene scene = new UndecoratorScene(stage, rootLayout);
            // Overrides defaults
            //scene.addStylesheet("undecorator/demoapp/demoapp.css");
            //Scene scene = new Scene(rootLayout);
            stage.setScene(scene);

            stage.sizeToScene();
            stage.toFront();

            Undecorator undecorator = scene.getUndecorator();
            undecorator.setMinWidth(width);
            undecorator.setMinHeight(height);

            stage.setMinWidth(undecorator.getMinWidth());
            stage.setMinHeight(undecorator.getMinHeight());

            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
