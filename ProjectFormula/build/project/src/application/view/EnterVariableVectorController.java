package application.view;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.sun.prism.paint.Color;

import application.ApplicationContext;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import magius.GeneralProblem;
import magius.GeneralVariable;

public class EnterVariableVectorController {

	@FXML
	private Text typeField;

	@FXML
	private Text variableName;

	@FXML
	private TextField numberIndexOne;

	@FXML
	private TextField numberIndexTwo;

	@FXML
	private Button cancelButton;

	@FXML
	private Button okButton;

	boolean okButtonClicked;

	private Stage dialogStage;

	private String type;

	private int objectiveType;

	private int objectiveIdx;

	private GeneralProblem problem;

	private GeneralVariable variable;

	public EnterVariableVectorController() {
		okButtonClicked = false;
		setObjectiveIdx(0);
	}

	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

	/**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {
    	okButtonController();
    	cancelButtonController();

    	lockInput(false);

    }

    private void lockInput(boolean value){
    	if(value){
    		numberIndexOne.setEditable(false);
    		numberIndexTwo.setEditable(false);
    		numberIndexOne.setStyle("-fx-background-color: #555555;");
    		numberIndexTwo.setStyle("-fx-background-color: #555555;");
    	}else{
    		numberIndexOne.setEditable(true);
    		numberIndexTwo.setEditable(true);
    		numberIndexOne.setStyle("-fx-background-color: #FF000000;");
    		numberIndexTwo.setStyle("-fx-background-color: #FF000000;");
    	}

    }

    public boolean isOkClicked(){
    	return okButtonClicked;
    }


    private boolean isInteger(TextField field){
    	try
    	  {
    	    Integer value = Integer.parseInt(field.getText());
    	  }
    	  catch(NumberFormatException nfe)
    	  {
    	    return false;
    	  }
    	  return true;
    }

    private void okButtonController(){
    	okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            	//Validate
            	//if((!numberIndex.getText().equals("") && isInteger(numberIndex))){

            		okButtonClicked = true;

            		dialogStage.close();

            	/*}else{
            		if(!isInteger(numberIndex)){
            			numberIndex.setStyle("-fx-background-color: #FF5B5E;");
            		}
            	}*/


            }
    	});
    }

    private void cancelButtonController(){
    	cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	dialogStage.close();
            }
    	});
    }

	public int getObjectiveType() {
		return objectiveType;
	}

	public void setObjectiveType(int objectiveType) {
		this.objectiveType = objectiveType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
		typeField.setText(type);

		numberIndexTwo.setDisable(type.contentEquals("Vector"));
	}

	public GeneralVariable getVariable() {
		return variable;
	}

	public void setVariable(GeneralVariable value) {
		this.variable = value;
		variableName.setText(variable.name);
	}

	public void setProblem(GeneralProblem problem){
		this.problem = problem;
	}

	public int getObjectiveIdx() {
		return objectiveIdx;
	}

	public void setObjectiveIdx(int objectiveIdx) {
		this.objectiveIdx = objectiveIdx;
	}

	public List<String> getResult(){
		List<String> result = new ArrayList<String>();
		result.clear();

		//Looking for variable
		for(int i=0;i<problem.variable.size();i++){
			if(problem.variable.get(i).name.equals(variable.name)){
				result.add("V" + i);
				break;
			}
		}

		if(result.size() == 0) return result;

		if(!numberIndexOne.getText().isEmpty())
			result.add("" + numberIndexOne.getText());

		if(!numberIndexTwo.getText().isEmpty())
			result.add("" + numberIndexTwo.getText());

		return result;
	}


}
