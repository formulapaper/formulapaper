package application.view;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.sun.prism.paint.Color;

import application.ApplicationContext;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import magius.GeneralProblem;
import magius.GeneralVariable;

public class EnterLoopController {

	@FXML
	private Text typeField;

	@FXML
	private TextField initialText;

	@FXML
	private TextField finalText;

	@FXML
	private ComboBox<String> identifierBox;

	@FXML
	private Button cancelButton;

	@FXML
	private Button okButton;

	boolean okButtonClicked;

	private Stage dialogStage;

	private String type;


	private GeneralProblem problem;

	public EnterLoopController() {
		okButtonClicked = false;
	}

	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

	/**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {
    	okButtonController();
    	cancelButtonController();

    	//updateIdentifier();

    	initialText.setText("0");
    	finalText.setText("N-1");
    }

    public boolean isOkClicked(){
    	return okButtonClicked;
    }

    public void updateIdentifier(){
    	List<String> identifierList = new ArrayList<String>();
    	identifierList.clear();
    	boolean have;
    	for(int i='a';i<='z';i++){
    		String tmp = "" + ((char) i);
    		System.out.println(tmp);
    		have = false;
    		for(int j=0;j<problem.loop.size();j++){
    			if(tmp.equals(problem.loop.get(j).identifier)){
    				have = true;
    				break;
    			}
    		}
    		if(!have){
    			identifierList.add(tmp);
    		}
    	}
    	identifierBox.getItems().clear();
    	identifierBox.getItems().addAll(identifierList);
    	if(identifierList.size() > 0)
    		identifierBox.setValue(identifierList.get(0));
    }

    /*public void updateAssociation(){
    	List<String> associationList = new ArrayList<String>();

    	associationList.add("");

    	for(GeneralVariable gv : problem.variable){
    		if(gv.type.equals("Normal")){
    			associationList.add(gv.name);
    		}
    	}

    	initialAssociation.getItems().clear();
    	finalAssociation.getItems().clear();

    	initialAssociation.getItems().addAll(associationList);
    	finalAssociation.getItems().addAll(associationList);

    	if(associationList.size() > 0){
    		initialAssociation.setValue(associationList.get(0));
    		finalAssociation.setValue(associationList.get(0));
    	}
    }*/


    /*private boolean isInteger(TextField field){
    	try
    	  {
    	    Integer value = Integer.parseInt(field.getText());
    	  }
    	  catch(NumberFormatException nfe)
    	  {
    	    return false;
    	  }
    	  return true;
    }*/

    private void okButtonController(){
    	okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            	//Validate
            	if(!initialText.getText().equals("") && !finalText.getText().equals("")/* && isInteger(initialText) && !finalText.getText().equals("") && isInteger(finalText)*/){

            		okButtonClicked = true;

            		dialogStage.close();

            	}else{
            		if(initialText.getText().equals("")){
            			initialText.setStyle("-fx-background-color: #FF5B5E;");
            		}

            		if(finalText.getText().equals("")){
            			finalText.setStyle("-fx-background-color: #FF5B5E;");
            		}
            	}


            }
    	});
    }

    private void cancelButtonController(){
    	cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	dialogStage.close();
            }
    	});
    }

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
		typeField.setText(type);
	}

	public String getIdentifier(){
		return identifierBox.getSelectionModel().getSelectedItem();
	}

	public String getInitial(){
		return initialText.getText().toString();
	}

	public String getFinal(){
		return finalText.getText().toString();
	}

	public void setProblem(GeneralProblem problem){
		this.problem = problem;
	}


}
