package application.model;

public class ProblemModel{
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ProblemModel(String name){
		this.name = name;
	}
}