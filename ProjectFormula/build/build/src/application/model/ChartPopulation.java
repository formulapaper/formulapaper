package application.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ChartPopulation {

	private String name, title;

	private String xAxisName, yAxisName;

	private int xAxis, yAxis;

	private List<File> file;

	public ChartPopulation(){
		setFile(new ArrayList<File>());

		setxAxis(0);
		setyAxis(1);

	}

	public String getName() {
		return name;
	}

	@XmlElement
	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	@XmlElement
	public void setTitle(String title) {
		this.title = title;
	}

	public int getxAxis() {
		return xAxis;
	}

	@XmlElement
	public void setxAxis(int xAxis) {
		this.xAxis = xAxis;
	}

	public int getyAxis() {
		return yAxis;
	}

	@XmlElement
	public void setyAxis(int yAxis) {
		this.yAxis = yAxis;
	}

	public List<File> getFile() {
		return file;
	}

	@XmlElement
	public void setFile(List<File> file) {
		this.file = file;
	}

	@Override
	public String toString() {
		return name;
	}

	public String getxAxisName() {
		return xAxisName;
	}

	@XmlElement
	public void setxAxisName(String xAxisName) {
		this.xAxisName = xAxisName;
	}

	public String getyAxisName() {
		return yAxisName;
	}

	@XmlElement
	public void setyAxisName(String yAxisName) {
		this.yAxisName = yAxisName;
	}





}
