package application.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import com.sun.deploy.uitoolkit.impl.fx.ui.FXConsole;

import application.ApplicationContext;
import application.MainApp;
import application.model.AlgorithmConfiguration;
import application.model.AlgorithmFactory;
import application.model.AlgorithmParameters;
import application.model.ProblemType;
import box.InformationBox;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.DoubleSpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.util.JMException;
import magius.GeneralProblem;
import undecorator.insidefx.undecorator.Undecorator;
import undecorator.insidefx.undecorator.UndecoratorScene;

public class RunAlgorithmController {

	AlgorithmConfiguration algorithmConfiguration;

	AlgorithmParameters algorithmParameters;

	@FXML
	private ImageView backButton;

	@FXML
	private Label algorithmName;

	@FXML
	private ProgressBar progressBar;

	@FXML
	private ProgressBar instanceProgressBar;

	public DoubleProperty instancesProgress;

	@FXML
	private ListView<File> instanceList;

	ObservableList<File> instanceFileList;

	@FXML
	private Button runButton;

	@FXML
	private Button cancelButton;

	@FXML
	private Button configurationButton;

	@FXML
	private TextArea debugConsole;
	private OutputStream outConsole;


	/**
     * The constructor is called before initialize()
     */
    public RunAlgorithmController() {
    	algorithmConfiguration = new AlgorithmConfiguration();

    	algorithmParameters = null;

    	for(String s : ApplicationContext.getContext().getProject().getProblemNames()){
    		algorithmConfiguration.addProblem(s);
    	}

    	instanceFileList = FXCollections.observableArrayList();
	}

    /**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {

    	initButtons();

    	instanceList.setItems(instanceFileList);

    	instanceList.setCellFactory(new Callback<ListView<File>, ListCell<File>>() {
    	     @Override public ListCell<File> call(ListView<File> list) {
    	         return new ListCellHighlight();
    	     }
    	 });

    	instancesProgress = new SimpleDoubleProperty(0);

    	//BackButton Action
    	backButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
    	     @Override
    	     public void handle(MouseEvent event) {
    	    	 ApplicationContext.getContext().getMainApp().loadScene("MainMenu");
    	    	 ApplicationContext.getContext().getMainApp().setScene();

    	     }
    	});

    	//Tooltip
    	progressBar.setTooltip(
    			new Tooltip("Algorithm progress bar")
    	);

    	instanceProgressBar.setTooltip(
    			new Tooltip("Instance progress bar")
    	);

    	ApplicationContext.getContext().initializeConsole(debugConsole);

    }


    String decodeOutputFile(String file, String instance, int run){

    	//Remove extension
    	file = file.replaceAll("%i", instance.replaceFirst("[.][^.]+$", ""));

    	file = file.replaceAll("%r", String.valueOf(run).replaceFirst("[.][^.]+$", ""));

    	return file;
    }

    public void initButtons(){
    	runButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //Test load a class from file
                try {

                	if(algorithmParameters == null){
                		InformationBox ib = new InformationBox("Error", "Insert a valid configuration before run the algorithm.", 18);
                		ib.show();
                	}else if(!algorithmParameters.getInstance().isEmpty()){

                		algorithmName.setText(algorithmParameters.getAlgorithm());

                		new Thread(){
					    	@Override
					    	public void run(){

					    			Problem problem = null;

							    	instanceProgressBar.progressProperty().bind(instancesProgress);
							    	ApplicationContext.getContext().isRunningAlgorithm.set(true);


			                		for(int i=0;i<algorithmParameters.getInstance().size();i++){
			                			if(ApplicationContext.getContext().isRunningAlgorithm.getValue() == false){
			                				instanceProgressBar.progressProperty().unbind();
			                				instanceProgressBar.setProgress(0);
			                				break;
			                			}else{
			                				instanceList.getFocusModel().focus(i);
			                				instancesProgress.set(  (double) i /algorithmParameters.getInstance().size());
			                			}
			                			try {

			                				//Looking if the problem is in the project. Why?
			                				//TODO: Need review this.
			                				for(ProblemType pt : ApplicationContext.getContext().getProject().getProblems()){
			                					//Imported problem
			                					if(pt.getName().equals(algorithmParameters.getProblem()) /*&& pt.getType().equals(ProblemType.TYPE_IMPORTED)*/){
			                						problem = compileAndLoad(algorithmParameters.getProblem(), algorithmParameters.getInstance().get(i).getPath());
			                						break;
			                					}
			                				}

										    //ProgressBar
										    progressBar.progressProperty().unbind();
										    progressBar.setProgress(0);
										    progressBar.progressProperty().bind(ApplicationContext.getContext().algorithmProgress);

										    //Add the operators to the algorithm
										    ApplicationContext.getContext().algorithmInitialize(problem, algorithmParameters);

										    SolutionSet population = ApplicationContext.getContext().algorithm.execute();

										    //Creating output file FUN
										    File output = new File(ApplicationContext.getContext().getProject().getPath() + "\\Solutions\\" + algorithmParameters.getProblem() + "\\" + decodeOutputFile(algorithmParameters.getOutputPattern(), algorithmParameters.getInstance().get(i).getName(), 0) + ".fun");
										    output.getParentFile().mkdirs();
										    output.createNewFile();

										    population.printObjectivesToFile(output.getAbsolutePath());

										  //Creating output file VAR
										    output = new File(ApplicationContext.getContext().getProject().getPath() + "\\Solutions\\" + algorithmParameters.getProblem() + "\\" + decodeOutputFile(algorithmParameters.getOutputPattern(), algorithmParameters.getInstance().get(i).getName(), 0) + ".var");
										    output.getParentFile().mkdirs();
										    output.createNewFile();

										    population.printVariablesToFile(output.getAbsolutePath());
										} catch (Exception e) {
											e.printStackTrace();
										}
			                		}

			                		//When finishs all the instances set progress to 1
			                		instancesProgress.set(1);
			                		instanceList.getFocusModel().focus(-1);;
					    	}

						    }.start();
                		}
                } catch (Exception e) {
					// TODO Auto-generated catch block
					dialogException(e);
					e.printStackTrace();
				}
            }
        });

    	cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	ApplicationContext.getContext().isRunningAlgorithm.set(false);
            	progressBar.progressProperty().unbind();
			    progressBar.setProgress(0);

			    instanceProgressBar.progressProperty().unbind();
			    instanceProgressBar.setProgress(0);
            }
        });

    	configurationButton.setOnAction(new EventHandler<ActionEvent>() {

    		@Override
    		public void handle(ActionEvent event){
    			BorderPane page = null;
            	try{
                	FXMLLoader loader = new FXMLLoader();
	                loader.setLocation(MainApp.class.getResource("view/CreateParameters.fxml"));
	                page = (BorderPane) loader.load();


                    // Cria o palco dialogStage.
                    Stage dialogStage = new Stage();
                    dialogStage.setTitle("Configuration");
                    dialogStage.initModality(Modality.WINDOW_MODAL);
                    dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
                    UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
                    dialogStage.setScene(scene);

                    dialogStage.sizeToScene();
                    dialogStage.toFront();

                    Undecorator undecorator = scene.getUndecorator();
                    dialogStage.setMinWidth(undecorator.getMinWidth());
                    dialogStage.setMinHeight(undecorator.getMinHeight());

                    // Define a pessoa no controller.
                    CreateParametersController controller = loader.getController();
                    controller.setDialogStage(dialogStage);
                    if(algorithmParameters != null){
                    	controller.setAlgorithmParameters(algorithmParameters);
                    }

                    // Mostra a janela e espera at� o usu�rio fechar.
                    dialogStage.showAndWait();

                    if(controller.isOkClicked()){
                    	algorithmParameters = controller.getAlgorithmParameters();

                    	instanceFileList.clear();
                    	instanceFileList.addAll(algorithmParameters.getInstance());

                    }

	            } catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

	  	     }

		});

    }

    public Problem compileAndLoad(String problemName, String instancePath) throws MalformedURLException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException{
    	// Prepare source somehow.
    	Problem problem = null;

    	// Save source in .java file.
    	File root = new File(ApplicationContext.getContext().getProject().getPath() + "\\Problems\\");
    	File sourceFile = new File(root, problemName + ".java");
    	//sourceFile.getParentFile().mkdirs();
    	//Files.write(sourceFile.toPath(), source, StandardCharsets.UTF_8);

    	// Compile source file.
    	JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
    	//System.out.println(sourceFile.getPath());
    	compiler.run(null, null, null, sourceFile.getPath());

    	URL url = root.toURL();          // file:/c:/myclasses/
        URL[] urls = new URL[]{url};

        ClassLoader cl = new URLClassLoader(urls);
        Class cls = cl.loadClass(problemName);
        System.out.println(cls.getName());
        Constructor constructor = cls.getConstructor(String.class);
        problem = (Problem) constructor.newInstance(instancePath);

        return problem;

    	// Load and instantiate compiled class.
    	/*URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] { root.toURI().toURL() });
    	Class<?> cls = Class.forName("Test", true, classLoader); // Should print "hello".
    	Object instance = cls.newInstance(); // Should print "world".
    	System.out.println(instance); // Should print "test.Test@hashcode".*/
    }

    public void dialogException(Exception ex){
    	Alert alert = new Alert(AlertType.ERROR);
    	alert.setTitle("Exception Dialog");
    	alert.setHeaderText("Look, an Exception Dialog");
    	alert.setContentText("Could not find file blabla.txt!");

    	// Create expandable Exception.
    	StringWriter sw = new StringWriter();
    	PrintWriter pw = new PrintWriter(sw);
    	ex.printStackTrace(pw);
    	String exceptionText = sw.toString();

    	Label label = new Label("The exception stacktrace was:");

    	TextArea textArea = new TextArea(exceptionText);
    	textArea.setEditable(false);
    	textArea.setWrapText(true);

    	textArea.setMaxWidth(Double.MAX_VALUE);
    	textArea.setMaxHeight(Double.MAX_VALUE);
    	GridPane.setVgrow(textArea, Priority.ALWAYS);
    	GridPane.setHgrow(textArea, Priority.ALWAYS);

    	GridPane expContent = new GridPane();
    	expContent.setMaxWidth(Double.MAX_VALUE);
    	expContent.add(label, 0, 0);
    	expContent.add(textArea, 0, 1);

    	// Set expandable Exception into the dialog pane.
    	alert.getDialogPane().setExpandableContent(expContent);

    	alert.showAndWait();
    }

}
