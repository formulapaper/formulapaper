package magius;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import jmetal.core.*;
import jmetal.encodings.solutionType.ArrayRealSolutionType;
import jmetal.encodings.solutionType.BinaryRealSolutionType;
import jmetal.encodings.solutionType.IntSolutionType;
import jmetal.encodings.solutionType.PermutationSolutionType;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.encodings.variable.Permutation;
import jmetal.util.JMException;
import jmetal.util.Configuration.*;
import jmetal.util.wrapper.XReal;

public class TSPFormula extends Problem {

int N;
int M1[][];
int M2[][];


public TSPFormula(String filename) throws FileNotFoundException{
Scanner scn  = new Scanner (new File (filename));
N = scn.nextInt();
M1 = new int[N][N];
for(int i=0;i<N; ++i){
for(int j=0;j<N; ++j){
M1[i][j] = scn.nextInt();
}
}
M2 = new int[N][N];
for(int i=0;i<N; ++i){
for(int j=0;j<N; ++j){
M2[i][j] = scn.nextInt();
}
}
numberOfVariables_ = 1;
numberOfObjectives_ = 2;
numberOfConstraints_ = 0;
problemName_ = "TSP";
upperLimit_ = new double[numberOfVariables_] ;
lowerLimit_ = new double[numberOfVariables_] ;
for (int i = 0; i < numberOfVariables_; i++) {
lowerLimit_[i] = 0.0;
upperLimit_[i] = 1.0;
}
try{
solutionType_ = new PermutationSolutionType(this);
length_       = new int[numberOfVariables_];
length_[0]      = N;
}
catch (Exception e){
e.printStackTrace();
}

}

@Override
public void evaluate(Solution solution) throws JMException {
double obj[] = new double[2];
Variable _variable[] = solution.getDecisionVariables();
Integer X[] = new Integer[N];
for(int ii = 0;ii < X.length;ii++)
X[ii] = ((Permutation)solution.getDecisionVariables()[0]).vector_[ii];
double L01 = 0.0;
for(int i=0;i<=N-2;i++){
double L02;
L02 = M1[X[i]][X[i+1]];
L01 += L02;
}
obj[0] = L01+M1[X[N-1]][X[0]];

solution.setObjective(0, obj[0]);
double L11 = 0.0;
for(int j=0;j<=N-2;j++){
double L12;
L12 = M2[X[j]][X[j+1]];
L11 += L12;
}
obj[1] = L11+M2[X[N-1]][X[0]];

solution.setObjective(1, obj[1]);
}
@Override
public void evaluateConstraints(Solution solution) throws JMException {
double cons1[] = new double[0];
double cons2[] = new double[0];
int penality[] = new int[0];
Variable _variable[] = solution.getDecisionVariables();
Integer X[] = new Integer[N];
for(int ii = 0;ii < X.length;ii++)
X[ii] = ((Permutation)solution.getDecisionVariables()[0]).vector_[ii];
solution.setNumberOfViolatedConstraint(0);
solution.setOverallConstraintViolation(0);
}
}
