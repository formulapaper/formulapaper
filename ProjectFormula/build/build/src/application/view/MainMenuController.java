package application.view;

import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import application.ApplicationContext;
import application.MainApp;
import application.model.ProblemModel;
import application.model.ProblemType;
import box.ConfirmationBox;
import box.InformationBox;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import magius.GeneralProblem;
import undecorator.insidefx.undecorator.Undecorator;
import undecorator.insidefx.undecorator.UndecoratorScene;

public class MainMenuController {

	@FXML
	private Label projectName;

	@FXML
	private Label projectPath;

	@FXML
	private Button createProblemButton;

	@FXML
	private Button importProblemButton;

	@FXML
	private Button runAlgorithmButton;

	@FXML
	private Button runExperimentsButton;

	@FXML
	private Button chartsButton;

	@FXML
	private Button resultsButton;

	@FXML
	private Button utilityButton;

	@FXML
	private Button exitButton;

	//Problem Table

	private ObservableList<ProblemModel> problemTableList;

	@FXML
	private TableView<ProblemModel> problemTable;

	@FXML
	private TableColumn columnName;

	@FXML
	private TableColumn columnEdit;

	@FXML
	private TableColumn columnDelete;


	public MainMenuController() {
		// TODO Auto-generated constructor stub
	}

	/**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {
    	initializeButtons();

    	projectName.setText(ApplicationContext.getContext().getProject().getName());
    	projectPath.setText(ApplicationContext.getContext().getProject().getPath().toString());

    	problemTableList = FXCollections.observableArrayList();

    	createTable();

    	updateList();
    }

    private void updateList(){
    	problemTableList.clear();
    	for(String s : ApplicationContext.getContext().getProject().getProblemNames()){
    		ProblemModel pm = new ProblemModel(s);

    		problemTableList.add(pm);
    	}
    }

    private void createTable(){
    	columnName.setCellValueFactory( new PropertyValueFactory<>( "name" ) );

    	columnEdit.setCellValueFactory( new PropertyValueFactory<>( "DUMMY" ) );

    	columnDelete.setCellValueFactory( new PropertyValueFactory<>( "DUMMY" ) );

    	Callback<TableColumn<ProblemModel, String>, TableCell<ProblemModel, String>> editCellFactory = //
                new Callback<TableColumn<ProblemModel, String>, TableCell<ProblemModel, String>>()
                {
                    @Override
                    public TableCell call( final TableColumn<ProblemModel, String> param )
                    {
                        final TableCell<ProblemModel, String> cell = new TableCell<ProblemModel, String>()
                        {

                            final Button btn = new Button( "Edit" );

                            @Override
                            public void updateItem( String item, boolean empty )
                            {
                                super.updateItem( item, empty );
                                if ( empty )
                                {
                                    setGraphic( null );
                                    setText( null );
                                }
                                else
                                {
                                    btn.setOnAction( ( ActionEvent event ) ->
                                            {
                                            	ProblemModel problemModel = getTableView().getItems().get( getIndex() );

                                            	for(ProblemType pt : ApplicationContext.getContext().getProject().getProblems()){
                            	            		if(pt.getName().equals(problemModel.getName()) && pt.getType().equals(ProblemType.TYPE_NATIVE)){
                            	            			GeneralProblem problem = ApplicationContext.getContext().getProject().loadProblem(pt.getName());

                                    	            	ApplicationContext.getContext().getMainApp().loadScene("CreateProblem");
                                                		CreateProblemController createProblemController = (CreateProblemController) ApplicationContext.getContext().getMainApp().getViewController();
                                                		createProblemController.setProblem(problem);
                                                		ApplicationContext.getContext().getMainApp().setScene();

                                    	            	break;
                            	            		}else if(pt.getName().equals(problemModel.getName()) && pt.getType().equals(ProblemType.TYPE_IMPORTED)){
                            	            			InformationBox informationBox = new InformationBox("Information", "You can't edit a imported problem.", 18);

                            	            			informationBox.show();
                            	            		}
                            	            	}
                                    } );
                                    setGraphic( btn );
                                    setText( null );
                                }
                            }
                        };
                        return cell;
                    }
                };

        columnEdit.setCellFactory( editCellFactory );

        Callback<TableColumn<ProblemModel, String>, TableCell<ProblemModel, String>> deleteCellFactory = //
                new Callback<TableColumn<ProblemModel, String>, TableCell<ProblemModel, String>>()
                {
                    @Override
                    public TableCell call( final TableColumn<ProblemModel, String> param )
                    {
                        final TableCell<ProblemModel, String> cell = new TableCell<ProblemModel, String>()
                        {

                            final Button btn = new Button( "Delete" );

                            @Override
                            public void updateItem( String item, boolean empty )
                            {
                                super.updateItem( item, empty );
                                if ( empty )
                                {
                                    setGraphic( null );
                                    setText( null );
                                }
                                else
                                {
                                    btn.setOnAction( ( ActionEvent event ) ->
                                            {
                                            	ProblemModel problemModel = getTableView().getItems().get( getIndex() );

                                            	ConfirmationBox confirmationBox = new ConfirmationBox("Confirmation", "Do you really want delete the problem " + problemModel.getName(), 16);

                                            	if(confirmationBox.show()){
	                                            	File deleteFile = new File(ApplicationContext.getContext().getProject().getPath().toString() + "\\Problems\\" + problemModel.getName() + ".java");
	                            	            	deleteFile.delete();

	                            	            	deleteFile = new File(ApplicationContext.getContext().getProject().getPath().toString() + "\\Problems\\" + problemModel.getName() + ".class");
	                            	            	deleteFile.delete();

	                            	            	for(int i=0;i<ApplicationContext.getContext().getProject().getProblems().size();i++){
	                            	            		if(ApplicationContext.getContext().getProject().getProblems().get(i).getName().equals(problemModel.getName())){
	                            	            			ApplicationContext.getContext().getProject().getProblems().remove(i);
	                            	            			//ApplicationContext.getContext().getProject().getProblemNames().remove(i);
	                            	            			break;
	                            	            		}
	                            	            	}
	                            	            	updateList();
	                            	            	ApplicationContext.getContext().getProject().save();
                                            	}
                                    } );
                                    setGraphic( btn );
                                    setText( null );
                                }
                            }
                        };
                        return cell;
                    }
                };

        columnDelete.setCellFactory( deleteCellFactory );

    	problemTable.setItems(problemTableList);
    }

    private void initializeButtons(){

    		importProblemButton.setOnAction(new EventHandler<ActionEvent>() {
	            @Override
	            public void handle(ActionEvent event) {
	            	FileChooser chooser = new FileChooser();
	            	chooser.setTitle("Import Problem");
	            	File defaultDirectory = new File("C:\\Users\\");
	            	chooser.setInitialDirectory(defaultDirectory);
	            	File selectedFile = chooser.showOpenDialog(ApplicationContext.getContext().getMainApp().getStage());

	            	if(selectedFile != null){
	            		try {
	            			File problemFolder = new File(ApplicationContext.getContext().getProject().getPath() + "\\Problems\\" + selectedFile.getName());

							problemFolder.createNewFile();

		            		CopyOption[] options = new CopyOption[]{
		            				  StandardCopyOption.REPLACE_EXISTING,
		            				  StandardCopyOption.COPY_ATTRIBUTES
		            				};

							Files.copy(selectedFile.toPath(), problemFolder.toPath(), options);

							//If everything runs fine, add the problem to Project.Problems and update problem list
							String problemName = selectedFile.getName();
							String extension = "";
							int pos = problemName.lastIndexOf(".");
							if (pos > 0) {
								extension = problemName.substring(pos+1, problemName.length());
								problemName = problemName.substring(0, pos);
							}

							//If this problem is not in the project
							if(!ApplicationContext.getContext().getProject().getProblems().contains(problemName) && extension.equals("java")){
								ApplicationContext.getContext().getProject().getProblems().add(new ProblemType(problemName, ProblemType.TYPE_IMPORTED));
								updateList();

								//Probably is better save the project now
								ApplicationContext.getContext().getProject().save();
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	            	}

	            }
	    	});

    		createProblemButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                	BorderPane page = null;
                	try{
	                	FXMLLoader loader = new FXMLLoader();
		                loader.setLocation(MainApp.class.getResource("view/NewProblemForm.fxml"));
		                page = (BorderPane) loader.load();


	                    // Cria o palco dialogStage.
	                    Stage dialogStage = new Stage();
	                    dialogStage.setTitle("Create New Problem");
	                    dialogStage.initModality(Modality.WINDOW_MODAL);
	                    dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
	                    UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
	                    dialogStage.setScene(scene);

	                    dialogStage.sizeToScene();
	                    dialogStage.toFront();

	                    Undecorator undecorator = scene.getUndecorator();
	                    dialogStage.setMinWidth(undecorator.getMinWidth());
	                    dialogStage.setMinHeight(undecorator.getMinHeight());

	                    // Define a pessoa no controller.
	                    NewProblemFormController controller = loader.getController();
	                    controller.setDialogStage(dialogStage);
	                    /*controller.setPerson(person);*/

	                    // Mostra a janela e espera at� o usu�rio fechar.
	                    dialogStage.showAndWait();

	                    if(controller.isOkClicked()){

	                    	if(!controller.getName().equals("")){
	                    		ApplicationContext.getContext().getProject().getProblems().add(new ProblemType(controller.getName(), ProblemType.TYPE_NATIVE));
	                    		updateList();

	                    		System.out.println(controller.getName() + " " +  controller.getVariableType() + " " + controller.getNumberOfObjectives() + " " +  controller.getNumberOfConstraints());

	                    		GeneralProblem tmpProblem = new GeneralProblem(controller.getName(), controller.getVariableType(), 1, controller.getNumberOfObjectives(), controller.getNumberOfConstraints());

	                    		//Probably is better save the project now
	                    		ApplicationContext.getContext().getProject().save();
	                    		ApplicationContext.getContext().getProject().saveProblem(tmpProblem);

	                    		ApplicationContext.getContext().getMainApp().loadScene("CreateProblem");
	                    		CreateProblemController createProblemController = (CreateProblemController) ApplicationContext.getContext().getMainApp().getViewController();
	                    		createProblemController.setProblem(tmpProblem);
	                    		ApplicationContext.getContext().getMainApp().setScene();

	                    	}
	                    }

		            } catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

                }
    		});

    		runAlgorithmButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                	ApplicationContext.getContext().getMainApp().loadScene("RunAlgorithm");
                	ApplicationContext.getContext().getMainApp().setScene();
                }
    		});

    		runExperimentsButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                	ApplicationContext.getContext().getMainApp().loadScene("RunExperiments");
                	ApplicationContext.getContext().getMainApp().setScene();
                }
    		});

    		chartsButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                	//InformationBox informationBox = new InformationBox("Not avaliable", "This content is not yet avaliable.", 18);
                	//informationBox.show();

                	//Block this button for now
                	ApplicationContext.getContext().getMainApp().loadScene("Charts");
                	ApplicationContext.getContext().getMainApp().setScene();
                }
    		});

    		resultsButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                	InformationBox informationBox = new InformationBox("Not avaliable", "This content is not yet avaliable.", 18);

                	informationBox.show();
                }
    		});

    		utilityButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                	ApplicationContext.getContext().getMainApp().loadScene("Utility");
                	ApplicationContext.getContext().getMainApp().setScene();
                }
    		});

    		exitButton.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                    	ApplicationContext.getContext().getMainApp().loadScene("Menu");
                    	ApplicationContext.getContext().getMainApp().setScene();

                    }
        	});

    }

}


