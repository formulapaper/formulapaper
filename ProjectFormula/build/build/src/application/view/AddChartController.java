package application.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import com.sun.deploy.uitoolkit.impl.fx.ui.FXConsole;

import application.ApplicationContext;
import application.model.AlgorithmConfiguration;
import application.model.AlgorithmFactory;
import application.model.ChartPopulation;
import application.model.ProblemType;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.DoubleSpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.util.JMException;
import magius.GeneralProblem;
import undecorator.insidefx.undecorator.UndecoratorScene;

public class AddChartController {

	@FXML
	private Button cancelButton;

	@FXML
	private Button okButton;

	@FXML
	private Button addFilesButton;

	@FXML
	private TextField name;

	@FXML
	private TextField title;

	@FXML
	private TextField labelX;

	@FXML
	private TextField labelY;

	@FXML
	private ChoiceBox xObjective;

	@FXML
	private ChoiceBox yObjective;

	boolean okButtonClicked;

	private Stage dialogStage;

	@FXML
	private ListView<File> fileView;

	ObservableList<File> chartFileList;

	private ChartPopulation chart;



	/**
     * The constructor is called before initialize()
     */
    public AddChartController() {
    	okButtonClicked = false;

    	chart = new ChartPopulation();

    	chartFileList = FXCollections.observableArrayList();
	}

    public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

    /**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {
    	okButtonController();
    	cancelButtonController();

    	fileView.setItems(chartFileList);

    	fileView.setCellFactory(new Callback<ListView<File>, ListCell<File>>() {
    	     @Override public ListCell<File> call(ListView<File> list) {
    	         return new ListCellHighlight();
    	     }
    	 });

    	addFilesButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            	FileChooser chooser = new FileChooser();
            	chooser.setTitle("Load Instances");
            	File defaultDirectory = new File(ApplicationContext.getContext().getProject().getPath().toString());
            	chooser.setInitialDirectory(defaultDirectory);

            	 List<File> list =
            			 chooser.showOpenMultipleDialog(ApplicationContext.getContext().getMainApp().getStage());
                 if (list != null) {
                	 chartFileList.clear();
                	 chartFileList.addAll(list);
                 }
            }
        });

    	List<String> xObjectivelist = new ArrayList<String>();
    	List<String> yObjectivelist = new ArrayList<String>();
    	for(int i=1;i<=10;i++){
    		xObjectivelist.add("" + i);
    		yObjectivelist.add("" + i);
    	}
    	xObjective.getItems().addAll(xObjectivelist);
    	if(xObjectivelist.size() > 0)
    		xObjective.setValue(xObjectivelist.get(0));

    	yObjective.getItems().addAll(yObjectivelist);
    	if(yObjectivelist.size() > 0)
    		yObjective.setValue(yObjectivelist.get(1));


    }

    public boolean isOkClicked(){
    	return okButtonClicked;
    }

    private void okButtonController(){
    	okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            	//Validate
            	if(name.getText().equals("")){
            		name.setStyle("-fx-background-color: #FF5B5E;");
            	}

            	if(title.getText().equals("")){
            		title.setStyle("-fx-background-color: #FF5B5E;");
            	}

            	if(labelX.getText().equals("")){
            		labelX.setStyle("-fx-background-color: #FF5B5E;");
            	}

            	if(labelY.getText().equals("")){
            		labelY.setStyle("-fx-background-color: #FF5B5E;");
            	}

            	if(!name.getText().equals("") && !title.getText().equals("") && !labelX.getText().equals("") && !labelY.getText().equals("")){

            		okButtonClicked = true;

            		chart.setName(name.getText());
            		chart.setTitle(title.getText());
            		chart.setxAxisName(labelX.getText());
            		chart.setyAxisName(labelY.getText());

            		chart.setFile(chartFileList);

            		chart.setxAxis(xObjective.getSelectionModel().getSelectedIndex());
            		chart.setyAxis(yObjective.getSelectionModel().getSelectedIndex());

            		dialogStage.close();

            	}


            }
    	});
    }

    private void cancelButtonController(){
    	cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	dialogStage.close();
            }
    	});
    }

	public ChartPopulation getChart() {
		return chart;
	}

	public void setChart(ChartPopulation chart) {
		this.chart = chart;

		name.setText(chart.getName());
		title.setText(chart.getTitle());
		labelX.setText(chart.getxAxisName());
		labelY.setText(chart.getyAxisName());

		chartFileList.clear();
		chartFileList.addAll(chart.getFile());
	}


}
