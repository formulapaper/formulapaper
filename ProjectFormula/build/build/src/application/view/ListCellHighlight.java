package application.view;

import java.io.File;

import application.ApplicationContext;
import javafx.scene.control.ListCell;
import javafx.scene.paint.Color;

public class ListCellHighlight extends ListCell<File> {

     public ListCellHighlight() {
     }

     @Override protected void updateItem(File item, boolean empty) {
         // calling super here is very important - don't skip this!
         super.updateItem(item, empty);

         if (item != null) {
            setText(item.getName());
         } else {
            setText("");   // <== clear the now empty cell.
         }
     }
 }
