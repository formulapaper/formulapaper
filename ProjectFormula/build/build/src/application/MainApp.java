package application;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import application.model.GeneralSettings;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import undecorator.insidefx.undecorator.Undecorator;
import undecorator.insidefx.undecorator.UndecoratorScene;

public class MainApp extends Application {

    private Stage stage;
    private BorderPane rootLayout;
    UndecoratorScene scene;
    FXMLLoader loader;

    private GeneralSettings generalSettings;

    public GeneralSettings getGeneralSettings() {
		return generalSettings;
	}

	@Override
    public void start(Stage primaryStage) {
        this.stage = primaryStage;
        this.stage.setTitle("Formula");

        ApplicationContext.getContext().setMainApp(this);

        generalSettings = new GeneralSettings();
        generalSettings.load();

        initRootLayout();

    }

    public Stage getStage(){
    	return stage;
    }

    /**
     * Inicializa o root layout (layout base).
     */
    public void initRootLayout() {
        try {
            // Carrega o root layout do arquivo fxml.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/Login.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Mostra a scene (cena) contendo oroot layout.
            UndecoratorScene scene = new UndecoratorScene(stage, rootLayout);
            // Overrides defaults

            //Scene scene = new Scene(rootLayout);
            stage.setScene(scene);

            stage.sizeToScene();
            stage.toFront();

            Undecorator undecorator = scene.getUndecorator();
            stage.setMinWidth(undecorator.getMinWidth());
            stage.setMinHeight(undecorator.getMinHeight());

            scene.getStylesheets().clear();

            scene.getStylesheets().add(generalSettings.getSelectedStylesheet());

            System.out.println("Style: " + generalSettings.getSelectedStyle());

            rootLayout.getStylesheets().clear();

            rootLayout.getStylesheets().add(generalSettings.getSelectedStylesheet());

            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadScene(String sceneName){
    	// Carrega o root layout do arquivo fxml.
        loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/" + sceneName  + ".fxml"));
        try {
			rootLayout = (BorderPane) loader.load();

            rootLayout.getStylesheets().clear();

            rootLayout.getStylesheets().add(generalSettings.getSelectedStylesheet());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public Object getViewController(){
    	return loader.getController();
    }

    public void setScene(){
    	// Mostra a scene (cena) contendo oroot layout.
        scene = new UndecoratorScene(stage, rootLayout);

        scene.getStylesheets().clear();

        scene.getStylesheets().add(generalSettings.getSelectedStylesheet());

    	stage.setScene(scene);
    }


    public static void main(String[] args) {
        launch(args);
    }
}