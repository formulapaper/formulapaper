package application;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Map;

import application.model.AlgorithmFactory;
import application.model.AlgorithmParameters;

import application.model.Project;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextArea;
import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;

public class ApplicationContext {

	private static ApplicationContext context = new ApplicationContext(); //class variable!

	private MainApp mainApp;

	private Project project;

	//Problem and Algorithm
	public Problem   problem   ; // The problem to solve
    public Algorithm algorithm ; // The algorithm to use
    public Operator  crossover ; // Crossover operator
    public Operator  mutation  ; // Mutation operator
    public Operator  selection ; // Selection operator

    //Algorithm Control
    public DoubleProperty algorithmProgress;
	public BooleanProperty isRunningAlgorithm;

	public ApplicationContext() {

		project = new Project();
		project.setName("");

		algorithmProgress = new SimpleDoubleProperty(0);
		isRunningAlgorithm = new SimpleBooleanProperty(false);

		// TODO Auto-generated constructor stub
	}

	public static ApplicationContext getContext(){
		return context;
	}

	public MainApp getMainApp(){
		return mainApp;
	}

	public void setMainApp(MainApp _mainApp){
		mainApp = _mainApp;
	}

	public Project getProject(){
		return project;
	}

	public void algorithmInitialize(Problem problem, AlgorithmParameters ap){
		algorithm = AlgorithmFactory.createAlgorithm(ap.getAlgorithm(), problem);

		for (Map.Entry<String, Object> entry : ap.getMap().entrySet()) {
		    String key = entry.getKey();
		    Object value = entry.getValue();

		    algorithm.setInputParameter(key, value);
		}

		algorithm.addOperator("crossover",ap.getCrossover());
	    algorithm.addOperator("mutation",ap.getMutation());
	    algorithm.addOperator("selection",ap.getSelection());


	}

	public OutputStream initializeConsole(TextArea debugConsole){
		double debugConsoleHeight = debugConsole.getPrefHeight();
    	double stageHeight = 640;

    	debugConsole.setPrefHeight(debugConsoleHeight + (ApplicationContext.getContext().getMainApp().getStage().getHeight() - stageHeight));

    	//Debug console
    	mainApp.getStage().heightProperty().addListener(new ChangeListener<Number>() {
	    @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight) {
	        //System.out.println("Height: " + newSceneHeight + " " + (debugConsoleHeight + (Double.valueOf("" + newSceneHeight) - stageHeight)));
	        debugConsole.setPrefHeight(debugConsoleHeight + (Double.valueOf("" + newSceneHeight) - stageHeight));
	    }
    	});
    	OutputStream out = new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                //appendText(String.valueOf((char) b));
            	//debugConsole.appendText("" + ((char) b));

        	 Platform.runLater(new Runnable() {
        	        public void run() {
        	        	debugConsole.appendText("" + ((char) b));
        	        }
        	    });
            }
        };
    	PrintStream ps = new PrintStream(out, true);
        System.setOut(ps);
        System.setErr(ps);

        return out;
	}

}
