package application.model;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import application.ApplicationContext;
import application.MainApp;
import magius.GeneralProblem;

@XmlRootElement
public class Project {

	private List<ProblemType> problem;
	private List<ChartPopulation> chart;
	private List<AlgorithmParameters> parameter;

	String name;
	File path;

	public Project() {
		problem = new ArrayList<ProblemType>();
		chart = new ArrayList<ChartPopulation>();
		parameter = new ArrayList<AlgorithmParameters>();


	}



	public String getName(){
		return name;
	}

	@XmlElement
	public void setPath(File _path){
		path = _path;
	}


	public File getPath(){
		return path;
	}

	public List<ProblemType> getProblems() {
        return problem;
    }

	public List<String> getProblemNames() {
		List<String> result;
		result = new ArrayList<String>();

		for(ProblemType pt : problem){
			result.add(pt.getName());
		}

        return result;
    }

	@XmlElement
	public void setProblems(List<ProblemType> problems) {
        this.problem = problem;
    }

	@XmlElement
	public void setName(String newName){
		name = newName;
	}

	private void copy(Project other){
		name = other.getName();
		problem.clear();
		problem.addAll(other.getProblems());
		chart.addAll(other.getChart());

		for(AlgorithmParameters ap : other.getParameter()){
			parameter.add(new AlgorithmParameters(ap));
		}


		//System.out.println(parameter.get(0).getMap());
		//System.out.println(parameter.get(0).getCrossover());
	}

	/**
	 * Define o caminho do arquivo do arquivo carregado atual. O caminho � persistido no
	 * registro espec�fico do SO (Sistema Operacional).
	 *
	 * @param file O arquivo ou null para remover o caminho
	 */
	public void setPreferenceFilePath(File file) {
	    Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
	    if (file != null) {
	        prefs.put("filePath", file.getPath());
	    } else {
	        prefs.remove("filePath");
	    }
	}

	/**
	 * Salva os dados da pessoa atual no arquivo especificado.
	 *
	 * @param file
	 */
	public void save(){

			try {
		        JAXBContext context = JAXBContext
		                .newInstance(Project.class);
		        Marshaller m;

				m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		        // Enpacotando e salvando XML  no arquivo.
		        m.marshal(this, new File(path + "\\project.gao"));

		        setPreferenceFilePath(path);
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	public void saveProblem(GeneralProblem problem){
		try {
			/*JAXBContext context = JAXBContext
	                .newInstance(GeneralProblem.class);
	        Marshaller m;

			m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

	        // Enpacotando e salvando XML  no arquivo.
	        m.marshal(problem, new File(path + "\\Problems\\" + problem.getName() + ".pro"));

	        setPreferenceFilePath(path);*/

	        FileOutputStream fout = new FileOutputStream(path + "\\Problems\\" + problem.getName() + ".pro");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(problem);
			oos.close();
			fout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public GeneralProblem loadProblem(String problemName) {
		GeneralProblem loadedProblem = null;
		try {
			InputStream file = new FileInputStream(path + "\\Problems\\" + problemName + ".pro");
		    InputStream buffer = new BufferedInputStream(file);
			ObjectInput input = new ObjectInputStream (buffer);

			loadedProblem = (GeneralProblem) input.readObject();

			input.close();
			buffer.close();
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return loadedProblem;
	}

	/**
	 * Carrega os dados da pessoa do arquivo especificado. A pessoa atual
	 * ser� substitu�da.
	 *
	 * @param file
	 */
	public boolean load(File file) {
	    try {
	        JAXBContext context = JAXBContext
	                .newInstance(Project.class);
	        Unmarshaller um = context.createUnmarshaller();

	        // Reading XML from the file and unmarshalling.
	        Project wrapper = (Project) um.unmarshal(file);

	        this.copy(wrapper);

	        setPreferenceFilePath(path);

	        return true;
	    } catch (Exception e) { // catches ANY exception
	    	e.printStackTrace();
	    	return false;
	    }
	}

	public List<ChartPopulation> getChart() {
		return chart;
	}

	@XmlElement
	public void setChart(List<ChartPopulation> chart) {
		this.chart = chart;
	}

	public List<AlgorithmParameters> getParameter() {
		return parameter;
	}

	@XmlElement
	public void setParameter(List<AlgorithmParameters> parameter) {
		this.parameter = parameter;
	}



}

