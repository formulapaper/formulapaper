package application.view;

import java.awt.Desktop;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

//import com.sun.beans.decoder.ValueObject;
//import com.sun.deploy.uitoolkit.impl.fx.ui.FXConsole;

import application.ApplicationContext;
import application.MainApp;
import application.model.AlgorithmConfiguration;
import application.model.AlgorithmFactory;
import application.model.AlgorithmParameters;
import application.model.ProblemModel;
import application.model.ProblemType;
import box.ConfirmationBox;
import box.InformationBox;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.SpinnerValueFactory.DoubleSpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import magius.GeneralProblem;
import undecorator.insidefx.undecorator.Undecorator;
import undecorator.insidefx.undecorator.UndecoratorScene;

public class RunExperimentsController {

	@FXML
	private ImageView backButton;

	@FXML
	private ProgressBar progressBar;

	@FXML
	private ProgressBar instanceProgressBar;

	public DoubleProperty instancesProgress;

	@FXML
	private ProgressBar configurationProgressBar;
	public DoubleProperty configurationProgress;

	@FXML
	private ProgressBar runningProgressBar;
	public DoubleProperty runningProgress;

	@FXML
	private ListView<AlgorithmParameters> parametersList;
	ObservableList<AlgorithmParameters> algorithmParameters;

	@FXML
	private Button runButton;

	@FXML
	private Button cancelButton;

	@FXML
	private Button newConfigurationButton;

	@FXML
	private Label configurationRunning;
	public StringProperty configurationRunningNow;

	@FXML
	private TableView<AlgorithmParameters> configurationTable;
	private ObservableList<AlgorithmParameters> configurationList;

	@FXML
	private TableColumn columnName;

	@FXML
	private TableColumn columnProblem;

	@FXML
	private TableColumn columnAlgorithm;

	@FXML
	private TableColumn columnRuns;

	@FXML
	private TextArea debugConsole;
	private OutputStream outConsole;

	/**
     * The constructor is called before initialize()
     */
    public RunExperimentsController() {
    	algorithmParameters = FXCollections.observableArrayList();
    	configurationList = FXCollections.observableArrayList();
	}

    /**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {

    	initButtons();

    	initTable();

    	instancesProgress = new SimpleDoubleProperty(0);

    	runningProgress = new SimpleDoubleProperty(0);

    	configurationProgress = new SimpleDoubleProperty(0);

    	configurationRunningNow = new SimpleStringProperty("Input the configurations");

    	configurationRunning.textProperty().bind(configurationRunningNow);

    	parametersList.setItems(algorithmParameters);

    	if(ApplicationContext.getContext().getProject().getParameter() == null){
    		ApplicationContext.getContext().getProject().setParameter(new ArrayList<AlgorithmParameters>());
    	}else{
    		algorithmParameters.clear();
    		algorithmParameters.addAll(ApplicationContext.getContext().getProject().getParameter());

    		//System.out.println(ApplicationContext.getContext().getProject().getParameter());
    	}


    	//BackButton Action
    	backButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
    	     @Override
    	     public void handle(MouseEvent event) {
    	    	 ApplicationContext.getContext().getMainApp().loadScene("MainMenu");
    	    	 ApplicationContext.getContext().getMainApp().setScene();

    	     }
    	});

    	//Tooltip
    	progressBar.setTooltip(
    			new Tooltip("Algorithm progress bar")
    	);

    	runningProgressBar.setTooltip(
    			new Tooltip("Running progress bar")
    	);

    	instanceProgressBar.setTooltip(
    			new Tooltip("Instance progress bar")
    	);

    	configurationProgressBar.setTooltip(
    			new Tooltip("Configuration progress bar")
    	);

    	configurationTable.setRowFactory(new Callback<TableView<AlgorithmParameters>, TableRow<AlgorithmParameters>>() {
            @Override
            public TableRow<AlgorithmParameters> call(TableView<AlgorithmParameters> tableView) {
                final TableRow<AlgorithmParameters> row = new TableRow<AlgorithmParameters>();
                final ContextMenu contextMenu = new ContextMenu();
                final MenuItem removeMenuItem = new MenuItem("Remove");
                removeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                    	configurationTable.getItems().remove(row.getItem());
                    }
                });

                final MenuItem goToFolder = new MenuItem("To folder");
                goToFolder.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                    	System.out.println("Teste");
                    	try {
							Desktop.getDesktop().open(new File(ApplicationContext.getContext().getProject().getPath() + "\\Solutions\\" + row.getItem().getProblem()));
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
                    }
                });
                contextMenu.getItems().addAll(removeMenuItem, goToFolder);
               // Set context menu on row, but use a binding to make it only show for non-empty rows:
                row.contextMenuProperty().bind(
                        Bindings.when(row.emptyProperty())
                        .then((ContextMenu)null)
                        .otherwise(contextMenu)
                );
                return row ;
            }
        });


    	parametersList.setCellFactory(lv -> {

            ListCell<AlgorithmParameters> cell = new ListCell<AlgorithmParameters>(){
            	@Override
                protected void updateItem(AlgorithmParameters item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setText(item.getName());
                    } else {
                        setText("");
                    }
                }

            };

            ContextMenu contextMenu = new ContextMenu();

            contextMenu.setStyle("-fx-font-size: 14.0px; -fx-text-base-color: #2196F3; -fx-background-radius: 2.0; -fx-background-color: rgb(255.0, 255.0, 255.0, 0.95);");

            MenuItem addItem = new MenuItem("Add");
            addItem.setOnAction(event -> {
                AlgorithmParameters item = cell.getItem();

                //System.out.println("Adding " + item);

                //configurationList.add(new AlgorithmParameters(item));
                configurationList.add(item);

            });

            MenuItem cloneItem = new MenuItem("Clone");
            cloneItem.setOnAction(event -> {
                AlgorithmParameters item = cell.getItem();

                algorithmParameters.add(new AlgorithmParameters(item));

                //System.out.println(algorithmParameters.get(algorithmParameters.size()-1));
                //System.out.println("Adding " + item);

                //configurationList.add(new AlgorithmParameters(item));
                //configurationList.add(item);

            });

            MenuItem editItem = new MenuItem("Edit");
            editItem.setOnAction(event -> {
                AlgorithmParameters item = cell.getItem();

                BorderPane page = null;
            	try{
                	FXMLLoader loader = new FXMLLoader();
	                loader.setLocation(MainApp.class.getResource("view/CreateParameters.fxml"));
	                page = (BorderPane) loader.load();


                    // Cria o palco dialogStage.
                    Stage dialogStage = new Stage();
                    dialogStage.setTitle("Configuration");
                    dialogStage.initModality(Modality.WINDOW_MODAL);
                    dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
                    UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
                    dialogStage.setScene(scene);

                    dialogStage.sizeToScene();
                    dialogStage.toFront();

                    Undecorator undecorator = scene.getUndecorator();
                    dialogStage.setMinWidth(undecorator.getMinWidth());
                    dialogStage.setMinHeight(undecorator.getMinHeight());

                    // Define a pessoa no controller.
                    CreateParametersController controller = loader.getController();
                    controller.setDialogStage(dialogStage);
                    controller.setAlgorithmParameters(item);

                    // Mostra a janela e espera at� o usu�rio fechar.
                    dialogStage.showAndWait();

                    if(controller.isOkClicked()){
                    	item = controller.getAlgorithmParameters();
                    	updateParameterList();
                    }

	            } catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

                //System.out.println("Editing " + item);
                // code to edit item...
            });

            MenuItem deleteItem = new MenuItem("Delete");
            deleteItem.setOnAction(event -> {
            	ConfirmationBox confirmationBox = new ConfirmationBox("Delete", "Do you really want delete this configuration?", 18);

            	if(confirmationBox.show()){
            		parametersList.getItems().remove(cell.getItem());
            		updateParameterList();
            	}

            });


            contextMenu.getItems().addAll(addItem, cloneItem, editItem, deleteItem);

            //Correct name

            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                    cell.textProperty().set("");
                } else {
                    cell.setContextMenu(contextMenu);
                    cell.textProperty().set(cell.getItem().getName());
                    //System.out.println(cell.getItem().getName());
                }
            });


            return cell ;
        });


    	/*System.out.println("-------------------");
    	for(AlgorithmParameters ap : parametersList.getItems()){
    		System.out.println(ap.getName());
    	}*/

    	ApplicationContext.getContext().initializeConsole(debugConsole);

    }

    private void initTable(){
    	columnName.setCellValueFactory( new PropertyValueFactory<>( "name" ) );
    	columnProblem.setCellValueFactory( new PropertyValueFactory<>( "problem" ) );
    	columnAlgorithm.setCellValueFactory( new PropertyValueFactory<>( "algorithm" ) );
    	columnRuns.setCellValueFactory( new PropertyValueFactory<>( "quantityRuns" ) );

    	configurationTable.setItems(configurationList);
    }


    String decodeOutputFile(String file, String instance, int run){

    	//Remove extension
    	file = file.replaceAll("%i", instance.replaceFirst("[.][^.]+$", ""));

    	file = file.replaceAll("%r", String.valueOf(run).replaceFirst("[.][^.]+$", ""));

    	return file;
    }

    public void initButtons(){
    	runButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            	 try {

                 	if(configurationList.size() == 0){
                 		InformationBox ib = new InformationBox("Error", "Add a valid configuration before run the algorithm.", 18);
                 		ib.show();
                 	}else{

                 		//Thread thread[] = new Thread[configurationList.size()];
	              			configurationRunningNow.setValue("Running");
                 			//thread[inst] = new Thread(){
                 			new Thread(){
     					    	@Override
     					    	public void run(){

     					    		ApplicationContext.getContext().isRunningAlgorithm.set(true);

     					    		configurationProgressBar.progressProperty().bind(configurationProgress);

     					    		List<Long> timeList = new ArrayList<Long>();

     					    		File timeMetric = new File("");

     					    		for(int inst = 0; inst < configurationList.size(); ++inst){
     		                 			AlgorithmParameters ap = configurationList.get(inst);

     		                 			System.out.println("Running configuration " + ap.getName());
     		                 			System.out.println("Running algorithm " + ap.getAlgorithm());

     					    			Problem problem = null;

     					    			if(ApplicationContext.getContext().isRunningAlgorithm.getValue() == false)
 			                				break;

     							    	instanceProgressBar.progressProperty().bind(instancesProgress);
     							    	runningProgressBar.progressProperty().bind(runningProgress);

     							    	configurationProgress.setValue((double) inst / configurationList.size());


     			                		for(int i=0;i<ap.getInstance().size();i++){

     			                			System.out.println("Instance: " + ap.getInstance().get(i).getName());

     			                			//Clear metric files
 										   	//Creating a time metric file
 										    timeMetric = new File(ApplicationContext.getContext().getProject().getPath() + "\\Solutions\\" + ap.getProblem() + "\\" + configurationList.get(inst).getName()  + "\\" + ap.getInstance().get(i).getName() + ".time");
 										    timeMetric.getParentFile().mkdirs();
 										    try {
												timeMetric.createNewFile();

												metricToFile("", timeMetric, false);
											} catch (IOException e1) {
												// TODO Auto-generated catch block
												e1.printStackTrace();
											}

     			                			runningProgress.set(0);

     			                			if(ApplicationContext.getContext().isRunningAlgorithm.getValue() == false)
     			                				break;

     			                			//Calcule Mean and Standart Deviation
     			                			double mean = 0.0;
     			                			double SD = 0.0;
     			                			timeList.clear();


     			                			for(int r = 0; r < ap.getQuantityRuns();++r){

     			                				System.out.println("Run: " + r);

	     			                			if(ApplicationContext.getContext().isRunningAlgorithm.getValue() == false){
	     			                				instanceProgressBar.progressProperty().unbind();
	     			                				instanceProgressBar.setProgress(0);

	     			                				runningProgressBar.progressProperty().unbind();
	     			                				runningProgressBar.setProgress(0);
	     			                				break;
	     			                			}else{
	     			                				runningProgress.set((double) r / ap.getQuantityRuns());
	     			                				instancesProgress.set((double) i /ap.getInstance().size());
	     			                			}
	     			                			try {

	     			                				//Looking if the problem is in the project. Why?
	     			                				//TODO: Need review this.
	     			                				for(ProblemType pt : ApplicationContext.getContext().getProject().getProblems()){
	     			                					//Imported problem
	     			                					if(pt.getName().equals(ap.getProblem()) /*&& pt.getType().equals(ProblemType.TYPE_IMPORTED)*/){
	     			                						problem = compileAndLoad(ap.getProblem(), ap.getInstance().get(i).getPath());
	     			                						break;
	     			                					}
	     			                				}

	     										    //ProgressBar
	     										    progressBar.progressProperty().unbind();
	     										    progressBar.setProgress(0);
	     										    progressBar.progressProperty().bind(ApplicationContext.getContext().algorithmProgress);

	     										    //Add the operators to the algorithm
	     										    ApplicationContext.getContext().algorithmInitialize(problem, ap);

	     										   long initTime = System.currentTimeMillis();
	     										   SolutionSet population = ApplicationContext.getContext().algorithm.execute();
	     										   long estimatedTime = System.currentTimeMillis() - initTime;

	     										   mean += estimatedTime;
	     										   timeList.add(estimatedTime);

		     										timeMetric = new File(ApplicationContext.getContext().getProject().getPath() + "\\Solutions\\" + ap.getProblem() + "\\" + configurationList.get(inst).getName()  + "\\" + ap.getInstance().get(i).getName() + ".time");
		   										    timeMetric.getParentFile().mkdirs();
		   										    try {
		  												timeMetric.createNewFile();

		  												metricToFile(String.valueOf(estimatedTime), timeMetric, true);
		  											} catch (IOException e1) {
		  												// TODO Auto-generated catch block
		  												e1.printStackTrace();
		  											}


	     										    //Creating output file FUN
	     										    File output = new File(ApplicationContext.getContext().getProject().getPath() + "\\Solutions\\" + ap.getProblem() + "\\" + decodeOutputFile(ap.getOutputPattern(), ap.getInstance().get(i).getName(), r) + ".fun");
	     										    output.getParentFile().mkdirs();
	     										    output.createNewFile();

	     										    population.printObjectivesToFile(output.getAbsolutePath());

	     										    //Creating output file VAR
	     										    output = new File(ApplicationContext.getContext().getProject().getPath() + "\\Solutions\\" + ap.getProblem() + "\\" + decodeOutputFile(ap.getOutputPattern(), ap.getInstance().get(i).getName(), r) + ".var");
	     										    output.getParentFile().mkdirs();
	     										    output.createNewFile();

	     										    population.printVariablesToFile(output.getAbsolutePath());
	     										} catch (Exception e) {
	     											e.printStackTrace();
	     										}
	     			                		}

     			                			runningProgress.set(1);

     			                			//Setting the Mean and SD
     			                			mean /= ap.getQuantityRuns();
     			                			for(int j=0;j<timeList.size();++j){
     			                				SD += Math.pow(timeList.get(j) - mean, 2.0);
     			                			}
     			                			SD = Math.sqrt(SD);

     			                			timeMetric = new File(ApplicationContext.getContext().getProject().getPath() + "\\Solutions\\" + ap.getProblem() + "\\" + configurationList.get(inst).getName()  + "\\" + ap.getInstance().get(i).getName() + ".time");
   										    timeMetric.getParentFile().mkdirs();
   										    try {
  												timeMetric.createNewFile();

  												metricToFile(String.valueOf("Mean: " + new DecimalFormat("#0.00000").format(mean)), timeMetric, true);
  												metricToFile(String.valueOf("SD: " + new DecimalFormat("#0.00000").format(SD)), timeMetric, true);
  											} catch (IOException e1) {
  												// TODO Auto-generated catch block
  												e1.printStackTrace();
  											}
     			                		}

     			                		//When finishs all the instances set progress to 1
     			                		instancesProgress.set(1);
     					    		}

     					    		configurationProgress.setValue(1);

     					    		Platform.runLater(new Runnable() {
     					               @Override public void run() {
     					            	  configurationRunningNow.setValue("Finished");
     					               }
     					           });


     					    	}

     						    }.start();


     						    //thread[inst].setName(configurationList.get(inst).getName());



                 		 /*new Thread(){
					    	@Override
					    	public void run(){
					    		for(int inst = 0; inst < configurationList.size(); ++inst){
		                 			try {

		                 				//configurationRunning.textProperty().unbind();
		                 				//configurationRunning.textProperty().bind(configurationRunningNow);
		                 				configurationRunningNow.setValue(thread[inst].getName());

			                 			thread[inst].start();
										thread[inst].join();
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
					    		}
					    	}

					    }.start();*/

                 	}
                 } catch (Exception e) {
 					// TODO Auto-generated catch block
 					dialogException(e);
 					e.printStackTrace();
 				}

            }
        });

    	cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	ApplicationContext.getContext().isRunningAlgorithm.set(false);
            	progressBar.progressProperty().unbind();
			    progressBar.setProgress(0);

			    instanceProgressBar.progressProperty().unbind();
			    instanceProgressBar.setProgress(0);

			    runningProgressBar.progressProperty().unbind();
			    runningProgressBar.setProgress(0);

			    configurationRunningNow.setValue("Cancelled");
            }
        });

    	newConfigurationButton.setOnAction(new EventHandler<ActionEvent>() {

    		@Override
    		public void handle(ActionEvent event){
    			BorderPane page = null;
            	try{
                	FXMLLoader loader = new FXMLLoader();
	                loader.setLocation(MainApp.class.getResource("view/CreateParameters.fxml"));
	                page = (BorderPane) loader.load();


                    // Cria o palco dialogStage.
                    Stage dialogStage = new Stage();
                    dialogStage.setTitle("Configuration");
                    dialogStage.initModality(Modality.WINDOW_MODAL);
                    dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
                    UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
                    dialogStage.setScene(scene);

                    dialogStage.sizeToScene();
                    dialogStage.toFront();

                    Undecorator undecorator = scene.getUndecorator();
                    dialogStage.setMinWidth(undecorator.getMinWidth());
                    dialogStage.setMinHeight(undecorator.getMinHeight());

                    // Define a pessoa no controller.
                    CreateParametersController controller = loader.getController();
                    controller.setDialogStage(dialogStage);

                    // Mostra a janela e espera at� o usu�rio fechar.
                    dialogStage.showAndWait();

                    if(controller.isOkClicked()){
                    	algorithmParameters.add(controller.getAlgorithmParameters());
                    	updateParameterList();
                    }

	            } catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

	  	     }

		});

    }

    void updateParameterList(){
    	ApplicationContext.getContext().getProject().getParameter().clear();
    	ApplicationContext.getContext().getProject().getParameter().addAll(algorithmParameters);
    	ApplicationContext.getContext().getProject().save();
    }

    public Problem compileAndLoad(String problemName, String instancePath) throws MalformedURLException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException{
    	// Prepare source somehow.
    	Problem problem = null;

    	// Save source in .java file.
    	File root = new File(ApplicationContext.getContext().getProject().getPath() + "\\Problems\\");
    	File sourceFile = new File(root, problemName + ".java");
    	//sourceFile.getParentFile().mkdirs();
    	//Files.write(sourceFile.toPath(), source, StandardCharsets.UTF_8);

    	// Compile source file.
    	JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
    	//System.out.println(sourceFile.getPath());
    	compiler.run(null, null, null, sourceFile.getPath());

    	URL url = root.toURL();          // file:/c:/myclasses/
        URL[] urls = new URL[]{url};

        ClassLoader cl = new URLClassLoader(urls);
        Class cls = cl.loadClass(problemName);
        //System.out.println(cls.getName());
        Constructor constructor = cls.getConstructor(String.class);
        problem = (Problem) constructor.newInstance(instancePath);

        return problem;

    	// Load and instantiate compiled class.
    	/*URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] { root.toURI().toURL() });
    	Class<?> cls = Class.forName("Test", true, classLoader); // Should print "hello".
    	Object instance = cls.newInstance(); // Should print "world".
    	System.out.println(instance); // Should print "test.Test@hashcode".*/
    }

    public void dialogException(Exception ex){
    	Alert alert = new Alert(AlertType.ERROR);
    	alert.setTitle("Exception Dialog");
    	alert.setHeaderText("Look, an Exception Dialog");
    	alert.setContentText("Could not find file blabla.txt!");

    	// Create expandable Exception.
    	StringWriter sw = new StringWriter();
    	PrintWriter pw = new PrintWriter(sw);
    	ex.printStackTrace(pw);
    	String exceptionText = sw.toString();

    	Label label = new Label("The exception stacktrace was:");

    	TextArea textArea = new TextArea(exceptionText);
    	textArea.setEditable(false);
    	textArea.setWrapText(true);

    	textArea.setMaxWidth(Double.MAX_VALUE);
    	textArea.setMaxHeight(Double.MAX_VALUE);
    	GridPane.setVgrow(textArea, Priority.ALWAYS);
    	GridPane.setHgrow(textArea, Priority.ALWAYS);

    	GridPane expContent = new GridPane();
    	expContent.setMaxWidth(Double.MAX_VALUE);
    	expContent.add(label, 0, 0);
    	expContent.add(textArea, 0, 1);

    	// Set expandable Exception into the dialog pane.
    	alert.getDialogPane().setExpandableContent(expContent);

    	alert.showAndWait();
    }

    void metricToFile(String value, File file, boolean append){
    	try {
	      /* Open the file */
    	  FileWriter fw = new FileWriter(file, append);
	      BufferedWriter bw = new BufferedWriter(fw);

	      bw.write(value);

	      if(append)
	    	  bw.newLine();

	      /* Close the file */
	      bw.close();
	    }catch (IOException e) {
	      Configuration.logger_.severe("Error acceding to the file");
	      e.printStackTrace();
	    }
    }

}
