package application.model;

import java.util.HashMap;
import java.util.Map;


public class ParametersGA {

	private Map<String,Double> map;

	public ParametersGA() {
		map = new HashMap<String,Double>();
	}

	Map<String, Double> getMap(){
		return map;
	}

}
