package application.view;

import java.io.File;

import com.sun.prism.paint.Color;

import application.ApplicationContext;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class InformationBoxController {

	@FXML
	private Text text;

	@FXML
	private Button okButton;

	private Stage dialogStage;

	private double fontSize;

	public InformationBoxController() {
	}

	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

	public void setText(String _text){
		text.setText(_text);
	}

	public void setFontSize(double _fontSize){
		fontSize = _fontSize;
		text.setFont(Font.font(null, FontWeight.BOLD, fontSize));
	}

	/**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {
    	okButtonController();

    	Platform.runLater(new Runnable() {
   	     @Override
   	     public void run() {
   	         text.requestFocus();
   	     }
   	});
    }

    private void okButtonController(){
    	okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	dialogStage.close();
            }
    	});
    }

}
