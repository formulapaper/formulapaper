package application.view;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.sun.prism.paint.Color;

import application.ApplicationContext;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import jdk.nashorn.internal.runtime.arrays.NumericElements;

public class NewProblemFormController {

	@FXML
	private TextField name;

	@FXML
	private Button cancelButton;

	@FXML
	private Button okButton;

	@FXML
	private ChoiceBox variableType;

	@FXML
	private ChoiceBox numberOfObjectives;

	@FXML
	private ChoiceBox numberOfConstraints;

	boolean okButtonClicked;

	private Stage dialogStage;


	public NewProblemFormController() {
		okButtonClicked = false;
	}

	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

	public String getName(){
		return name.getText();
	}

	public String getVariableType(){
		return variableType.getSelectionModel().getSelectedItem().toString();
	}

	public int getNumberOfObjectives(){
		return Integer.valueOf(numberOfObjectives.getSelectionModel().getSelectedItem().toString());
	}

	public int getNumberOfConstraints(){
		return Integer.valueOf(numberOfConstraints.getSelectionModel().getSelectedItem().toString());
	}

	/**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {
    	okButtonController();
    	cancelButtonController();

    	name.setText("");

    	initializeComboBox();
    }

    public boolean isOkClicked(){
    	return okButtonClicked;
    }


    private void okButtonController(){
    	okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            	//Validate
            	if(!name.getText().equals("")){
	        		okButtonClicked = true;

	        		dialogStage.close();
            	}else{
            		name.setStyle("-fx-background-color: #FF5B5E;");
            	}
            }
    	});
    }

    private void cancelButtonController(){
    	cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	dialogStage.close();
            }
    	});
    }

    private void initializeComboBox(){
    	List<String> variableTypeList = new ArrayList<String>();
    	variableTypeList.add("Integer");
    	variableType.getItems().addAll(variableTypeList);
    	if(variableTypeList.size() > 0)
    		variableType.setValue(variableTypeList.get(0));

    	List<String> numberOfObjectivesList = new ArrayList<String>();
    	for(int i=1;i<=10;i++){
    		numberOfObjectivesList.add(i + "");
    	}
    	numberOfObjectives.getItems().addAll(numberOfObjectivesList);
    	if(numberOfObjectivesList.size() > 0)
    		numberOfObjectives.setValue(numberOfObjectivesList.get(0));

    	List<String> numberOfConstraintsList = new ArrayList<String>();
    	for(int i=0;i<=10;i++){
    		numberOfConstraintsList.add(i + "");
    	}
    	numberOfConstraints.getItems().addAll(numberOfConstraintsList);
    	if(numberOfObjectivesList.size() > 0)
    		numberOfConstraints.setValue(numberOfConstraintsList.get(0));
    }

}
