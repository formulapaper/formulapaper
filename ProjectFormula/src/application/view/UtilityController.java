package application.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import com.sun.deploy.uitoolkit.impl.fx.ui.FXConsole;

import application.ApplicationContext;
import application.MainApp;
import application.model.AlgorithmConfiguration;
import application.model.AlgorithmFactory;
import application.model.AlgorithmParameters;
import application.model.ProblemType;
import box.InformationBox;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.DoubleSpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.util.JMException;
import magius.GeneralProblem;
import undecorator.insidefx.undecorator.Undecorator;
import undecorator.insidefx.undecorator.UndecoratorScene;

public class UtilityController {

	@FXML
	private ImageView backButton;

	@FXML
	private Button createParetoFrontButton;

	/**
     * The constructor is called before initialize()
     */
    public UtilityController() {

	}

    /**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {

    	initButtons();

    	//BackButton Action
    	backButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
    	     @Override
    	     public void handle(MouseEvent event) {
    	    	 ApplicationContext.getContext().getMainApp().loadScene("MainMenu");
    	    	 ApplicationContext.getContext().getMainApp().setScene();

    	     }
    	});
    }



    public void initButtons(){

    	createParetoFrontButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
    		@Override
    		public void handle(MouseEvent click){
    			BorderPane page = null;
    	       	try{
    	           	FXMLLoader loader = new FXMLLoader();
    	                loader.setLocation(MainApp.class.getResource("view/CreateParetoFront.fxml"));
    	                page = (BorderPane) loader.load();


    	               // Cria o palco dialogStage.
    	               Stage dialogStage = new Stage();
    	               dialogStage.setTitle( "Create Pareto Front");
    	               dialogStage.initModality(Modality.WINDOW_MODAL);
    	               dialogStage.initOwner(ApplicationContext.getContext().getMainApp().getStage());
    	               UndecoratorScene scene = new UndecoratorScene(dialogStage, page);
    	               dialogStage.setScene(scene);

    	               dialogStage.sizeToScene();
    	               dialogStage.toFront();

    	               Undecorator undecorator = scene.getUndecorator();
    	               dialogStage.setMinWidth(undecorator.getMinWidth());
    	               dialogStage.setMinHeight(undecorator.getMinHeight());

    	               // Define a pessoa no controller.
    	               CreateParetoFrontController controller = loader.getController();
    	               controller.setDialogStage(dialogStage);


    	               // Mostra a janela e espera at� o usu�rio fechar.
    	               dialogStage.showAndWait();

    	               if(controller.isOkClicked()){

    	            	   System.out.println("OK clicked");

    	               }

    	            } catch (IOException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
    		}
		});

    }
}
