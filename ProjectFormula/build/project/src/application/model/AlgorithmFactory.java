package application.model;

import application.ApplicationContext;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import jmetal.core.Algorithm;
import jmetal.core.Problem;
import jmetal.metaheuristics.mocell.MOCell;
import jmetal.metaheuristics.nsgaII.NSGAII;

public class AlgorithmFactory {

	public static Algorithm createAlgorithm(String algorithmName,Problem problem){

		switch(algorithmName){
		case "NSGA-II": return /*new NSGAII(problem);*/new NSGAII(problem,
											ApplicationContext.getContext().algorithmProgress,
											ApplicationContext.getContext().isRunningAlgorithm);

		case "MoCell": return /*new MOCell(problem);*/  new MOCell(problem,
									ApplicationContext.getContext().algorithmProgress,
									ApplicationContext.getContext().isRunningAlgorithm);

		default:
			return null;
		}



	}

}
