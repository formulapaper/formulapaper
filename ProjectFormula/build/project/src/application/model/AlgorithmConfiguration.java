package application.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AlgorithmConfiguration {

	private List<String> algorithm;
	private List<String> problem;

	private List<String> initialPopulation;
	private List<String> selection;
	private List<String> mutation;
	private List<String> crossover;

	public AlgorithmConfiguration() {
		algorithm = new ArrayList<String>();
		problem = new ArrayList<String>();

		initialPopulation = new ArrayList<String>();
		selection = new ArrayList<String>();
		mutation = new ArrayList<String>();
		crossover = new ArrayList<String>();

		loadStaticParameters();
	}

	private void loadStaticParameters(){
		//Algorith
		algorithm.add("NSGA-II");
		algorithm.add("MoCell");

		//Initial Population
		initialPopulation.add("Random");

		//Mutation
		mutation.add("BitFlipMutation");
		mutation.add("PolynomialMutation");
		mutation.add("SwapMutation");


		//Crossover
		crossover.add("SinglePointCrossover");
		crossover.add("SBXCrossover");
		crossover.add("PMXCrossover");

		//Selection
		selection.add("BinaryTournament");
	}

	public void addProblem(String problemName){
		problem.add(problemName);
	}

	public List<String> getAlgorithms(){
		return algorithm;
	}

	public List<String> getProblems(){
		return problem;
	}

	public List<String> getInitialPopulations(){
		return initialPopulation;
	}

	public List<String> getMutations(){
		return mutation;
	}

	public List<String> getCrossovers(){
		return crossover;
	}

	public List<String> getSelections(){
		return selection;
	}

}
