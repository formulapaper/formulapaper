package magius;

import magius.MathEval;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TextField;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.IntSolutionType;
import jmetal.util.JMException;

/**
 * Created by ThiNepo on 17/07/2014.
 */
public class GeneralProblem extends Problem implements Serializable{
    private static final long serialVersionUID = 6529685098267757690L;

    public ArrayList<String> objective[];
    public ArrayList<String> constraintPart1[];
    public ArrayList<String> constraintPart2[];
    public ArrayList<Loop> loop;
    public List<GeneralVariable> variable;

    public String customCodeObjective[];
    public String customCodeConstraint[];
    public String customCodeVariables;
    public String customCodeConstructor;

    public ArrayList<String> constraintType;

    public ArrayList<Integer> loopManagerObjectives[] = null;

    public ArrayList<Integer> loopManagerConstraintPart1[] = null;
    public ArrayList<Integer> loopManagerConstraintPart2[] = null;

    public ArrayList<String> readList;

    int count;

    private String variableType;

    private double globalUpperLimit, globalLowerLimit;

    public GeneralProblem(String title,String variableType, int numberOfVariables, int numberOfObjectives, int numberOfConstraints) {

        count = 0;

        numberOfVariables_ = numberOfVariables;
        numberOfObjectives_ = numberOfObjectives;
        numberOfConstraints_ = numberOfConstraints;
        problemName_ = title;

        globalLowerLimit = 0.0;
        globalUpperLimit = 1.0;

        upperLimit_ = new double[numberOfVariables_];
        lowerLimit_ = new double[numberOfVariables_];

        this.setVariableType("Null");

        try {
            //solutionType_ = new BinarySolutionType(this);
            if (variableType.contentEquals("Integer")){
                solutionType_ = new IntSolutionType(this);
                this.setVariableType(variableType);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < numberOfVariables_; i++) {
            lowerLimit_[i] = globalLowerLimit;
            upperLimit_[i] = globalUpperLimit;
        }

        objective = new ArrayList[numberOfObjectives];
        for (int i = 0; i < numberOfObjectives; i++)
            objective[i] = new ArrayList<String>();

        constraintPart1 = new ArrayList[numberOfConstraints];
        constraintPart2 = new ArrayList[numberOfConstraints];
        constraintType = new ArrayList<String>();
        for (int i = 0; i < numberOfConstraints; i++) {
        	constraintType.add("<");
            constraintPart1[i] = new ArrayList<String>();
            constraintPart2[i] = new ArrayList<String>();
        }

        loop = new ArrayList<Loop>();

        variable = new ArrayList<GeneralVariable>();

        readList = new ArrayList<String>();

        //Special variable - Size of Solution Vector and Solution Vector
        variable.add(new GeneralVariable("Vector", "X", numberOfVariables_));
        variable.add(new GeneralVariable("Normal", "N"));

        variable.get(1).data = 0;
        variable.get(0).setVariableDimension1("N");

        //CustomCode
        customCodeVariables = "";
        customCodeConstructor = "";

        customCodeObjective = new String[numberOfObjectives];
        for(int i = 0; i < numberOfObjectives; ++i){
        	customCodeObjective[i] = "";
        }

        customCodeConstraint= new String[numberOfConstraints];
        for(int i = 0; i < numberOfConstraints; ++i){
        	customCodeConstraint[i] = "";
        }


    }

    public void setNumberOfVariables(int numberOfVariables){
    	numberOfVariables_ = numberOfVariables;

    	upperLimit_ = new double[numberOfVariables_];
        lowerLimit_ = new double[numberOfVariables_];

        for (int i = 0; i < numberOfVariables_; i++) {
            lowerLimit_[i] = globalLowerLimit;
            upperLimit_[i] = globalUpperLimit;
        }

        //Search for solution vector X and resize
        for(int i=0;i<variable.size();i++){
        	if(variable.get(i).name.equals("X")){
        		variable.get(i).resize(numberOfVariables_);
        	}
        }
    }

    public void updateValues(String title,String variableType, int numberOfVariables, int numberOfObjectives, int numberOfConstraints) {
        count = 0;

        numberOfVariables_ = numberOfVariables;
        numberOfObjectives_ = numberOfObjectives;
        numberOfConstraints_ = numberOfConstraints;
        problemName_ = title;

        upperLimit_ = new double[numberOfVariables_];
        lowerLimit_ = new double[numberOfVariables_];

        try {
            //solutionType_ = new BinarySolutionType(this);
            if (variableType.contentEquals("Integer"))
                solutionType_ = new IntSolutionType(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < numberOfVariables_; i++) {
            lowerLimit_[i] = 0;
            upperLimit_[i] = 1;
        }
    }

    public void loadProblem(){

    }

    public void loadInstance(File file){
        try {
            Scanner scn = new Scanner(file);
            for(int i=0;i<readList.size();i++){
                for(int j=0;j<variable.size();j++){
                    if(variable.get(j).name.contentEquals(readList.get(i))){
                        if(variable.get(j).type.contentEquals("Normal")){
                            variable.get(j).data = Integer.valueOf(scn.next());
                            System.out.println(variable.get(j).data);

                            //If is N then set chromossome size
                            if(variable.get(j).name.equals("N")){
                            	System.out.println("Read N value");
                            	setNumberOfVariables(variable.get(j).data);
                            }

                            for(GeneralVariable gv : variable){
                            	if(gv.getVariableDimension2().equals(variable.get(j).name) && gv.type.equals("Matrix")){
                            		gv.resize(gv.dimension1, variable.get(j).data);
                            	}else if(gv.getVariableDimension1().equals(variable.get(j).name)){
                            		if(gv.type.equals("Vector"))
                            			gv.resize(variable.get(j).data);
                            		else if(gv.type.equals("Matrix")){
                            			gv.resize(variable.get(j).data, gv.dimension2);
                            		}

                            	}
                            }

                            for(Loop l : loop){
                            	if(l.getInitialVariable().equals(variable.get(j).name)){
                            		l.initial = variable.get(j).data;
                            	}

                            	if(l.getFinalVariable().equals(variable.get(j).name)){
                            		l.N = variable.get(j).data;
                            	}
                            }

                        }else if(variable.get(j).type.contentEquals("Vector")){
                            for(int k=0;k<variable.get(j).dimension1;k++) {
                                variable.get(j).vectorData[k] = Integer.valueOf(scn.next());
                                System.out.print(variable.get(j).vectorData[k] + " ");
                            }
                            System.out.println("");

                        }else if(variable.get(j).type.contentEquals("Matrix")){
                        for(int k1=0;k1<variable.get(j).dimension1;k1++) {
                            for(int k2=0;k2<variable.get(j).dimension2;k2++) {
                                variable.get(j).matrixData[k1][k2] = Integer.valueOf(scn.next());
                            }
                        }
                        System.out.println("");

                    }
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void findLoops(){

        System.out.println("ENTROU FIND LOOPS");

        //Objectives
        loopManagerObjectives = new ArrayList[numberOfObjectives_];
        for(int j=0;j<numberOfObjectives_;j++) {
            Stack<Integer> stack = new Stack<Integer>();
            loopManagerObjectives[j] = new ArrayList<Integer>();
            for (int i = 0; i < objective[j].size();i++){
                loopManagerObjectives[j].add(-1);
                if(objective[j].get(i).startsWith("L")){
                    loopManagerObjectives[j].set(i,Integer.valueOf(objective[j].get(i).substring(1)));
                    stack.add(i);
                }else if(objective[j].get(i).startsWith("]")){
                    loopManagerObjectives[j].set(i,stack.peek());
                    loopManagerObjectives[j].set(stack.peek(), i);
                    stack.pop();
                }else{
                    loopManagerObjectives[j].set(i, -1);
                }
            }
        }

        //Constraint Part 1
        loopManagerConstraintPart1 = new ArrayList[numberOfConstraints_];
        for(int j=0;j<numberOfConstraints_;j++) {
            Stack<Integer> stack = new Stack<Integer>();
            loopManagerConstraintPart1[j] = new ArrayList<Integer>();
            for (int i = 0; i < constraintPart1[j].size();i++){
                loopManagerConstraintPart1[j].add(-1);
                if(constraintPart1[j].get(i).startsWith("L")){
                    loopManagerConstraintPart1[j].set(i,Integer.valueOf(constraintPart1[j].get(i).substring(1)));
                    stack.add(i);
                }else if(constraintPart1[j].get(i).startsWith("]")){
                    loopManagerConstraintPart1[j].set(i,stack.peek());
                    loopManagerConstraintPart1[j].set(stack.peek(), i);
                    stack.pop();
                }else{
                    loopManagerConstraintPart1[j].set(i, -1);
                }
            }
        }

        //Constraint Part 2
        loopManagerConstraintPart2 = new ArrayList[numberOfConstraints_];
        for(int j=0;j<numberOfConstraints_;j++) {
            Stack<Integer> stack = new Stack<Integer>();
            loopManagerConstraintPart2[j] = new ArrayList<Integer>();
            for (int i = 0; i < constraintPart2[j].size();i++){
                loopManagerConstraintPart2[j].add(-1);
                if(constraintPart2[j].get(i).startsWith("L")){
                    loopManagerConstraintPart2[j].set(i,Integer.valueOf(constraintPart2[j].get(i).substring(1)));
                    stack.add(i);
                }else if(constraintPart2[j].get(i).startsWith("]")){
                    loopManagerConstraintPart2[j].set(i,stack.peek());
                    loopManagerConstraintPart2[j].set(stack.peek(), i);
                    stack.pop();
                }else{
                    loopManagerConstraintPart2[j].set(i, -1);
                }
            }
        }

        System.out.println("SAIU FIND LOOPS");

    }

    public void loadTestProblem(){

        //OBJECTIVE 0
        objective[0].add("100");
        objective[0].add("+");
        objective[0].add("L0");
        objective[0].add("V0");
        objective[0].add("i");
        objective[0].add("]");
        objective[0].add("+");
        objective[0].add("10");

        loop.add(new Loop("sum", "i",0, 10));

        //OBJECTIVE 1
        loop.add(new Loop("prod", "j",0, 10));
        objective[1].add("L1");
        objective[1].add("V0");
        objective[1].add("j");
        objective[1].add("+");
        objective[1].add("2");
        objective[1].add("]");

        variable.add(new GeneralVariable("Normal","maxCost"));
        variable.add(new GeneralVariable("Vector","A",numberOfVariables_));

        //Constraint 1
        constraintPart1[0].add("L0");
        constraintPart1[0].add("V0");
        constraintPart1[0].add("i");
        constraintPart1[0].add("*");
        constraintPart1[0].add("V2");
        constraintPart1[0].add("i");
        constraintPart1[0].add("]");

        constraintPart2[0].add("V1");

        constraintType.add("<");

    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    public String getComparisionSymbol(String expression){

    	if(expression.compareTo("<=") == 0)
    		return "\\leq";

    	if(expression.compareTo("==") == 0)
    		return "=";

    	if(expression.compareTo("!=") == 0)
    		return "\\neq";

    	if(expression.compareTo(">=") == 0)
    		return "\\geq";

    	return expression;
    }

    public String getTex(ArrayList<String> vector[], int obj){
        String expression = "";

        for(int i=0;i<vector[obj].size();i++){
            if(vector[obj].get(i).startsWith("L")){ //LOOP
                expression += loop.get(Integer.valueOf(vector[obj].get(i).substring(1))).getTex() + " ";
            }else if(vector[obj].get(i).startsWith("V")){
                if(variable.get(Integer.valueOf(vector[obj].get(i).substring(1))).type.contentEquals("Normal")){
                    expression += variable.get(Integer.valueOf(vector[obj].get(i).substring(1))).getTex() + " ";
                }else if(variable.get(Integer.valueOf(vector[obj].get(i).substring(1))).type.contentEquals("Vector")){
                    expression += variable.get(Integer.valueOf(vector[obj].get(i).substring(1))).getTex();
                    expression += "_{" + vector[obj].get(i+1) + "} ";
                    i++;
                }else if(variable.get(Integer.valueOf(vector[obj].get(i).substring(1))).type.contentEquals("Matrix")) {
                    expression += variable.get(Integer.valueOf(vector[obj].get(i).substring(1))).getTex();
                    expression += "_{" + vector[obj].get(i+1);
                    expression += "," + vector[obj].get(i+2) + "} ";
                    i+=2;
                }
            }else{
                expression += (vector[obj].get(i) + " ");
            }
        }



        //System.out.println("LATEX EXPRESSION: "  + expression);

        return expression;
    }

    //Processando apenas Normal e Vector
    int processVariable(ArrayList<String> vector[], int obj, int index){
        //System.out.println("Processing variable type " + variable.get(index).type);
        if(variable.get(index).type.contentEquals("Normal")){
            count++;
            return variable.get(index).data;
        }else if(variable.get(index).type.contentEquals("Vector")){
            String dimension1 = vector[obj].get(count+1);
            if(isInteger(dimension1)){
            	count += 2;
                return variable.get(index).vectorData[Integer.valueOf(dimension1)];
            }else{
	            for(int i=0;i<loop.size();i++){
	                if(loop.get(i).identifier.contentEquals(dimension1)){
	                    count += 2;
	                    return variable.get(index).vectorData[loop.get(i).count];
	                }
	            }
            }

        }

        return -1;
    }

    double evaluationExpression(String expression){
        MathEval math=new MathEval();

        //System.out.println("--------------------------AVALIANDO " + expression + "-------------------------------------");
        try {
            //System.out.println("--------------------------RESULTS " + math.evaluate(expression) + "-------------------------------------");
            return math.evaluate(expression);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0.0;
    }




    double processLoop(ArrayList<String> vector[], ArrayList<Integer> loopManager[], int obj, int index){
        String value = "";
        String expression = "";
        loop.get(index).count = loop.get(index).initial;

        count++;

        while(loop.get(index).count < loop.get(index).N){

            /*for(int i=0;i<vector[obj].size();i++){
                System.out.print(vector[obj].get(i) + " ");
            }
            System.out.println();*/

            //System.out.println("LOOP : " + loop.get(index).count + " / " + loop.get(Integer.valueOf(index)).N + " OBJ: " + obj + " VECTOR SIZE: " + vector[obj].size() + " COUNT: " + count );
            //System.out.println("Looking to " + vector[obj].get(count));
            //System.out.println("Value: " + value);
            if(vector[obj].get(count).startsWith("V")) {
                int varIndex = Integer.valueOf(vector[obj].get(count).substring(1));
                value += String.valueOf(processVariable(vector,obj,varIndex));
            }else if(vector[obj].get(count).startsWith("L")) {
                value += String.valueOf(processLoop(vector, loopManager, obj,Integer.valueOf(vector[obj].get(count).substring(1))));
            }else if(vector[obj].get(count).contentEquals("]")){
                count = loopManager[obj].get(count);
                loop.get(Integer.valueOf(vector[obj].get(count).substring(1))).count++;

                if(loop.get(index).count >= loop.get(Integer.valueOf(index)).N) count = loopManager[obj].get(count);

                if(expression.contentEquals("")){
                    expression += "(" + value + ")";
                }else{
                    if(loop.get(index).type.contentEquals("sum")){
                        expression += " + (" + value + ")";
                    }else if(loop.get(index).type.contentEquals("prod")){
                        expression += " * (" + value + ")";
                    }
                }

                value = "";

                count++;
            }else{
                value += vector[obj].get(count);
                count++;
            }
        }

        //System.out.println("LOOP: Avaliando a expressão " + expression + " => " + evaluationExpression(expression));

        return evaluationExpression(expression);
    }

    double evaluateFormula(ArrayList<String> vector[], ArrayList<Integer> loopManager[], Variable _variable[], int obj){
            //Load solution vector
            try {
                for(int i = 0;i < numberOfVariables_;i++)
                   variable.get(0).vectorData[i] = (int) _variable[i].getValue();
            } catch (JMException e) {
                e.printStackTrace();
            }

            //Clear Loops
            for(int i=0;i<loop.size();i++) loop.get(i).count = loop.get(i).initial;

            count = 0;

            String expression = "";

        //System.out.println("OBJ " + obj + " " + vector[obj].size());

            while(count < vector[obj].size()){
                //System.out.println("Looking to " + vector[obj].get(count) + " COUNT: " + count);
                if(vector[obj].get(count).startsWith("V")) {
                    int varIndex = Integer.valueOf(vector[obj].get(count).substring(1));
                    expression += (String.valueOf(processVariable(vector,obj,varIndex)));
                }else if(vector[obj].get(count).startsWith("L")) {
                    expression += processLoop(vector,loopManager, obj,Integer.valueOf(vector[obj].get(count).substring(1))) + "" ;
                }else if(vector[obj].get(count).contentEquals("]")){
                    count = loopManager[obj].get(count);
                    loop.get(Integer.valueOf(vector[obj].get(count).substring(1))).count++;
                    count++;
                }else{
                    expression += ( vector[obj].get(count) );
                    count++;
                }

            }
            //System.out.println("MAIN OBJ " + obj + " : Avaliando a expressão " + expression + " => " + evaluationExpression(expression));

            return evaluationExpression(expression);
            //solution.setObjective(obj, -evaluationExpression(expression));
    }

    @Override
    public void evaluate(Solution solution) throws JMException {
        Variable _variable[] = solution.getDecisionVariables();
        double value;

        //Objectives
        for(int i=0;i<numberOfObjectives_;i++){
            value = evaluateFormula(objective,loopManagerObjectives,_variable,i);
            solution.setObjective(i, value);
        }
    }

    @Override
    public void evaluateConstraints(Solution solution) throws JMException {
        super.evaluateConstraints(solution);

        Variable _variable[] = solution.getDecisionVariables();

        solution.setNumberOfViolatedConstraint(0);
        solution.setOverallConstraintViolation(0);

        //Constraint
        for(int i=0;i<numberOfConstraints_;i++){

            double a, b;
            a = evaluateFormula(constraintPart1,loopManagerConstraintPart1,_variable,i);
            b = evaluateFormula(constraintPart2,loopManagerConstraintPart2,_variable,i);

            //System.out.println(a + " " + b);

            if(constraintType.get(i).contentEquals("<")){
                if(a > b) {
                    solution.setNumberOfViolatedConstraint(1);
                    solution.setOverallConstraintViolation(-1);
                }
            }else if(constraintType.get(i).contentEquals(">")){
                if(a < b) {
                    solution.setNumberOfViolatedConstraint(1);
                    solution.setOverallConstraintViolation(-1);
                }
            }else if(constraintType.get(i).contentEquals("=")){
                if(a != b) {
                    solution.setNumberOfViolatedConstraint(1);
                    solution.setOverallConstraintViolation(-1);
                }
            }

        }

    }

	public String getVariableType() {
		return variableType;
	}

	public void setVariableType(String variableType) {
		this.variableType = variableType;
	}

	public double getGlobalLowerLimit() {
		return globalLowerLimit;
	}

	public void setGlobalLowerLimit(double globalLowerLimit) {
		this.globalLowerLimit = globalLowerLimit;
	}

	public double getGlobalUpperLimit() {
		return globalUpperLimit;
	}

	public void setGlobalUpperLimit(double globalUpperLimit) {
		this.globalUpperLimit = globalUpperLimit;
	}
}
