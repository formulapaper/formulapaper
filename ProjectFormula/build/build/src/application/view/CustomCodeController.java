package application.view;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.StyleSpans;
import org.fxmisc.richtext.StyleSpansBuilder;

import com.sun.prism.paint.Color;

import application.ApplicationContext;
import box.InformationBox;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class CustomCodeController {

	@FXML
	private Button okButton;

	@FXML
	private Button helpButton;

	@FXML
	private AnchorPane mainPane;

	private Stage dialogStage;

	CodeArea codeArea;
	private String code;

	private double fontSize;

	private static final String[] KEYWORDS = new String[] {
            "abstract", "assert", "boolean", "break", "byte",
            "case", "catch", "char", "class", "const",
            "continue", "default", "do", "double", "else",
            "enum", "extends", "final", "finally", "float",
            "for", "goto", "if", "implements", "import",
            "instanceof", "int", "interface", "long", "native",
            "new", "package", "private", "protected", "public",
            "return", "short", "static", "strictfp", "super",
            "switch", "synchronized", "this", "throw", "throws",
            "transient", "try", "void", "volatile", "while", "String"
    };

	private static final String[] OBJCONST = new String[] {
            "OBJ", "PENALITY"
    };

    private static final String KEYWORD_PATTERN = "\\b(" + String.join("|", KEYWORDS) + ")\\b";
    private static final String OBJ_CONST_PATTERN = "\\b(" + String.join("|", OBJCONST) + ")\\b";
    private static final String PAREN_PATTERN = "\\(|\\)";
    private static final String BRACE_PATTERN = "\\{|\\}";
    private static final String BRACKET_PATTERN = "\\[|\\]";
    private static final String SEMICOLON_PATTERN = "\\;";
    private static final String STRING_PATTERN = "\"([^\"\\\\]|\\\\.)*\"";
    private static final String COMMENT_PATTERN = "//[^\n]*" + "|" + "/\\*(.|\\R)*?\\*/";

    private static final Pattern PATTERN = Pattern.compile(
            "(?<KEYWORD>" + KEYWORD_PATTERN + ")"
            + "|(?<OBJCONST>" + OBJ_CONST_PATTERN + ")"
            + "|(?<PAREN>" + PAREN_PATTERN + ")"
            + "|(?<BRACE>" + BRACE_PATTERN + ")"
            + "|(?<BRACKET>" + BRACKET_PATTERN + ")"
            + "|(?<SEMICOLON>" + SEMICOLON_PATTERN + ")"
            + "|(?<STRING>" + STRING_PATTERN + ")"
            + "|(?<COMMENT>" + COMMENT_PATTERN + ")"
    );

	public CustomCodeController() {
		code = "";
	}

	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

	/**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {
    	buttonsController();

    	codeArea = new CodeArea();
        codeArea.setParagraphGraphicFactory(LineNumberFactory.get(codeArea));

        codeArea.richChanges().subscribe(change -> {
            codeArea.setStyleSpans(0, computeHighlighting(codeArea.getText()));
        });

        codeArea.replaceText(0, 0, code);

        codeArea.setMinHeight(mainPane.getMinHeight());
        codeArea.setMinWidth(mainPane.getMinWidth());

        //codeArea.getStylesheets().add(JavaKeywordsAsync.class.getResource("java-keywords.css").toExternalForm());

        mainPane.getChildren().add(codeArea);


    }

    private static final String sampleCode = String.join("\n", new String[] {
	        "package com.example;",
	        "",
	        "import java.util.*;",
	        "",
	        "public class Foo extends Bar implements Baz {",
	        "",
	        "    /*",
	        "     * multi-line comment",
	        "     */",
	        "    public static void main(String[] args) {",
	        "        // single-line comment",
	        "        for(String arg: args) {",
	        "            if(arg.length() != 0)",
	        "                System.out.println(arg);",
	        "            else",
	        "                System.err.println(\"Warning: empty string as argument\");",
	        "        }",
	        "    }",
	        "",
	        "}"
	    });

    private static StyleSpans<Collection<String>> computeHighlighting(String text) {
        Matcher matcher = PATTERN.matcher(text);
        int lastKwEnd = 0;
        StyleSpansBuilder<Collection<String>> spansBuilder
                = new StyleSpansBuilder<>();
        while(matcher.find()) {
            String styleClass =
                    matcher.group("KEYWORD") != null ? "keyword" :
                    matcher.group("OBJCONST") != null ? "obj_const" :
                    matcher.group("PAREN") != null ? "paren" :
                    matcher.group("BRACE") != null ? "brace" :
                    matcher.group("BRACKET") != null ? "bracket" :
                    matcher.group("SEMICOLON") != null ? "semicolon" :
                    matcher.group("STRING") != null ? "string" :
                    matcher.group("COMMENT") != null ? "comment" :
                    null; /* never happens */ assert styleClass != null;
            spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
            spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
            lastKwEnd = matcher.end();
        }
        spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
        return spansBuilder.create();
    }

    private void buttonsController(){
    	okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	code = codeArea.getText();
            	dialogStage.close();
            }
    	});

    	helpButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	String new_line = System.getProperty("line.separator");
            	InformationBox informationBox = new InformationBox("Help",
            			"Here you can put your custom code right after the generated code." + new_line
            			+ "This bring the possibility to you modify the value of objectives and constraints. To do that, use the variables:"
            			+ " OBJ to objectives and"
            			+ " PENALITY to constraints"
            			+ " Remember, this will be the final values to this objective or constraint. So, problably you would like to use the operator "
            			+ "+= to preserve the value calculed by the generated code.", 10);

            	informationBox.show();
            }
    	});
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
		codeArea.replaceText(0, 0, code);
	}

}
