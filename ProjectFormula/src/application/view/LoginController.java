package application.view;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import application.ApplicationContext;
import application.MainApp;
import application.model.AlgorithmConfiguration;
import box.InformationBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.User;
import undecorator.insidefx.undecorator.Undecorator;
import undecorator.insidefx.undecorator.UndecoratorScene;
import webservice.FormulaWebService;

public class LoginController {

	@FXML
	private Button signInButton;

	@FXML
	private Label serverStatus;

	@FXML
	private Text errorMessage;

	@FXML
	private TextField emailField;

	@FXML
	private PasswordField passwordField;

	@FXML
	private Label email;

	@FXML
	private Label password;

	String status;


	/**
     * The constructor is called before initialize()
     */
    public LoginController() {

	}

	/**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {

    	//initializeCreateButton();
    	initializeSignInButton();

    	serverStatus.setText("Server Status: " + FormulaWebService.getStatus());

    	errorMessage.setFont(new Font(null, 20));
    	errorMessage.setVisible(false);


    	//Increase font to labels "Email" and "Password"
    	email.setFont(new Font(null, 24));
    	password.setFont(new Font(null, 24));

    }

    public void initializeSignInButton(){
    	signInButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            	String email, password;

            	email = emailField.getText();
            	password = passwordField.getText();

            	//Special case
            	/*if(email.equals("thi.nepo@gmail.com") && password.equals("roma2536")){
            	  	ApplicationContext.getContext().getMainApp().loadScene("Menu");
                	ApplicationContext.getContext().getMainApp().setScene();
            	}else{
            		User user = FormulaWebService.getUser(email, password);
	            	if(user == null){
	            		errorMessage.setVisible(true);
	            	}else{*/
	            	  	ApplicationContext.getContext().getMainApp().loadScene("Menu");
                    	ApplicationContext.getContext().getMainApp().setScene();
	            	/*}
            	}*/



            }
    	});
    }

}
