package application.model;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import application.ApplicationContext;
import jmetal.core.Operator;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.util.JMException;

@XmlRootElement
public class AlgorithmParameters {

	private String name;

	private HashMap<String, Object> map;

	private List<File> instance;

	private int quantityRuns;

	private String algorithm;

	private String problem;

	private String outputPattern;

	private String crossoverName, mutationName, selectionName;

	private double crossoverProbability, mutationProbability;

	Operator  crossover ; // Crossover operator
    Operator  mutation  ; // Mutation operator
    Operator  selection ; // Selection operator

	public AlgorithmParameters(){
		map = new HashMap<String, Object>();
		instance = new ArrayList<File>();

		name = "";
	}

	//Copy Constructor
	public AlgorithmParameters(AlgorithmParameters other) {
		this.name = other.getName();
		this.setMap(other.getMap());
		instance = new ArrayList<File>();
		this.instance.addAll(other.getInstance());
		this.quantityRuns = other.getQuantityRuns();
		this.algorithm = other.getAlgorithm();
		this.problem = other.getProblem();
		this.outputPattern = other.getOutputPattern();
		this.crossoverName = other.getCrossoverName();
		this.mutationName = other.getMutationName();
		this.selectionName = other.getSelectionName();
		try {
			this.setCrossover(other.getCrossoverName(), other.getCrossoverProbability());
			this.setMutation(other.getMutationName(), other.getMutationProbability());
			this.setSelection(other.getSelectionName());
		} catch (JMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setMap(HashMap<String, Object> map) {
		this.map = map;
	}

	public Operator getCrossover(){
		return crossover;
	}

	public Operator getMutation(){
		return mutation;
	}

	public Operator getSelection(){
		return selection;
	}

	public HashMap<String, Object> getMap(){
		return map;
	}

	public void addParameter(String name, double value){
		map.put(name, value);
	}

	public void addParameter(String name, int value){
		map.put(name, value);
	}

	public void setCrossover(String name, double probability) throws JMException{
		HashMap parameters = new HashMap() ;

	    parameters.put("probability", probability) ;
	    parameters.put("distributionIndex", 20.0) ;

	    crossover = CrossoverFactory.getCrossoverOperator(name, parameters);


	    crossoverName = name;
	    crossoverProbability = probability;
	}

	public void setMutation(String name, double probability) throws JMException{
		HashMap parameters = new HashMap() ;

	    parameters.put("probability", probability) ;
	    parameters.put("distributionIndex", 20.0) ;

	    mutation = MutationFactory.getMutationOperator(name, parameters);

	    mutationName = name;
	    mutationProbability = probability;
	}

	public void setSelection(String name) throws JMException{
		HashMap parameters = null;

	    selection = SelectionFactory.getSelectionOperator(name, parameters);

	    selectionName = name;
	}


	public List<File> getInstance() {
		return instance;
	}

	public void setInstance(List<File> instance) {
		this.instance = instance;
	}

	public int getQuantityRuns() {
		return quantityRuns;
	}

	public void setQuantityRuns(int quantityRuns) {
		this.quantityRuns = quantityRuns;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public String getProblem() {
		return problem;
	}

	public void setProblem(String problem) {
		this.problem = problem;
	}

	public String getOutputPattern() {
		return outputPattern;
	}

	public void setOutputPattern(String outputPattern) {
		this.outputPattern = outputPattern;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString(){
		return name;
	}

	public String getCrossoverName() {
		return crossoverName;
	}

	public void setCrossoverName(String crossoverName) {
		this.crossoverName = crossoverName;
	}

	public String getMutationName() {
		return mutationName;
	}

	public void setMutationName(String mutationName) {
		this.mutationName = mutationName;
	}

	public String getSelectionName() {
		return selectionName;
	}

	public void setSelectionName(String selectionName) {
		this.selectionName = selectionName;
	}

	public double getCrossoverProbability() {
		return crossoverProbability;
	}

	public void setCrossoverProbability(double crossoverProbability) {
		this.crossoverProbability = crossoverProbability;
	}

	public double getMutationProbability() {
		return mutationProbability;
	}

	public void setMutationProbability(double mutationProbability) {
		this.mutationProbability = mutationProbability;
	}

}
