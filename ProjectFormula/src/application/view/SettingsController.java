package application.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import com.sun.deploy.uitoolkit.impl.fx.ui.FXConsole;

import application.ApplicationContext;
import application.model.AlgorithmConfiguration;
import application.model.AlgorithmFactory;
import application.model.ChartPopulation;
import application.model.ProblemType;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.DoubleSpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.util.JMException;
import magius.GeneralProblem;
import undecorator.insidefx.undecorator.UndecoratorScene;

public class SettingsController {

	@FXML
	private Button okButton;

	@FXML
	private ComboBox style;

	boolean okButtonClicked;

	private Stage dialogStage;


	/**
     * The constructor is called before initialize()
     */
    public SettingsController() {
    	okButtonClicked = false;
	}

    public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

    /**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {
    	okButtonController();


    	List<String> styleList = new ArrayList<String>();
    	styleList.add("Default");
    	styleList.add("Black and White");

    	style.getItems().addAll(styleList);
    	if(styleList.size() > 0){
    		style.setValue(styleList.get(ApplicationContext.getContext().getMainApp().getGeneralSettings().getSelectedStyle()));
    	}


    }

    public boolean isOkClicked(){
    	return okButtonClicked;
    }

    private void okButtonController(){
    	okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            		ApplicationContext.getContext().getMainApp().getGeneralSettings().setSelectedStyle(style.getSelectionModel().getSelectedIndex());
            		ApplicationContext.getContext().getMainApp().getGeneralSettings().save();

            		dialogStage.close();


            }
    	});
    }

}
