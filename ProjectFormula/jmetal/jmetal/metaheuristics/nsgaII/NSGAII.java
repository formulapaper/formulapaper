//  NSGAII.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.metaheuristics.nsgaII;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.value.ObservableValue;
import jmetal.core.*;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.Distance;
import jmetal.util.JMException;
import jmetal.util.Ranking;
import jmetal.util.comparators.CrowdingComparator;
import mpi.MPI;
import mpi.Status;
import mpj.StartMPJ.TAG;

/**
 *  Implementation of NSGA-II.
 *  This implementation of NSGA-II makes use of a QualityIndicator object
 *  to obtained the convergence speed of the algorithm. This version is used
 *  in the paper:
 *     A.J. Nebro, J.J. Durillo, C.A. Coello Coello, F. Luna, E. Alba
 *     "A Study of Convergence Speed in Multi-Objective Metaheuristics."
 *     To be presented in: PPSN'08. Dortmund. September 2008.
 */

public class NSGAII extends Algorithm {

	DoubleProperty progress;
	BooleanProperty running;

 /**
   * Constructor
   * @param problem Problem to solve
   */
  public NSGAII(Problem problem) {
    super (problem) ;

    progress = null;
    running = null;

    //Change to test MPI
    useMPI = false;
  } // NSGAII

  /**
   * Constructor
   * @param problem Problem to solve
   */
  public NSGAII(Problem problem, DoubleProperty _progress, BooleanProperty _running) {
    super (problem) ;

    progress = _progress;
    running = _running;

    //Change to test MPI
    useMPI = false;
  } // NSGAII

  /**
   * Runs the NSGA-II algorithm.
   * @return a <code>SolutionSet</code> that is a set of non dominated solutions
   * as a result of the algorithm execution
   * @throws JMException
   */
  public SolutionSet execute() throws JMException, ClassNotFoundException {
    int populationSize;
    int maxEvaluations;
    int evaluations;

    QualityIndicator indicators; // QualityIndicator object
    int requiredEvaluations; // Use in the example of use of the
    // indicators object (see below)

    SolutionSet population;
    SolutionSet offspringPopulation;
    SolutionSet union;

    Operator mutationOperator;
    Operator crossoverOperator;
    Operator selectionOperator;

    Distance distance = new Distance();

    //Read the parameters
    populationSize = ((Integer) getInputParameter("populationSize")).intValue();
    maxEvaluations = ((Integer) getInputParameter("maxEvaluations")).intValue();
    indicators = (QualityIndicator) getInputParameter("indicators");

    //Initialize the variables
    population = new SolutionSet(populationSize);
    evaluations = 0;

    requiredEvaluations = 0;

    //Read the operators
    mutationOperator = operators_.get("mutation");
    crossoverOperator = operators_.get("crossover");
    selectionOperator = operators_.get("selection");

    // Create the initial solutionSet
    Solution newSolution;
    for (int i = 0; i < populationSize; i++) {
      newSolution = new Solution(problem_);
      problem_.evaluate(newSolution);
      problem_.evaluateConstraints(newSolution);
      evaluations++;
      population.add(newSolution);
    } //for

    // Generations
    while (evaluations < maxEvaluations) {
      //System.out.println((double) evaluations / maxEvaluations);
      if(running != null && running.getValue() == false) return new SolutionSet();
      if(progress != null) progress.set(((double) evaluations / maxEvaluations));

      // Create the offSpring solutionSet
      offspringPopulation = new SolutionSet(populationSize);
      Solution[] parents = new Solution[2];
      for (int i = 0; i < (populationSize / 2); i++) {
        if (evaluations < maxEvaluations) {
          //obtain parents
          parents[0] = (Solution) selectionOperator.execute(population);
          parents[1] = (Solution) selectionOperator.execute(population);
          Solution[] offSpring = (Solution[]) crossoverOperator.execute(parents);
          mutationOperator.execute(offSpring[0]);
          mutationOperator.execute(offSpring[1]);
          problem_.evaluate(offSpring[0]);
          problem_.evaluateConstraints(offSpring[0]);
          problem_.evaluate(offSpring[1]);
          problem_.evaluateConstraints(offSpring[1]);
          offspringPopulation.add(offSpring[0]);
          offspringPopulation.add(offSpring[1]);
          evaluations += 2;
        } // if
      } // for

      // Create the solutionSet union of solutionSet and offSpring
      union = ((SolutionSet) population).union(offspringPopulation);

      // Ranking the union
      Ranking ranking = new Ranking(union);

      int remain = populationSize;
      int index = 0;
      SolutionSet front = null;
      population.clear();

      // Obtain the next front
      front = ranking.getSubfront(index);

      while ((remain > 0) && (remain >= front.size())) {
        //Assign crowding distance to individuals
        distance.crowdingDistanceAssignment(front, problem_.getNumberOfObjectives());
        //Add the individuals of this front
        for (int k = 0; k < front.size(); k++) {
          population.add(front.get(k));
        } // for

        //Decrement remain
        remain = remain - front.size();

        //Obtain the next front
        index++;
        if (remain > 0) {
          front = ranking.getSubfront(index);
        } // if
      } // while

      // Remain is less than front(index).size, insert only the best one
      if (remain > 0) {  // front contains individuals to insert
        distance.crowdingDistanceAssignment(front, problem_.getNumberOfObjectives());
        front.sort(new CrowdingComparator());
        for (int k = 0; k < remain; k++) {
          population.add(front.get(k));
        } // for

        remain = 0;
      } // if

      // This piece of code shows how to use the indicator object into the code
      // of NSGA-II. In particular, it finds the number of evaluations required
      // by the algorithm to obtain a Pareto front with a hypervolume higher
      // than the hypervolume of the true Pareto front.
      if ((indicators != null) &&
          (requiredEvaluations == 0)) {
        double HV = indicators.getHypervolume(population);
        if (HV >= (0.98 * indicators.getTrueParetoFrontHypervolume())) {
          requiredEvaluations = evaluations;
        } // if
      } // if


      //Before start again, send a solution to root
      //System.out.println(population.best(new CrowdingComparator()).getObjective(0) + " " + population.best(new CrowdingComparator()).getObjective(1));
      if(useMPI){
    	  Solution solutionToSend[] = new Solution[5];
    	  solutionToSend[0] = new Solution(population.best(new CrowdingComparator()));
    	  MPI.COMM_WORLD.Send(solutionToSend, 0, 1, MPI.OBJECT, 0, TAG.SOLUTION.code);

    	  //Receive a good solution
    	  Status solutionFromRoot = MPI.COMM_WORLD.Iprobe(0, TAG.SOLUTION.code);
    	  if(solutionFromRoot != null){
    		  MPI.COMM_WORLD.Recv(solutionToSend, 0, solutionFromRoot.count, MPI.OBJECT, 0, TAG.SOLUTION.code);
    		  for(int j=0;j<solutionFromRoot.count;++j){
	    		  if(population.size() + 1 >= population.getCapacity())
	    			  population.remove(population.indexWorst(new CrowdingComparator()));
	    		  population.add(solutionToSend[j]);
    		  }

    		  System.out.println(MPI.COMM_WORLD.Rank() + " received " + solutionFromRoot.count + " solutions from Root");
    	  }
      }

    } // while

    // Return as output parameter the required evaluations
    setOutputParameter("evaluations", requiredEvaluations);

    // Return the first non-dominated front
    Ranking ranking = new Ranking(population);
    ranking.getSubfront(0).printFeasibleFUN("FUN_NSGAII") ;

    if(useMPI){
      System.out.println(MPI.COMM_WORLD.Rank() + " finishing...");

  	  Solution solutionToSend[] = new Solution[ranking.getSubfront(0).size()];
  	  for(int j=0;j<ranking.getSubfront(0).size();++j){
  		solutionToSend[j] = new Solution(ranking.getSubfront(0).get(j));
  	  }

  	  MPI.COMM_WORLD.Send(solutionToSend, 0, ranking.getSubfront(0).size(), MPI.OBJECT, 0, TAG.SOLUTION.code);
    }

    //When finishs NSGA-II set the progress to 1
    if(progress != null) progress.set(1);

    return ranking.getSubfront(0);
  } // execute
} // NSGA-II
