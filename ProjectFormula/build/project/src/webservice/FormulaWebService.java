package webservice;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import model.FormulaUtility;
import model.User;

public class FormulaWebService {

	public static final String URL = "http://www.larces.uece.br:8080/com.formula.rest/api/";

	static public String getStatus(){
		String status = "Down";

		//Session session = sshConnection();

		try{
	    	Client client = ClientBuilder.newClient();

	    	WebTarget target = client.target(FormulaWebService.URL + "status");

	    	if(target != null)
	    		status = target.request(MediaType.TEXT_HTML).get(String.class);
    	}catch(Exception e){
    		e.printStackTrace();
    	}

		//session.disconnect();

		return status;
	}

	static public User getUser(String email, String pwd){
		User user = null;
		String userXML = null;
		try{
	    	Client client = ClientBuilder.newClient();

	    	System.out.println(FormulaWebService.URL + "login/do/" + email  + "/" + pwd);

	    	WebTarget target = client.target(FormulaWebService.URL + "login/do/" + email  + "/" + pwd);

	    	if(target != null){
	    		userXML = target.request(MediaType.TEXT_XML).get(String.class);

	    		if(userXML != null){
	    			user = (User) FormulaUtility.xmlToObject(userXML, User.class);
	    		}
	    	}
    	}catch(Exception e){
    		e.printStackTrace();
    	}

		return user;
	}

	static private Session sshConnection(){
		String user = "scherm";
        String password = "!qaz@wsx";
        String host = "200.19.177.85";
        int port=6080;

        Session session = null;

        try
            {
            JSch jsch = new JSch();
            session = jsch.getSession(user, host, port);
            int lport = 4321;
            String rhost = "localhost";
            int rport = 8080;
            session.setPassword(password);
            session.setConfig("StrictHostKeyChecking", "no");
            System.out.println("Establishing Connection...");
            session.connect();
            int assinged_port=session.setPortForwardingL(lport, rhost, rport);
            System.out.println("localhost:"+assinged_port+" -> "+rhost+":"+rport);
            }
        catch(Exception e){System.err.print(e);}

        return session;
	}

}
