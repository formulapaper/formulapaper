package application.view;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.sun.prism.paint.Color;

import application.ApplicationContext;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import magius.GeneralProblem;
import magius.GeneralVariable;

public class CreateVariableController {

	@FXML
	private TextField name;

	@FXML
	private ChoiceBox<String> type;

	@FXML
	private TextField sizeOne;

	@FXML
	private TextField sizeTwo;

	@FXML
	private ComboBox<String> associatedVariableOne;

	@FXML
	private ComboBox<String> associatedVariableTwo;

	@FXML
	private Button cancelButton;

	@FXML
	private Button okButton;

	boolean okButtonClicked;

	private Stage dialogStage;

	private GeneralProblem problem;

	private GeneralVariable variable;

	int TYPE;
	static int NORMAL = 0;
	static int VECTOR = 1;
	static int MATRIX = 2;

	public CreateVariableController() {
		okButtonClicked = false;
		TYPE = NORMAL;
	}

	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

	/**
     * Initialize the controller class. This method is called automatically
     * before the fxml file be loaded
     */
    @FXML
    private void initialize() {
    	okButtonController();
    	cancelButtonController();

    	updateType();

    	variable = null;

    	sizeOne.setDisable(TYPE == NORMAL);
    	sizeTwo.setDisable(TYPE != MATRIX);

		associatedVariableOne.setDisable(TYPE == NORMAL);
		associatedVariableTwo.setDisable(TYPE != MATRIX);

    	type.valueProperty().addListener((obs, oldValue, newValue) -> {
    		if(newValue.equals("Normal"))
    			TYPE = NORMAL;
    		else if(newValue.equals("Vector"))
    			TYPE = VECTOR;
    		else if(newValue.equals("Matrix"))
    			TYPE = MATRIX;

    		sizeOne.setDisable(TYPE == NORMAL);
        	sizeTwo.setDisable(TYPE != MATRIX);

    		associatedVariableOne.setDisable(TYPE == NORMAL);
    		associatedVariableTwo.setDisable(TYPE != MATRIX);
		});

    }


    public boolean isOkClicked(){
    	return okButtonClicked;
    }


    private boolean isInteger(TextField field){
    	try
    	  {
    	    Integer value = Integer.parseInt(field.getText());
    	  }
    	  catch(NumberFormatException nfe)
    	  {
    	    return false;
    	  }
    	  return true;
    }

    private void okButtonController(){
    	okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            	//Validate
            	if((!name.getText().equals("")) &&
            		(type.getSelectionModel().getSelectedIndex() == 0 ||
            		(type.getSelectionModel().getSelectedIndex() == 1 && !sizeOne.getText().equals("") && isInteger(sizeOne)) ||
                    (type.getSelectionModel().getSelectedIndex() == 2 && !sizeOne.getText().equals("") && isInteger(sizeOne) && !sizeTwo.getText().equals("") && isInteger(sizeTwo)))){

            		//Its normal
            		if(type.getSelectionModel().getSelectedIndex() == 0){
            			variable = new GeneralVariable("Normal", name.getText());
            		}else if(type.getSelectionModel().getSelectedIndex() == 1){
            			variable = new GeneralVariable("Vector", name.getText(), Integer.valueOf(sizeOne.getText()));
            			if(associatedVariableOne.getSelectionModel().getSelectedIndex() != 0){
            				variable.setVariableDimension1(associatedVariableOne.getSelectionModel().getSelectedItem());
            			}
            		}else if(type.getSelectionModel().getSelectedIndex() == 2){
            			variable = new GeneralVariable("Matrix", name.getText(), Integer.valueOf(sizeOne.getText()), Integer.valueOf(sizeTwo.getText()));
            			if(associatedVariableOne.getSelectionModel().getSelectedIndex() != 0 && associatedVariableTwo.getSelectionModel().getSelectedIndex() != 0){
            				variable.setVariableDimension1(associatedVariableOne.getSelectionModel().getSelectedItem());
            				variable.setVariableDimension2(associatedVariableTwo.getSelectionModel().getSelectedItem());
            			}
            		}

            		okButtonClicked = true;

            		dialogStage.close();

            	}else{
            		if(type.getSelectionModel().getSelectedIndex() == 1 && !isInteger(sizeOne)){
            			sizeOne.setStyle("-fx-background-color: #FF5B5E;");
            		}else if(type.getSelectionModel().getSelectedIndex() == 2){
            			if(!isInteger(sizeOne))
            				sizeOne.setStyle("-fx-background-color: #FF5B5E;");

            			if(!isInteger(sizeTwo))
            				sizeTwo.setStyle("-fx-background-color: #FF5B5E;");
            		}

            		if(name.getText().equals("")){
            			name.setStyle("-fx-background-color: #FF5B5E;");
            		}
            	}


            }
    	});
    }

    private void cancelButtonController(){
    	cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	dialogStage.close();
            }
    	});
    }

    private void updateType(){
    	List<String> typeList  = new ArrayList<String>();
    	typeList.clear();

    	typeList.add("Normal");
    	typeList.add("Vector");
    	typeList.add("Matrix");

       	type.getItems().clear();
       	type.getItems().addAll(typeList);
    	if(typeList.size() > 0)
    		type.setValue(typeList.get(0));
    }

    private void updateAssociatedVariable(){
    	List<String> tmp  = new ArrayList<String>();
    	tmp.clear();

    	tmp.add("");

    	for(int i=0;i<problem.variable.size();i++){
    		if(problem.variable.get(i).type.equals("Normal")){
    			tmp.add(problem.variable.get(i).name);
    		}
    	}

       	associatedVariableOne.getItems().clear();
       	associatedVariableOne.getItems().addAll(tmp);
    	if(tmp.size() > 0)
    		associatedVariableOne.setValue(tmp.get(0));

    	associatedVariableTwo.getItems().clear();
       	associatedVariableTwo.getItems().addAll(tmp);
    	if(tmp.size() > 0)
    		associatedVariableTwo.setValue(tmp.get(0));
    }


	public GeneralVariable getVariable() {
		return variable;
	}

	public void setVariable(GeneralVariable value) {
		this.variable = value;
		name.setText(variable.name);

		type.getSelectionModel().select(variable.type);

		sizeOne.setText(variable.dimension1 + "");
		sizeTwo.setText(variable.dimension2 + "");

		associatedVariableOne.getSelectionModel().select(variable.getVariableDimension1());
		associatedVariableTwo.getSelectionModel().select(variable.getVariableDimension2());
	}

	public void setProblem(GeneralProblem problem){
		this.problem = problem;
		updateAssociatedVariable();
	}

}
