package application;

import formulaUserInterface.UserInterface;
import javafx.application.Application;
import javafx.stage.Stage;

public class Teste extends Application{

	UserInterface userInterface;

	@Override
	public void start(Stage primaryStage) throws Exception {
		userInterface = new UserInterface("FormulaUI", "view/Menu.fxml", 800,600, primaryStage);

		userInterface.show(MainApp.class);

	}

	public static void main(String[] args) {
		launch(args);
	}


}
